<?php
	require_once('database.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/style.css" />
	<script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <script type="text/javascript">
	
		$(document).ready(function() {
            $('.formatint').on('keypress',function(){
				if (event.keyCode < 48 || event.keyCode > 57) {
					event.preventDefault();
				}
			});
        });
		
	</script>
</head>
<body id="popup">
	<fieldset>
    	<legend>เพิ่มผู้ขาย/ผู้รับจ้าง/ผู้บริจาค</legend>
        <form action="#" method="POST">
            <label class="lbl">ชื่อ</label><input name="agent_name" type="text" value="<?php if(!empty($_POST['agent_name'])) echo $_POST['agent_name']; ?>" /><p class="description"><?php if(empty($_POST['agent_name'])) echo 'กรุณากรอกข้อมูล'; ?></p><br />
            <label class="lbl">ที่อยู่</label><input name="agent_addr" type="text" value="<?php if(!empty($_POST['agent_addr'])) echo $_POST['agent_addr']; ?>" /><p class="description"><?php if(empty($_POST['agent_addr'])) echo 'กรุณากรอกข้อมูล'; ?></p><br />
            <label class="lbl">เบอร์โทรศัพท์</label><input class="formatint" name="agent_tel" type="text" maxlength="10" value="<?php if(!empty($_POST['agent_tel'])) echo $_POST['agent_tel']; ?>" /><p class="description"><?php if(empty($_POST['agent_tel'])) echo 'กรุณากรอกข้อมูล'; ?></p><br />
            <input type="submit" value="เพิ่มข้อมูล" />
        </form>
    </fieldset>
</body>
<?php
	if(!empty($_POST)){
		if(empty($_POST['agent_name'])){
		}elseif(empty($_POST['agent_addr'])){
		}elseif(empty($_POST['agent_tel'])){
		}else{
			$exec = database::getInstance();
			$qry = $exec->execute('SELECT MAX(agent_id) AS max_id FROM agent');
			$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
			if(!empty($rs['max_id'])){
				$gen = substr($rs['max_id'],1)+1;
				$agent_id = sprintf('A%003.0f',$gen);
			}else{
				$agent_id = 'A001';
			}
			$exec->execute("INSERT INTO agent VALUES('$agent_id','$_POST[agent_name]','$_POST[agent_addr]','$_POST[agent_tel]')");
			echo 'เพิ่มข้อมูลผู้ขาย/ผู้รับจ้าง/ผู้บริจาคเรียบร้อยแล้ว';
			echo '<script>window.close();</script>';
		}
	}else{
		echo 'กรุณากรอกข้อมูลให้ครบถ้วน';
	}
?>
</html>

