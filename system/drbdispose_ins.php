<?php
	require_once 'database.php';
	require_once 'log.php';
    session_start();
	ob_start();
    $exec = database::getInstance()
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/prototype.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ระบบจัดการครุภัณฑ์ คณะบริหารธุรกิจ มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.min.css" />
    <script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <!-- InstanceBeginEditable name="head" -->
    <style type="text/css">
		.ui-tabs{ font-family:tahoma; font-size:11px; }
		.ui-datepicker{ width:220px; font-family:tahoma; font-size:11px; text-align:center; }
	</style>
    <script>
		$(function(){
			$('.datepicker').datepicker({
				dateFormat: 'dd-mm-yy',
				dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
				monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
				changeMonth: true,  
				changeYear: true,
				beforeShow:function(){    
					if($(this).val() != ''){  
						var arrayDate=$(this).val().split('-');       
						arrayDate[2]=parseInt(arrayDate[2]);  
						$(this).val(arrayDate[0]+'-'+arrayDate[1]+'-'+arrayDate[2]);  
					}  
					setTimeout(function(){
						$.each($('.ui-datepicker-year option'),function(j,k){  
							var textYear=parseInt($('.ui-datepicker-year option').eq(j).val());  
							$('.ui-datepicker-year option').eq(j).text(textYear);  
						});               
					},50);  
				},  
				onChangeMonthYear: function(){
					setTimeout(function(){
						$.each($('.ui-datepicker-year option'),function(i,v){  
							var textYear=parseInt($('.ui-datepicker-year option').eq(i).val());  
							$('.ui-datepicker-year option').eq(i).text(textYear);  
						});               
					},50);        
				},  
				onClose:function(){
					if($(this).val() != '' && $(this).val() == dateBefore){           
						var arrayDate=dateBefore.split('-');  
						arrayDate[2]=parseInt(arrayDate[2]);  
						$(this).val(arrayDate[0]+'-'+arrayDate[1]+'-'+arrayDate[2]);      
					}         
				},  
				onSelect: function(dateText, inst){
					dateBefore=$(this).val();  
					var arrayDate=dateText.split('-');  
					arrayDate[2]=parseInt(arrayDate[2]);  
					$(this).val(arrayDate[0]+'-'+arrayDate[1]+'-'+arrayDate[2]);  
				}
			});
		});
	</script>
	<!-- InstanceEndEditable -->
</head>
<body>
	<div id="header"></div>
    <div id="menu">
    	<?php
			include 'login.php';
		?>
    </div>
    <div id="middle">
    	<div id="info"><!-- InstanceBeginEditable name="content" -->
        	<form action="" method="post">
                <fieldset>
                    <legend>ข้อมูลการแทงจำหน่ายครุภัณฑ์</legend>
                    <label class="lbl">สาเหตุ</label><select name="dispose_cause">
                    	<option>ชำรุด</option>
                        <option>บริจาค</option>
                    </select><br />
                    <label class="lbl">หมายเหตุ</label><textarea name="dispose_note"></textarea>
                </fieldset>
                <fieldset>
                    <legend>ข้อมูลครุภัณฑ์</legend>
                        <table id="tbldrbno">
                            <thead>
                                <th>ลำดับที่</th>
                                <th>หมายเลขครุภัณฑ์</th>
                                <th>ชื่อครุภัณฑ์</th>
                                <th>สถานะปัจจุบัน</th>
                                <th>ผู้รับผิดชอบ</th>
                                <th>สถานะประกัน</th>
                            </thead>
                            <tbody>
                                <?php
                                    if(empty($_SESSION['drbno_id']))
                                        $_SESSION['drbno_id'] = $_POST['drbno_id'];
											
                                    $i = 0;
                                    foreach($_SESSION['drbno_id'] as $v){
                                        $qry = $exec->execute("SELECT * FROM drbno, drb, drbstatus, respons WHERE drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND respons.resp_id=drbno.resp_id AND drbno.drbno_status='1' AND drbno.drbno_id='$v' ORDER BY drbno_id ASC");
                                        if(mysqli_num_rows($qry) != 0){
                                            while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
                                                $i++;
                                                $culdate = date('Y-m-d', strtotime("+$rs[drb_warrenty] year", strtotime($rs['drb_receivedate'])));
                                                $nowdate = date('Y-m-d');
                                                if($culdate > $nowdate)
                                                	echo "<tr><td>$i</td><td>$rs[drbno_number]</td><td>$rs[drb_name]</td><td>$rs[drbstatus_name]</td><td>$rs[resp_name]</td><td>อยู่ในระยะรับประกัน</td><input name=\"drbno_id[]\" type=\"hidden\" value=\"$rs[drbno_id]\" /><tr>";
                                            	else
                                                	echo "<tr><td>$i</td><td>$rs[drbno_number]</td><td>$rs[drb_name]</td><td>$rs[drbstatus_name]</td><td>$rs[resp_name]</td><td>หมดประกัน</td><input name=\"drbno_id[]\" type=\"hidden\" value=\"$rs[drbno_id]\" /><tr>";
                                            }
                                        }
                                    }
									if($i == 0){
                                        unset($_SESSION['drbno_id']);
                                    	header('location:drbdispose_disp.php');
                                    }
                                ?>
                            </tbody>
                        </table>
                        <center>
                       </br> <button name="btn" value="save">แทงจำหน่าย</button><button name="btn" value="back">ย้อนกลับ</button><br />
                        </center>

                </fieldset>
                </form>
                
                <?php
					
					if($_POST['btn'] == 'save'){
						/*if(empty($_POST['resp_id'])){
							#echo 'empty control name resp_id.';
						}elseif($_POST['room_id'] == 'null'){
							#echo 'not choose room_id.';
						}elseif(count($_POST['drbno_id']) == 0){
							#echo 'not choose durable for storage.';
						}else{*/					
							$nowdate = date('Y-m-d');
							
							foreach($_POST['drbno_id'] as $v){
								$exec->execute("UPDATE drbno SET drbno_status='3' WHERE drbno_id='$v'");
                                
                                $qry = $exec->execute('SELECT MAX(dispose_id) AS max_id FROM drbdispose');
                                $rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
                                if(!empty($rs['max_id'])){
                                    $gen = substr($rs['max_id'], 1)+1;
                                    $dispose_id = sprintf('D%009.0f', $gen);
                                }else{
                                    $dispose_id = 'D000000001';
                                }								
                                echo "<script>alert('INSERT INTO drbdispose VALUES('$dispose_id','$nowdate','$_POST[dispose_cause]','$_POST[dispose_note]','$v')')</script>";
								$exec->execute("INSERT INTO drbdispose VALUES('$dispose_id','$nowdate','$_POST[dispose_cause]','$_POST[dispose_note]','$v')");
								$log->write_log($_SESSION['auth']['name'].'->dulable_dispose:'.$v);
							}
							unset($_POST);
							echo "<script>alert('แทงจำหน่ายครุภัณฑ์เรียบร้อยแล้ว');</script>";
                            header("refresh:0;drbdispose_ins.php");
					}elseif($_POST['btn'] == 'back'){
						unset($_SESSION['drbno_id']);
						header('location:drbdispose_disp.php');
					}
				?>
		<!-- InstanceEndEditable --></div>
    </div>
    <div id="footer"></div>
</body>
<!-- InstanceEnd --></html>
