<?php
	require_once 'database.php';
    session_start();
	ob_start();
    $exec = database::getInstance()
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/prototype.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ระบบจัดการครุภัณฑ์ คณะบริหารธุรกิจ มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.min.css" />
    <script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <!-- InstanceBeginEditable name="head" -->
    <style type="text/css">
		.ui-tabs{ font-family:tahoma; font-size:11px; }
		.ui-datepicker{ width:220px; font-family:tahoma; font-size:11px; text-align:center; }
		.ui-menu .ui-menu-item a{ height:14px; font-family:tahoma; font-size:12px; }
	</style>
    <script>
		$(document).ready(function(){
			
			$('#check_all').click(function () {    
				 $('input:checkbox').prop('checked', this.checked);    
			});
			
			loaddrbtype();
            loadarea();
			//loadroom();
			$('#area_id').change(function(){
				loadbuilding();
			});
			
			$('#building_id').change(function(){
				loadfloor();
			});
			
			$('#floor_id').change(function(){
				loadroom();
			});
        });
		
		$(function(){
			$('.autocomplete').autocomplete({
				source: 'autocomplete.php?method=resp',
				minLength: 1,
				focus: function(event, ui){
					$('.autocomplete').val(ui.item.resp_name);
				},
				select: function(event, ui){
					$('#resp_branch').val(ui.item.resp_branch);
					$('#resp_tel').val(ui.item.resp_tel);
					$('#resp_room').val(ui.item.resp_room);
					$('#resp_id').val(ui.item.resp_id);
					return false;
				}
			})
			.data('ui-autocomplete')._renderItem = function(ul,item){
				return $('<li>')
					.append('<a>'+item.resp_name+'</a>')
					.appendTo(ul);
			};
		});
		
		function loaddrbtype(){
			$('#drbtype_id').empty();
			$.ajax({
				type: 'POST',
				url: 'load_contr.php',
				data: { method: 'drbtype' },
				success: function(data){
					var obj = $.parseJSON(data);
					$('#drbtype_id').append('<option value="">-- กรุณาเลือก --</option>');
					$.each(obj, function(i,v){
						if(v['drbtype_id'] == "<?php echo $_POST['drbtype_id'] ?>"){
							$('#drbtype_id').append('<option value="'+v['drbtype_id']+'" selected>'+v['drbtype_name']+'</option>');
						}else{
							$('#drbtype_id').append('<option value="'+v['drbtype_id']+'">'+v['drbtype_name']+'</option>');
						}
					});
				}
			});
		}
		
		function loadarea(){
			$('#area_id').empty();
			$.ajax({
				type: 'POST',
				url: 'load_contr.php',
				data: { method: 'area' },
				success: function(data){
					var obj = $.parseJSON(data);
					$('#area_id').append('<option value="null">-- กรุณาเลือก --</option>');
					$.each(obj, function(i,v){
						$('#area_id').append('<option value="'+v['area_id']+'">'+v['area_name']+'</option>');
					});
				}
			});
		}
		
		function loadbuilding(){
			if($('#area_id').val() != 'null'){
				$('#building_id').empty();
				$.ajax({
					type: 'POST',
					url: 'load_contr.php',
					data: { method: 'building', area_id: $('#area_id').val() },
					success: function(data){
						$('#building_id').append('<option value="null">-- กรุณาเลือก --</option>');
						if(data != 'null'){
							var obj = $.parseJSON(data);
							$.each(obj, function(i,v){
								$('#building_id').append('<option value="'+v['building_id']+'">'+v['building_name']+'</option>');
							});
						}
					}
				});
			}else{
				$('#building_id').append('<option value="null">-- กรุณาเลือก --</option>');
			}
		}
		
		function loadfloor(){
			if($('#building_id').val() != 'null'){
				$('#floor_id').empty();
				$.ajax({
					type: 'POST',
					url: 'load_contr.php',
					data: { method: 'floor', building_id: $('#building_id').val() },
					success: function(data){
						$('#floor_id').append('<option value="null">-- กรุณาเลือก --</option>');
						if(data != 'null'){
							var obj = $.parseJSON(data);
							$.each(obj, function(i,v){
								$('#floor_id').append('<option value="'+v['floor_id']+'">'+v['floor_name']+'</option>');
							});
						}
					}
				});
			}else{
				$('#floor_id').append('<option value="null">-- กรุณาเลือก --</option>');
			}
		}
		
		function loadroom(){
			if($('#floor_id').val() != 'null'){
				$('#room_id').empty();
				console.log($('#floor_id :selected').val());
				$.ajax({
					type: 'POST',
					url: 'load_contr.php',
					data: { method: 'room', floor_id: $('#floor_id').val() },
					success: function(data){
						$('#room_id').append('<option value="">-- กรุณาเลือก --</option>');
						if(data != 'null'){
							var obj = $.parseJSON(data);
							$.each(obj, function(i,v){
								$('#room_id').append('<option value="'+v['room_id']+'">'+v['room_name']+'</option>');
							});
						}
					}
				});
			}else{
				$('#room_id').append('<option value="null">-- กรุณาเลือก --</option>');
			}
		}
	</script>
	<!-- InstanceEndEditable -->
</head>
<body>
	<div id="header"></div>
    <div id="menu">
    	<?php
			include 'login.php';
		?>
    </div>
    <div id="middle">
    	<div id="info"><!-- InstanceBeginEditable name="content" -->
        	<fieldset>
            	<legend>ค้นหาคุรภัณฑ์</legend>
                <form action="" method="get">
                
                	<label class="lbl">พื้นที่</label><select id="area_id"></select><br />
                    <label class="lbl">อาคาร</label><select id="building_id"></select><br />
                    <label class="lbl">ชั้น</label><select id="floor_id"></select><br />
                    <label class="lbl">ห้อง</label><select id="room_id" name="room_id"></select><br />
                	<label class="lbl">ประเภท</label><select id="drbtype_id" name="drbtype_id"></select>
                	<button id="btn_search">ค้นหา</button>
                </form>
            </fieldset>
            <fieldset>
            	<legend>ผลการค้นหา</legend>
                <form action="report_durable.php?room_id=<?=$_REQUEST[room_id];?>&drbtype_id=<?=$_REQUEST[drbtype_id];?>" method="post">
                	<?php
						$qry = $exec->genpage("SELECT * FROM drb, drbno, area, building, floor, room, respons WHERE drb.drb_id=drbno.drb_id AND building.area_id=area.area_id AND floor.building_id=building.building_id AND room.floor_id=floor.floor_id AND drbno.room_id=room.room_id AND drbno.resp_id=respons.resp_id  AND drbno.room_id LIKE '%$_REQUEST[room_id]%' AND drb.drbtype_id LIKE '%$_REQUEST[drbtype_id]%' ORDER BY drbno.drbno_id");
                    	$start = $exec->getstart();
					?>
                	<table id="tbldrbno">
                    	<thead>
                        	<th>ลำดับที่</th>
                            <th>หมายเลขครุภัณฑ์</th>
                            <th>ชื่อครุภัณฑ์</th>
                            <th>ยี้ห้อ</th>
                            <th>สาขาที่รับผิดชอบ</th>
                            <th>วันที่รับครุภัณฑ์</th>
                            <th>ระยะเวลา<br />รับประกัน</th>
                            <th>สถานะ</th>
                        </thead>
                        <tbody>
                        	<?php
								while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
									$start++;
									if($rs['drbno_status'] == '1')
										echo "<tr><td>$start</td><td>$rs[drbno_number]</td><td>$rs[drb_name]</td><td>$rs[drb_band]</td><td>$rs[resp_branch]</td><td>$rs[drb_receivedate]</td><td>$rs[drb_warrenty]</td><td>ปกติ</td></tr>";
									elseif($rs['drbno_status'] == '2')
										echo "<tr><td>$start</td><td>$rs[drbno_number]</td><td>$rs[drb_name]</td><td>$rs[drb_band]</td><td>$rs[resp_branch]</td><td>$rs[drb_receivedate]</td><td>$rs[drb_warrenty]</td><td>ส่งซ่อม</td></tr>";
									elseif($rs['drbno_status'] == '3')
										echo "<tr><td>$start</td><td>$rs[drbno_number]</td><td>$rs[drb_name]</td><td>$rs[drb_band]</td><td>$rs[resp_branch]</td><td>$rs[drb_receivedate]</td><td>$rs[drb_warrenty]</td><td>แทงจำหน่าย</td></tr>";
									
								}
							?>
                        </tbody>
                    </table>
                    <?php
                    	$exec->link();
					?><br />
                    <button>พิมพ์รายงาน</button>
                </form>
            </fieldset>
		<!-- InstanceEndEditable --></div>
    </div>
    <div id="footer"></div>
</body>
<!-- InstanceEnd --></html>
