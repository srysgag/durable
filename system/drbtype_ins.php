<?php
	require_once('database.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/style.css" />
	<script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
</head>
<body id="popup">
	<fieldset>
    	<legend>เพิ่มประเภทครุภัณฑ์</legend>
        <form action="#" method="POST">
            <label class="lbl">ประเภทครุภัณฑ์</label><input name="drbtype_name" type="text" value="<?php if(!empty($_POST['dtbtype_name'])) echo $_POST['dtbtype_name']; ?>" /><br />
            <p class="description"><?php if(empty($_POST['drbtype_name'])) echo 'กรุณากรอกประเภทครุภัณฑ์'; ?></p><br />
            <label class="lbl">อัตราค่าเสื่อม/ปี</label><input name="drbtype_deper" type="text" value="<?php if(!empty($_POST['drbtype_deper'])) echo $_POST['drbtype_deper']; ?>" size="5" /><label class="tail">บาท</label><br />
            <p class="description"><?php if(empty($_POST['drbtype_deper'])) echo 'กรุณากรอกข้อมูล'; ?></p><br />
            <input type="submit" value="เพิ่มข้อมูล" />
        </form>
    </fieldset>
</body>
<?php
	if(!empty($_POST)){
		if(empty($_POST['drbtype_name'])){
		}else{
			$exec = database::getInstance();
			$qry = $exec->execute('SELECT MAX(drbtype_id) AS max_id FROM drbtype');
			$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
			if(!empty($rs['max_id'])){
				$gen = substr($rs['max_id'],1)+1;
				$drbtype_id = sprintf('D%003.0f',$gen);
			}else{
				$drbtype_id = 'D001';
			}
			$exec->execute("INSERT INTO drbtype VALUES('$drbtype_id','$_POST[drbtype_name]','$_POST[drbtype_deper]')");
			echo 'เพิ่มข้อมูลประเภทครุภัณฑ์เรียบร้อยแล้ว';
			echo '<script>window.opener.loaddrbtype();window.close();</script>';
		}
	}else{
		echo 'กรุณากรอกข้อมูลให้ครบถ้วน';
	}
?>
</html>
