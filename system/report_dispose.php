<?php
	define('FPDF_FONTPATH','font/');
	header('Content-type: application/pdf');
	header('Content-Type: text/html; charset=UTF-8');
	
	require_once 'mc_table.php';
	require_once 'database.php';
	
	$exec = database::getInstance();
	$pdf = new PDF_MC_Table();
	$month = array('มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม');
	$getdate = date('Y-m-d');
	$today = explode('-', date('Y-m-d'));
	$i = 0;
	$c = 1;
	$p = 0;
	$start = explode('-', $_GET['start']);
	$end = explode('-', $_GET['end']);
	$qry = $exec->execute("SELECT * FROM drbno, drb, drbstatus, respons, drbdispose WHERE drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND respons.resp_id=drbno.resp_id AND drbno.drbno_id=drbdispose.drbno_id AND drbdispose.dispose_date BETWEEN '$_GET[start]' AND '$_GET[end]' AND drb.drbtype_id='$_GET[drbtype_id]' AND drbno.drbno_status='3' ORDER BY drbno.drbno_id ASC");
	
	
	$pdf->AddPage();
	$pdf->AddFont('angsana','','angsa.php');
	$pdf->AddFont('angsana','B','angsab.php');
	$pdf->AddFont('angsana','I','angsai.php');
	$pdf->AddFont('angsana','BI','angsaz.php');
	$pdf->SetFont('angsana','B',16);
	$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'หน้า '.($p+1) ), 0, 'R');
	$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'ณ วันที่ '.intval($today[2]).' เดือน '.$month[$today[1] - 1].' พ.ศ. '.($today[0]+543) ), 0, 'R');
    $pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ' ), 0, 'C');
	$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'รายงานแทงจำหน่ายครุภัณฑ์' ), 0, 'C');
	$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'ตั้งแต่วันที่ '.intval($start[2]).' '.$month[$start[1]-1].' '.($start[0]+543).' ถึงวันที่ '.intval($end[2]).' '.$month[$end[1]-1].' '.($end[0]+543)), 0, 'C');
	#$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'ตั้งแต่วันที่ '.intval($start[2]).' '.$month[$start[1]-1].' '.$start[0].' ถึงวันที่ '.intval($end[2]).' '.$month[$end[1]-1].' '.$end[0], 0, 'C'));
	$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'คณะบริหารธุรกิจ'), 0, 'R');
	$pdf->SetWidths(array(13,40,50,22,25,20,20));
	
	#$pdf->HeaderRow(array(iconv('UTF-8', 'cp874', 'ลำดับที่'), iconv('UTF-8', 'cp874', 'หมายเลขครุภัณฑ์'), iconv('UTF-8', 'cp874', 'รายการ'), iconv('UTF-8', 'cp874', 'ผู้รับผิดชอบ'), iconv('UTF-8', 'cp874', 'วันที่'), iconv('UTF-8', 'cp874', 'สาเหตุ'), iconv('UTF-8', 'cp874', 'หมายเหตุ')));
	while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
		if($c == 1){		
			$pdf->HeaderRow(array(iconv('UTF-8', 'cp874', 'ลำดับที่'), iconv('UTF-8', 'cp874', 'หมายเลขครุภัณฑ์'), iconv('UTF-8', 'cp874', 'รายการ'), iconv('UTF-8', 'cp874', 'วันที่ส่งซ่อม'), iconv('UTF-8', 'cp874', 'ผู้รับผิดชอบ'), iconv('UTF-8', 'cp874', 'อาการ'), iconv('UTF-8', 'cp874', 'ผู้รับซ่อม')));	
		}elseif($c / 20 == 1){
			$p++;
			$pdf->AddPage();
			$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'หน้า '.($p+1) ), 0, 'R');
			$pdf->HeaderRow(array(iconv('UTF-8', 'cp874', 'ลำดับที่'), iconv('UTF-8', 'cp874', 'หมายเลขครุภัณฑ์'), iconv('UTF-8', 'cp874', 'รายการ'), iconv('UTF-8', 'cp874', 'วันที่ส่งซ่อม'), iconv('UTF-8', 'cp874', 'ผู้รับผิดชอบ'), iconv('UTF-8', 'cp874', 'อาการ'), iconv('UTF-8', 'cp874', 'ผู้รับซ่อม')));
			
		}
		$i++;
		$c++;
		$pdf->Row(array($i, iconv('UTF-8', 'cp874', $rs['drbno_number']), iconv('UTF-8', 'cp874', $rs['drb_name'].' '.$rs['drb_model']), iconv('UTF-8', 'cp874', $rs['resp_name']), iconv('UTF-8', 'cp874', $rs['dispose_date']), iconv('UTF-8', 'cp874', $rs['dispose_cause']), iconv('UTF-8', 'cp874', $rs['dispose_note'])));
	}
	$pdf->Output();
?>