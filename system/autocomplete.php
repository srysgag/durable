<?php
	require_once('database.php');
	
	if($_REQUEST['method'] == 'agent'){
		$term = trim(strip_tags($_GET['term']));
		$exec = database::getInstance();
		$qry = $exec->execute("SELECT * FROM agent WHERE agent_name LIKE '%$term%'");
		while($rs = mysqli_fetch_array($qry,MYSQLI_ASSOC)){
			$rs_set[] = $rs;
		}
		echo json_encode($rs_set);
	}elseif($_REQUEST['method'] == 'resp'){
		$term = trim(strip_tags($_GET['term']));
		$exec = database::getInstance();
		$qry = $exec->execute("SELECT * FROM respons WHERE resp_name LIKE '%$term%'");
		while($rs = mysqli_fetch_array($qry,MYSQLI_ASSOC)){
			$rs_set[] = $rs;
		}
		echo json_encode($rs_set);
	}elseif($_REQUEST['method'] == 'drbno'){
		$term = trim(strip_tags($_GET['term']));
		$exec = database::getInstance();
		$qry = $exec->execute("SELECT * FROM drbno WHERE (drbno_status='1' OR drbno_status='2') AND drbno_number LIKE '%$term%'");
		while($rs = mysqli_fetch_array($qry,MYSQLI_ASSOC)){
			$rs_set[] = $rs;
		}
		echo json_encode($rs_set);
	}elseif($_REQUEST['method'] == 'drb'){
		$term = trim(strip_tags($_GET['term']));
		$exec = database::getInstance();
		$qry = $exec->execute("SELECT * FROM drb WHERE drb_no LIKE '%$term%'");
		while($rs = mysqli_fetch_array($qry,MYSQLI_ASSOC)){
			$rs_set[] = $rs;
		}
		echo json_encode($rs_set);
	}elseif($_REQUEST['method'] == 'drbrepair'){
		$term = trim(strip_tags($_GET['term']));
		$exec = database::getInstance();
		$qry = $exec->execute("SELECT * FROM drbno WHERE drbno_status='2' AND drbno_number LIKE '%$term%'");
		while($rs = mysqli_fetch_array($qry,MYSQLI_ASSOC)){
			$rs_set[] = $rs;
		}
		echo json_encode($rs_set);
	}elseif($_REQUEST['method'] == 'drbdispose'){
		$term = trim(strip_tags($_GET['term']));
		$exec = database::getInstance();
		$qry = $exec->execute("SELECT * FROM drbno WHERE drbno_status='3' AND drbno_number LIKE '%$term%'");
		while($rs = mysqli_fetch_array($qry,MYSQLI_ASSOC)){
			$rs_set[] = $rs;
		}
		echo json_encode($rs_set);
	}
	elseif($_REQUEST['method'] == 'search'){
		$data = array();
		$term = trim(strip_tags($_GET['term']));
		$exec = database::getInstance();
		if(trim(strip_tags($_GET['type'])) == 'number'){
			$qry = $exec->execute("SELECT drbno_number FROM drbno WHERE drbno_number LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$temp = array('name'=>$rs['drbno_number']);
				array_push($data, $temp);
			}
		}elseif(trim(strip_tags($_GET['type'])) == 'document'){
			$qry = $exec->execute("SELECT drb_no FROM drb WHERE drb_no LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$temp = array('name'=>$rs['drb_no']);
				array_push($data, $temp);
			}
		}elseif(trim(strip_tags($_GET['type'])) == 'room'){
			$qry = $exec->execute("SELECT room_name FROM room WHERE room_name LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$temp = array('name'=>$rs['room_name']);
				array_push($data, $temp);
			}
		}elseif(trim(strip_tags($_GET['type'])) == 'building'){
			$qry = $exec->execute("SELECT building_name FROM building WHERE building_name LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$temp = array('name'=>$rs['building_name']);
				array_push($data, $temp);
			}
		}elseif(trim(strip_tags($_GET['type'])) == 'area'){
			$qry = $exec->execute("SELECT area_name FROM area WHERE area_name LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$temp = array('name'=>$rs['area_name']);
				array_push($data, $temp);
			}
		}elseif(trim(strip_tags($_GET['type'])) == 'responsible'){
			$qry = $exec->execute("SELECT resp_name FROM respons WHERE resp_name LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$temp = array('name'=>$rs['resp_name']);
				array_push($data, $temp);
			}
		}elseif(trim(strip_tags($_GET['type'])) == 'branch'){
			$qry = $exec->execute("SELECT resp_branch FROM respons WHERE resp_branch LIKE '%$term%' GROUP BY resp_branch");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$temp = array('name'=>$rs['resp_branch']);
				array_push($data, $temp);
			}
		}
		
		echo json_encode($data);
	}/*elseif(trim(strip_tags($_GET['type'])) == 'emp'){
		$term = trim(strip_tags($_GET['term']));
		$exec = database::getInstance();
		$qry = $exec->execute("SELECT * FROM perm, auth, employee WHERE perm.perm_id=auth.perm_id AND auth.auth_id=employee.auth_id AND (emp_fname LIKE '%$term%' OR emp_lname LIKE '%$term%') GROUP BY employee.emp_id");
		while($rs = mysqli_fetch_array($qry,MYSQLI_ASSOC)){
			$temp = array('name'=>'test');
			array_push($data, $temp);
		}
		echo json_encode($data);
	}*/
?>