<?php
	define('FPDF_FONTPATH','font/');
	header('Content-type: application/pdf');
	header('Content-Type: text/html; charset=UTF-8');
	
	require_once 'mc_table.php';
	require_once 'database.php';
	
	$exec = database::getInstance();
	$pdf = new PDF_MC_Table();
	$month = array('มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม');
	$getdate = date('Y-m-d');
	$today = explode('-', date('Y-m-d'));
	$i = 0;
	$c = 1;
	$p = 0;
	$qry = $exec->execute("SELECT * FROM drb, drbno, area, building, floor, room, respons WHERE drb.drb_id=drbno.drb_id AND building.area_id=area.area_id AND floor.building_id=building.building_id AND room.floor_id=floor.floor_id AND drbno.room_id=room.room_id AND drbno.resp_id=respons.resp_id AND drbno.drbno_status='1' AND drb.typeof_detail='$_REQUEST[year]' ORDER BY drbno.drbno_id");
	
	
	$pdf->AddPage();
	$pdf->AddFont('angsana','','angsa.php');
	$pdf->AddFont('angsana','B','angsab.php');
	$pdf->AddFont('angsana','I','angsai.php');
	$pdf->AddFont('angsana','BI','angsaz.php');
	$pdf->SetFont('angsana','B',16);
	$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'หน้า '.($p+1) ), 0, 'R');
	$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'ณ วันที่ '.intval($today[2]).' เดือน '.$month[$today[1] - 1].' พ.ศ. '.($today[0]+543) ), 0, 'R');
    $pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ' ), 0, 'C');
	$room_id = trim($_REQUEST['room_id']);
	$drbtype_id = trim($_REQUEST['drbtype_id']);
	$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'รายงานครุภัณฑ์' ), 0, 'C');
	$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'ประจำปีงบประมาณ '.$_GET['year']), 0, 'C');
	$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'คณะบริหารธุรกิจ'), 0, 'C');
	$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', ''), 0, 'C');
	$pdf->SetWidths(array(15,45,60,30,40));
	
	#$pdf->HeaderRow(array(iconv('UTF-8', 'cp874', 'ลำดับที่'), iconv('UTF-8', 'cp874', 'หมายเลขครุภัณฑ์'), iconv('UTF-8', 'cp874', 'รายการ'), iconv('UTF-8', 'cp874', 'ราคาต่อหน่วย  (บาท)'), iconv('UTF-8', 'cp874', 'ผู้รับผิดชอบ')));
	while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
		
		if($c == 1){
			
			$pdf->HeaderRow(array(iconv('UTF-8', 'cp874', 'ลำดับที่'), iconv('UTF-8', 'cp874', 'หมายเลขครุภัณฑ์'), iconv('UTF-8', 'cp874', 'รายการ'), iconv('UTF-8', 'cp874', 'ราคาต่อหน่วย  (บาท)'), iconv('UTF-8', 'cp874', 'ผู้รับผิดชอบ')));
			
		}elseif($c / 20 == 1){
			$p++;
			$pdf->AddPage();
			$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'หน้า '.($p+1) ), 0, 'R');
			$pdf->HeaderRow(array(iconv('UTF-8', 'cp874', 'ลำดับที่'), iconv('UTF-8', 'cp874', 'หมายเลขครุภัณฑ์'), iconv('UTF-8', 'cp874', 'รายการ'), iconv('UTF-8', 'cp874', 'ราคาต่อหน่วย  (บาท)'), iconv('UTF-8', 'cp874', 'ผู้รับผิดชอบ')));
			
		}
		$i++;
		$c++;
		$pdf->Row(array($i, iconv('UTF-8', 'cp874', $rs['drbno_number']), iconv('UTF-8', 'cp874', $rs['drb_name'].' '.$rs['drb_model']), iconv('UTF-8', 'cp874', number_format($rs['drb_price'], 2, '.', ',')), iconv('UTF-8', 'cp874', $rs['resp_name'])));
	}
	$pdf->Output();
?>