<?php
	require_once 'database.php';
    session_start();
	ob_start();
    $exec = database::getInstance()
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/prototype.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ระบบจัดการครุภัณฑ์ คณะบริหารธุรกิจ มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.min.css" />
    <script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <!-- InstanceBeginEditable name="head" -->
    <style type="text/css">
		.ui-tabs{ font-family:tahoma; font-size:11px; }
		.ui-datepicker{ width:220px; font-family:tahoma; font-size:11px; text-align:center; }
		.ui-menu .ui-menu-item a{ height:14px; font-family:tahoma; font-size:12px; }
	</style> 
    <script>
		$(document).ready(function() {
			loaddrbtype();
		});
		
		$(function() {			
			$('.datepicker').datepicker({
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
				monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
				changeMonth: true,  
				changeYear: true,
				beforeShow:function(){    
					if($(this).val() != ''){  
						var arrayDate=$(this).val().split('-');       
						arrayDate[2]=parseInt(arrayDate[2]);  
						$(this).val(arrayDate[0]+'-'+arrayDate[1]+'-'+arrayDate[2]);  
					}  
					setTimeout(function(){
						$.each($('.ui-datepicker-year option'),function(j,k){  
							var textYear=parseInt($('.ui-datepicker-year option').eq(j).val());  
							$('.ui-datepicker-year option').eq(j).text(textYear);  
						});               
					},50);  
				},  
				onChangeMonthYear: function(){
					setTimeout(function(){
						$.each($('.ui-datepicker-year option'),function(i,v){  
							var textYear=parseInt($('.ui-datepicker-year option').eq(i).val());  
							$('.ui-datepicker-year option').eq(i).text(textYear);  
						});               
					},50);        
				},  
				onClose:function(){
					if($(this).val() != '' && $(this).val() == dateBefore){           
						var arrayDate=dateBefore.split('-');  
						arrayDate[2]=parseInt(arrayDate[2]);  
						$(this).val(arrayDate[0]+'-'+arrayDate[1]+'-'+arrayDate[2]);      
					}         
				},  
				onSelect: function(dateText, inst){
					dateBefore=$(this).val();  
					var arrayDate=dateText.split('-');  
					arrayDate[2]=parseInt(arrayDate[2]);  
					$(this).val(arrayDate[0]+'-'+arrayDate[1]+'-'+arrayDate[2]);  
				}
			});
		});
		
		function loaddrbtype(){
			$('#drbtype_id').empty();
			$.ajax({
				type: 'POST',
				url: 'load_contr.php',
				data: { method: 'drbtype' },
				success: function(data){
					var obj = $.parseJSON(data);
					$('#drbtype_id').append('<option value="null">-- กรุณาเลือก --</option>');
					$.each(obj, function(i,v){
						if(v['drbtype_id'] == "<?php echo $_POST['drbtype_id'] ?>"){
							$('#drbtype_id').append('<option value="'+v['drbtype_id']+'" selected>'+v['drbtype_name']+'</option>');
						}else{
							$('#drbtype_id').append('<option value="'+v['drbtype_id']+'">'+v['drbtype_name']+'</option>');
						}
					});
				}
			});
		}
	</script>
	<!-- InstanceEndEditable -->
</head>
<body>
	<div id="header"></div>
    <div id="menu">
    	<?php
			include 'login.php';
		?>
    </div>
    <div id="middle">
    	<div id="info"><!-- InstanceBeginEditable name="content" -->
        	<fieldset>
            	<legend>ค้นหาครุภัณฑ์</legend>
                <form action="" method="get">
                	<label>ตั้งแต่วันที่</label><input class="datepicker" name="start" />
                    <label>ถึงวันที่</label><input class="datepicker" name="end" />
                    <label>ประเภท</label><select id="drbtype_id" name="drbtype_id"></select>
                	<button id="btn_search">ค้นหา</button>
                </form>
            </fieldset>
            <fieldset>
            	<legend>ผลการค้นหา</legend>
                <form action="report_dispose.php?start=<?=$_REQUEST['start'];?>&end=<?=$_REQUEST['end'];?>&drbtype_id=<?=$_REQUEST['drbtype_id'];?>" method="post">
                	<?php
						$page = $exec->getstart();
						
						$start = explode('-', $_GET['start']);
						$end = explode('-', $_GET['end']);
						$qry = $exec->genpage("SELECT * FROM drbno, drb, drbstatus, respons, drbdispose WHERE drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND respons.resp_id=drbno.resp_id AND drbno.drbno_id=drbdispose.drbno_id AND drbdispose.dispose_date BETWEEN '$_GET[start]' AND '$_GET[end]' AND drb.drbtype_id='$_GET[drbtype_id]' AND drbno.drbno_status='3' ORDER BY drbno.drbno_id ASC");
					?>
                	<table id="tbldrbno">
                    	<thead>
                        	<th>ลำดับที่</th>
                            <th>หมายเลขครุภัณฑ์</th>
                            <th>ชื่อครุภัณฑ์</th>
                            <th>ผู้รับผิดชอบ</th>
                            <th>วันที่รับครุภัณฑ์</th>
                            <th>ระยะเวลา<br />รับประกัน</th>
                            <th>อายุการใช้งาน</th>
                        </thead>
                        <tbody>
                        	<?php
								while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
									$page++;
									echo "<tr><td>$page</td><td>$rs[drbno_number]</td><td>$rs[drb_name]</td><td>$rs[resp_name]</td><td>$rs[drb_receivedate]</td><td>$rs[drb_warrenty]</td><td>$rs[drb_lt]</td></tr>";
								}
							?>
                        </tbody>
                    </table>
                    <?php
                    	$exec->link();
					?><br />
                    <button>พิมพ์รายงาน</button>
                </form>
            </fieldset>
		<!-- InstanceEndEditable --></div>
    </div>
    <div id="footer"></div>
</body>
<!-- InstanceEnd --></html>
