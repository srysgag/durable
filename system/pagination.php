<?php
    class pagination{ 
        public $item_per_page = 20, $prev_page, $next_page, $first_page, $last_age, $cur_page, $num_rows, $start; 
          
        function __construct(){ 
            if(empty($_GET['query'])) 
                $this->cur_page = 1; 
            else
                $this->cur_page = $_GET['query']; 
				
            $this->next_page = $this->cur_page + 1; 
            $this->prev_page = $this->cur_page - 1; 
        } 
          
        function genpage($strSQL){
			$qry = database::execute($strSQL);
			$this->num_rows = mysqli_num_rows($qry);
			$this->start = ($this->cur_page - 1) * $this->item_per_page;
			
			$qry = database::execute("$strSQL LIMIT $this->start, $this->item_per_page");
			return $qry;
        }
		
		function getstart(){
			if(!empty($this->start)){
				return $this->start;
			}
		}
          
        function link(){
			$split = explode('/', $_SERVER['PHP_SELF']);
			#echo $split[2];
			if($split[2] == 'display.php'){
				if($this->cur_page == 1){
					if($this->num_rows > $this->item_per_page){
						echo $this->cur_page;
						echo ' <a href="'.$_SERVER['PHP_SELF'].'?search='.$_GET['search'].'&opt_search='.$_GET['opt_search'].'&query='.$this->next_page.'">'.$this->next_page.'</a>'; 
					}
				}elseif($this->cur_page >= ceil($this->num_rows / $this->item_per_page)){
					echo '<a href="'.$_SERVER['PHP_SELF'].'?search='.$_GET['search'].'&opt_search='.$_GET['opt_search'].'&query='.$this->prev_page.'">'.$this->prev_page.'</a>'; 
					echo ' '.$this->cur_page; 
				}else{ 
					echo '<a href="'.$_SERVER['PHP_SELF'].'?search='.$_GET['search'].'&opt_search='.$_GET['opt_search'].'&query='.$this->prev_page.'">'.$this->prev_page.'</a>'; 
					echo ' '.$this->cur_page.' '; 
					echo '<a href="'.$_SERVER['PHP_SELF'].'?search='.$_GET['search'].'&opt_search='.$_GET['opt_search'].'&query='.$this->next_page.'">'.$this->next_page.'</a>'; 
				}
			}else{
				if($this->cur_page == 1){
					if($this->num_rows > $this->item_per_page){
						echo $this->cur_page;
						echo ' <a href="'.$_SERVER['PHP_SELF'].'?search='.$_GET['search'].'&query='.$this->next_page.'">'.$this->next_page.'</a>'; 
					}
				}elseif($this->cur_page >= ceil($this->num_rows / $this->item_per_page)){
					echo '<a href="'.$_SERVER['PHP_SELF'].'?search='.$_GET['search'].'&query='.$this->prev_page.'">'.$this->prev_page.'</a>'; 
					echo ' '.$this->cur_page; 
				}else{ 
					echo '<a href="'.$_SERVER['PHP_SELF'].'?search='.$_GET['search'].'&query='.$this->prev_page.'">'.$this->prev_page.'</a>'; 
					echo ' '.$this->cur_page.' '; 
					echo '<a href="'.$_SERVER['PHP_SELF'].'?search='.$_GET['search'].'&query='.$this->next_page.'">'.$this->next_page.'</a>'; 
				}
			}
        } 
    }
?>