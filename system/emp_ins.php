<?php
	require_once 'database.php';
    session_start();
	ob_start();
    $exec = database::getInstance()
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/prototype.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ระบบจัดการครุภัณฑ์ คณะบริหารธุรกิจ มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.min.css" />
    <script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <!-- InstanceBeginEditable name="head" -->
    <script type="text/javascript">
		function readFile(input){
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#myimg').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
	</script>
	<!-- InstanceEndEditable -->
</head>
<body>
	<div id="header"></div>
    <div id="menu">
    	<?php
			include 'login.php';
		?>
    </div>
    <div id="middle">
    	<div id="info"><!-- InstanceBeginEditable name="content" -->
        	<form action="" method="post" enctype="multipart/form-data">
        		<fieldset>
                    <legend>ข้อมูลผู้ใช้งานระบบ</legend>
                    <div class="box2">
                    	<label class="lbl">ชื่อผู้ใช้งาน</label><input name="auth_user" /><br />
                        <label class="lbl">รหัสผ่าน</label><input name="auth_pass" type="password" /><br />
                        <label class="lbl">ชื่อ</label ><input name="emp_fname" /><br />
                        <label class="lbl">นามสกุล</label><input name="emp_lname" /><br />
                        <label class="lbl">เบอร์โทรศัพท์</label><input name="emp_tel" /><br />
                    </div>
                    <div class="box3">
                        <img id="myimg" />
                        <input name="emp_img" type="file" onchange="readFile(this);" />
                    </div>
                     <button>บันทึกข้อมูล</button>
                </fieldset>
            </form>
		<!-- InstanceEndEditable --></div>
    </div>
    <div id="footer"></div>
</body>
<!-- InstanceEnd --></html>

<?php
	if(!empty($_POST)){

		if(empty($_POST['auth_user'])){
			
		}elseif(empty($_POST['auth_pass'])){
			
		}elseif(empty($_POST['emp_fname'])){
			
		}elseif(empty($_POST['emp_lname'])){
			
		}elseif(empty($_POST['emp_tel'])){
			
		}elseif($_FILES['emp_img']['error'] > 0){
			
		}else{
			if($_FILES['emp_img']['type'] == 'image/jpeg'){
				$qry = $exec->execute("SELECT max(auth_id) as max_id FROM auth");
				$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
				if(!empty($rs['max_id'])){
					$split = explode('-', $rs['max_id']);
					$today = explode('-', date('Y-m-d'));
					$gen = substr($split[1], 1)+1;
					$id = sprintf('%002.0f', $gen);
					$auth_id = $today[1].$today[2].$today[0].'-'.$id;
				}else{
					$today = explode('-', date('Y-m-d'));
					$auth_id = $today[1].$today[2].$today[0].'-01';
				}
				
				$exec->execute("INSERT INTO auth VALUES('$auth_id','$_POST[auth_user]','$_POST[auth_pass]','2')");
				
				$qry = $exec->execute("SELECT max(emp_id) as max_id FROM employee");
				$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
				if(!empty($rs['max_id'])){
					$gen = substr($rs['max_id'], 1)+1;
                    $emp_id = sprintf('E%003.0f', $gen);
				}else{
					$emp_id = 'E001';
				}
				
				$tragetfolder = 'emp_img/'.$auth_id.'.jpg';
				move_uploaded_file($_FILES['emp_img']['tmp_name'], $tragetfolder);
				
				$exec->execute("INSERT INTO employee VALUES('$emp_id','$_POST[emp_fname]','$_POST[emp_lname]','$_POST[emp_tel]','$tragetfolder','$auth_id')");
				// echo "INSERT INTO employee VALUES('$emp_id','$_POST[emp_fname]','$_POST[emp_lname]','$_POST[emp_tel]''$tragetfolder','$auth_id')";
				echo 'เพิ่มข้อมูลพนักงานเรียบร้อยแล้ว';
			}
		}
	}
?>
