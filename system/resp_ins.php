<?php
	require_once('database.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/style.css" />
	<script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
</head>
<body id="popup">
	<fieldset>
    	<legend>ผู้รับผิดชอบ</legend>
        <form action="#" method="POST">
            <label class="lbl">ชื่อ</label><input name="resp_name" type="text" value="<?php if(!empty($_POST['resp_name'])) echo $_POST['resp_name']; ?>" /><p class="description"><?php if(empty($_POST['resp_name'])) echo 'กรุณากรอกข้อมูล'; ?></p><br />
            <label class="lbl">สาขา</label><input name="resp_branch" type="text" value="<?php if(!empty($_POST['resp_branch'])) echo $_POST['resp_branch']; ?>" /><p class="description"><?php if(empty($_POST['resp_branch'])) echo 'กรุณากรอกข้อมูล'; ?></p><br />
            <label class="lbl">เบอร์โทร</label><input name="resp_tel" type="text" value="<?php if(!empty($_POST['resp_tel'])) echo $_POST['resp_tel']; ?>" /><p class="description"><?php if(empty($_POST['resp_tel'])) echo 'กรุณากรอกข้อมูล'; ?></p><br />
            <label class="lbl">ห้องพัก</label><input name="resp_room" type="text" value="<?php if(!empty($_POST['resp_room'])) echo $_POST['resp_room']; ?>" /><p class="description"><?php if(empty($_POST['resp_room'])) echo 'กรุณากรอกข้อมูล'; ?></p><br />
            <input type="submit" value="เพิ่มข้อมูล" />
        </form>
    </fieldset>
</body>
<?php
	if(!empty($_POST)){
		if(empty($_POST['resp_name'])){
		}elseif(empty($_POST['resp_branch'])){
		}elseif(empty($_POST['resp_tel'])){
		}elseif(empty($_POST['resp_room'])){
		}else{
			$exec = database::getInstance();
			$qry = $exec->execute('SELECT MAX(resp_id) AS max_id FROM respons');
			$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
			if(!empty($rs['max_id'])){
				$gen = substr($rs['max_id'],1)+1;
				$resp_id = sprintf('R%003.0f',$gen);
			}else{
				$resp_id = 'R001';
			}
			$exec->execute("INSERT INTO respons VALUES('$resp_id','$_POST[resp_name]','$_POST[resp_branch]','$_POST[resp_tel]','$_POST[resp_room]')");
			echo 'เพิ่มข้อมูลผูู้ดูแลเรียบร้อยแล้ว';
			echo '<script>window.close();</script>';
		}
	}else{
		echo 'กรุณากรอกข้อมูลให้ครบถ้วน';
	}
?>
</html>