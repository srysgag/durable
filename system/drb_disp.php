<?php
	require_once 'database.php';
    session_start();
	ob_start();
    $exec = database::getInstance();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/prototype.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ระบบจัดการครุภัณฑ์ คณะบริหารธุรกิจ มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.min.css" />
    <script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <!-- InstanceBeginEditable name="head" -->
    <script>
		$(document).ready(function(){
			$('#check_all').click(function () {    
				 $('input:checkbox').prop('checked', this.checked);    
			});
            $('#btn_back').click(function(){ window.location = 'drb_ins.php'; });
        });
	</script>
	<!-- InstanceEndEditable -->
</head>
<body>
	<div id="header"></div>
    <div id="menu">
    	<?php
			include 'login.php';
		?>
    </div>
    <div id="middle">
    	<div id="info"><!-- InstanceBeginEditable name="content" -->
        	<fieldset>
                <legend>รายการครุภัณฑ์</legend>
                <!--<iframe width="987" src="drb_disp.php"></iframe>-->
                <form action="drbstorage_ins.php" method="post">
                	<?php
						$qry = $exec->genpage("SELECT * FROM drb,drbtype,drbno WHERE drb.drb_id=drbno.drb_id AND drbtype.drbtype_id=drb.drbtype_id AND drbno.drbno_status='0' GROUP BY drb.drb_id");
                    	$start = $exec->getstart();
					?>
                    <table id="tbldoc">
                    	<thead>
                            <th width="55">ลำดับที่</th>
                            <th width="110">เลขที่เอกสาร</th>
                            <th width="150">รายละเอียด</th>
                            <th width="150">ประเภทครุภัณฑ์</th>
                            <th width="150">ชื่อครุภัณฑ์</th>
                            <th>ยี่ห้อ</th>
                            <th>รุ่น</th>
                            <th>จำนวน</th>
                            <th width="150">ราคาต่อหน่วย</br>(+vat 7%)</th>
                            <th width="150">ราคารวม</th>
                            <th>เลือก</th>
                        </thead>
                        <tbody>
                        	<?php
								while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
									$start++;
									echo "<tr><td>$start</td><td>$rs[drb_no]</td><td>$rs[drb_detail]</td><td>$rs[drbtype_name]</td><td>$rs[drb_name]</td><td>$rs[drb_band]</td><td>$rs[drb_modal]</td><td>$rs[drb_import]</td><td style=text-align:right>".number_format($rs['drb_price'], 2, '.', ',')."</td><td style=text-align:right>".number_format($rs['drb_total'], 2, '.', ',')."</td><td><input name=drb_id[] type=checkbox value=$rs[drb_id] /></td></tr>";
								}
							?>
                        </tbody>
                        <tr><td colspan="8" style="text-align:right";>เลือกทั้งหมด</td><td><input id="check_all" type="checkbox" /></td></tr>
                    </table>
                    <?php
                    	$exec->link();
					?><br />
                    
                    <button>จัดเก็บครุภัณฑ์</button><br/><br/>
                    <center>
                    <a href="drb_ins.php" style="background-color:#E0FFFF">ย้อนกลับ</a>
                    </center>
                </form>
            </fieldset>
		<!-- InstanceEndEditable --></div>
    </div>
    <div id="footer"></div>
</body>
<!-- InstanceEnd --></html>
