<?php
	require_once 'database.php';
	require_once 'log.php';
    session_start();
	ob_start();
    $exec = database::getInstance();
	
	$qry = $exec->execute("SELECT * FROM drbno, drb, drbstatus, agent WHERE agent.agent_id=drb.agent_id AND drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND drbno.drbno_status='1' AND drbno.drbno_id='".$_POST[drbno_id][0]."'");
	#echo "SELECT * FROM drbno, drb, drbstatus, agent WHERE agent.agent_id=drb.agent_id AND drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND drbno.drbno_status='1' AND drbno.drbno_id='".$_POST[drbno_id][0]."'";
	$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
	$agent_name = $rs['agent_name'];
	$flag = true;
	$repair = true;
	
	for($i = 1; $i < count($_POST['drbno_id']); $i++){
		$qry = $exec->execute("SELECT * FROM drbno, drb, drbstatus, agent , respons WHERE agent.agent_id=drb.agent_id AND drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND drbno.drbno_status='1' AND drbno.drbno_id='".$_POST['drbno_id'][$i]."'");
		$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
		if($agent_name == $rs['agent_name']){
			$flag = true;
		}else{
			$flag = false;
		}
	}
	
	if($flag != true){
		echo "<script>alert('ไม่สามารถทำรายการนี้ได้');</script>";
		header("refresh:0;drbrepair_disp.php");
	}else{
		$qry = $exec->execute("SELECT * FROM drbno, drb, drbstatus, agent WHERE agent.agent_id=drb.agent_id AND drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND drbno.drbno_status='1' AND drbno.drbno_id='".$_POST[drbno_id][0]."'");
		#echo "SELECT * FROM drbno, drb, drbstatus, agent WHERE agent.agent_id=drb.agent_id AND drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND drbno.drbno_status='1' AND drbno.drbno_id='".$_POST[drbno_id][0]."'";
		$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
		
		$culdate = date('Y-m-d', strtotime("+$rs[drb_warrenty] year", strtotime($rs['drb_receivedate'])));
        $nowdate = date('Y-m-d');
		if($culdate > $nowdate){
			$repair = true;
			
		}else{
			$repair = false;
		}
	}
	
	/*foreach($_POST['drbno_id'] as $v){
		$qry = $exec->execute("SELECT * FROM drbno, drb, drbstatus, agent , respons WHERE agent.agent_id=drb.agent_id AND drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND drbno.drbno_status='1' AND drbno.drbno_id='$v'");
		
	}*/
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/prototype.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ระบบจัดการครุภัณฑ์ คณะบริหารธุรกิจ มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.min.css" />
    <script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <!-- InstanceBeginEditable name="head" -->
    <style type="text/css">
		.ui-tabs{ font-family:tahoma; font-size:11px; }
		.ui-datepicker{ width:220px; font-family:tahoma; font-size:11px; text-align:center; }
	</style>
    <script>
		$(document).ready(function(e) {
			$('#check_all').click(function () {    
				 $('input:checkbox').prop('checked', this.checked);    
			});   
			
			$('.formatint').on('keypress',function(){
				if (event.keyCode < 48 || event.keyCode > 57) {
					event.preventDefault();
				}
			});
			
			$('.formatstring').on('keypress',function(){
				console.log(event.keyCode);
				if ((event.keyCode < 97 || event.keyCode > 122) && ((event.keyCode < 3585 || event.keyCode > 3660))) {
					event.preventDefault();
				}
			});
        });
		$(function(){
			$('.datepicker').datepicker({
				dateFormat: 'dd-mm-yy',
				dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
				monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
				changeMonth: true,  
				changeYear: true,
				beforeShow:function(){    
					if($(this).val() != ''){  
						var arrayDate=$(this).val().split('-');       
						arrayDate[2]=parseInt(arrayDate[2]);  
						$(this).val(arrayDate[0]+'-'+arrayDate[1]+'-'+arrayDate[2]);  
					}  
					setTimeout(function(){
						$.each($('.ui-datepicker-year option'),function(j,k){  
							var textYear=parseInt($('.ui-datepicker-year option').eq(j).val());  
							$('.ui-datepicker-year option').eq(j).text(textYear);  
						});               
					},50);  
				},  
				onChangeMonthYear: function(){
					setTimeout(function(){
						$.each($('.ui-datepicker-year option'),function(i,v){  
							var textYear=parseInt($('.ui-datepicker-year option').eq(i).val());  
							$('.ui-datepicker-year option').eq(i).text(textYear);  
						});               
					},50);        
				},  
				onClose:function(){
					if($(this).val() != '' && $(this).val() == dateBefore){           
						var arrayDate=dateBefore.split('-');  
						arrayDate[2]=parseInt(arrayDate[2]);  
						$(this).val(arrayDate[0]+'-'+arrayDate[1]+'-'+arrayDate[2]);      
					}         
				},  
				onSelect: function(dateText, inst){
					dateBefore=$(this).val();  
					var arrayDate=dateText.split('-');  
					arrayDate[2]=parseInt(arrayDate[2]);  
					$(this).val(arrayDate[0]+'-'+arrayDate[1]+'-'+arrayDate[2]);  
				}
			});
		});
	</script>
	<!-- InstanceEndEditable -->
</head>
<body>
	<div id="header"></div>
    <div id="menu">
    	<?php
			include 'login.php';
		?>
    </div>
    <div id="middle">
    	<div id="info"><!-- InstanceBeginEditable name="content" -->
        	<form action="" method="post">
                <fieldset class="box1">
                    <legend>ข้อมูลผู้รับซ่อมครุภัณฑ์</legend>
                    <label class="lbl">บริษัท</label><input id="repair_company" name="repair_company" type="text" value="<?=$rs['agent_name'];?>" <?php if($repair == true) echo "readonly=\"readonly\"";?> /><br />
                    <label class="lbl">ที่อยู่</label><input id="repair_addr" name="repair_addr" type="text" value="<?=$rs['agent_addr'];?>" <?php if($repair == true) echo "readonly=\"readonly\"";?> /><br />
                    <label class="lbl">ผู้ติดต่อ</label><input class="formatstring" id="repair_contact" name="repair_contact" type="text" value="<?=$rs['agent_contact'];?>" <?php if($repair == true) echo "readonly=\"readonly\"";?> /><br />
                    <label class="lbl">เบอร์โทรศัพท์</label><input class="formatint" id="repair_contacttel" name="repair_contacttel" type="text" value="<?=$rs['agent_telcontac'];?>" <?php if($repair == true) echo "readonly=\"readonly\"";?> />
                </fieldset>
                <fieldset class="box1">
                    <legend>ข้อมูลผู้ส่งซ่อมครุภัณฑ์</legend>
                    <label class="lbl">ผู้ส่งซ่อม</label><input class="formatstring" id="repair_name" class="autocomplete" name="repair_name" /><br />
                    <label class="lbl">เบอร์โทรศัพท์</label><input class="formatint" id="repair_tel" name="repair_tel" /><br />
                    <label class="lbl">อาการที่เสีย</label><input id="repair_symptoms" name="repair_symptoms" /><br />
                    <label class="lbl">วันที่ส่งซ่อม</label><input id="repair_datesend" class="datepicker" name="repair_datesend" />
                </fieldset>
                <fieldset>
                    <legend>ข้อมูลครุภัณฑ์</legend>
                        <table id="tbldrbno">
                            <thead>
                                <th>ลำดับที่</th>
                                <th>หมายเลขครุภัณฑ์</th>
                                <th width="150">ชื่อครุภัณฑ์</th>
                                <th width="150">สถานะประกัน</th>
                                <th width="200">ผู้ขาย</th>
                                 <th width="200">ที่อยู่</th>
                                <th>เลือก</th>
                            </thead>
                            <tbody>
                                <?php
                                    if(empty($_SESSION['drbno_id']))
                                        $_SESSION['drbno_id'] = $_POST['drbno_id'];
											
                                    $i = 0;
                                    foreach($_SESSION['drbno_id'] as $v){
                                        $qry = $exec->execute("SELECT * FROM drbno, drb, drbstatus, agent , respons WHERE agent.agent_id=drb.agent_id AND drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND respons.resp_id=drbno.resp_id AND drbno.drbno_status='1' AND drbno.drbno_id='$v' ORDER BY drbno_id ASC");
                                        if(mysqli_num_rows($qry) != 0){
                                            while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
                                                $i++;
                                                $culdate = date('Y-m-d', strtotime("+$rs[drb_warrenty] year", strtotime($rs['drb_receivedate'])));
                                                $nowdate = date('Y-m-d');
                                                if($culdate > $nowdate)
                                                	echo "<tr><td>$i</td><td>$rs[drbno_number]</td><td>$rs[drb_name]</td><td>อยู่ในระยะรับประกัน</td><td>$rs[agent_name]</td><td>$rs[agent_addr]</td><td><input name=\"drbno_id[]\" type=\"checkbox\" value=\"$rs[drbno_id]\" /></td><tr>";
												else
                                                	echo "<tr><td>$i</td><td>$rs[drbno_number]</td><td>$rs[drb_name]</td><td>หมดรับประกัน</td><td>$rs[agent_name]</td><td>$rs[agent_addr]</td><td><input name=\"drbno_id[]\" type=\"checkbox\" value=\"$rs[drbno_id]\" /></td><tr>";
										    }	
                                        }
                                    }
									if($i == 0){
                                        unset($_SESSION['drbno_id']);
                                    	header('location:drbrepair_disp.php');
                                    }
                                ?>
                                <tr><td colspan="6">เลือกทั้งหมด</td><td><input id="check_all" type="checkbox" /></td></tr>
                            </tbody>
                        </table>
                      <center>  <button name="btn" value="save">บันทึก</button><button name="btn" value="back">ย้อนกลับ</button> </center>
                </fieldset>
                </form>
                
                <?php
					if($_POST['btn'] == 'save'){
						/*if(empty($_POST['resp_id'])){
							#echo 'empty control name resp_id.';
						}elseif($_POST['room_id'] == 'null'){
							#echo 'not choose room_id.';
						}elseif(count($_POST['drbno_id']) == 0){
							#echo 'not choose durable for storage.';
						}else{*/
						$convert_date = explode('-', $_POST['repair_datesend']);
                        $repair_datesend = $convert_date[2].'-'.$convert_date[1].'-'.$convert_date[0];
							$flag = false;
							foreach($_POST['drbno_id'] as $v){
								$qry = $exec->execute("SELECT * FROM drb, drbno WHERE drb.drb_id=drbno.drb_id AND drbno.drbno_id='$v'");
								#echo "SELECT * FROM drb, drbno WHERE drb.drb_id=drbno.drb_id AND drbno.drbno_id=$v";
								$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
								$temp_receive = date('Y-m-d', strtotime($rs['drb_receivedate']));
								$temp_send = date('Y-m-d', strtotime($repair_datesend));
								
								if($temp_receive <= $temp_send){
									$exec->execute("UPDATE drbno SET drbno_status='2' WHERE drbno_id='$v'");
									$qry = $exec->execute('SELECT MAX(repair_id) AS max_id FROM drbrepair');
									$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
									if(!empty($rs['max_id'])){
										$gen = substr($rs['max_id'], 1)+1;
										$repair_id = sprintf('R%009.0f', $gen);
									}else{
										$repair_id = 'R000000001';
									}								
									
									$exec->execute("INSERT INTO drbrepair VALUES('$repair_id','$_POST[repair_company]','$_POST[repair_addr]','$_POST[repair_contact]','$_POST[repair_contacttel]','$_POST[repair_name]','$_POST[repair_tel]','$_POST[repair_symptoms]','$repair_datesend','0000-00-00','0.00','$v')");
									$log->write_log($_SESSION['auth']['name'].'->dulable_repair:'.$v);
									
									
									#echo $temp_send.$temp_receive;
									#
									$flag = true;
								}else{
									echo "<script>alert('ครุภัณฑ์หมายเลข".$rs[drbno_number].$flag." วันที่ส่งซ่อมน้อยกว่าวันที่รับ');</script>";
									break;
									$flag = false;
								}
							}
							if ($flag==true){
							unset($_POST);
							echo "<script>alert('ส่งซ่อมครุภัณฑ์เรียบร้อยแล้ว');</script>";
							header("refresh:0;drbrepair_ins.php");
							}
                 		    
					}elseif($_POST['btn'] == 'back'){
						unset($_SESSION['drbno_id']);
						header('location:drbrepair_disp.php');
					}
				?>
		<!-- InstanceEndEditable --></div>
    </div>
    <div id="footer"></div>
</body>
<!-- InstanceEnd --></html>
