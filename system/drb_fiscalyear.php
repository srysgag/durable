<?php
	require_once 'database.php';
    session_start();
	ob_start();
    $exec = database::getInstance()
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/prototype.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ระบบจัดการครุภัณฑ์ คณะบริหารธุรกิจ มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.min.css" />
    <script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <!-- InstanceBeginEditable name="head" -->
	<!-- InstanceEndEditable -->
</head>
<body>
	<div id="header"></div>
    <div id="menu">
    	<?php
			include 'login.php';
		?>
    </div>
    <div id="middle">
    	<div id="info"><!-- InstanceBeginEditable name="content" -->
        	<fieldset>
            	<legend>ค้นหาคุรภัณฑ์</legend>
                <form action="" method="get">
                	<label class="lbl">ปีงบประมาณ</label><input name="year"/>
                	<button id="btn_search">ค้นหา</button>
                </form>
            </fieldset>
            <fieldset>
            	<legend>ผลการค้นหา</legend>
                <form action="report_fiscalyear.php?year=<?=$_REQUEST[year];?>" method="post">
                	<?php
						$qry = $exec->genpage("SELECT * FROM drb, drbno, area, building, floor, room, respons WHERE drb.drb_id=drbno.drb_id AND building.area_id=area.area_id AND floor.building_id=building.building_id AND room.floor_id=floor.floor_id AND drbno.room_id=room.room_id AND drbno.resp_id=respons.resp_id AND drbno.drbno_status='1' AND drb.typeof_detail='$_REQUEST[year]' ORDER BY drbno.drbno_id");
                    	$start = $exec->getstart();
					?>
                	<center><table id="tbldrbno">
                    	<thead>
                        	<th>ลำดับที่</th>
                            <th>หมายเลขครุภัณฑ์</th>
                            <th>ชื่อครุภัณฑ์</th>
                            <th>ยี่ห้อ</th>
                            <th>ผู้รับผิดชอบ</th>
                            <th>วันที่รับครุภัณฑ์</th>
                            <th>ระยะเวลา<br />รับประกัน</th>
                            
                        </thead>
                        <tbody>
                        	<?php
								while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
									$start++;
									echo "<tr><td>$start</td><td>$rs[drbno_number]</td><td>$rs[drb_name]</td><td>$rs[drb_band]</td><td>$rs[resp_name]</td><td>$rs[drb_receivedate]</td><td>$rs[drb_warrenty]</td></tr>";
								}
							?>
                        </tbody>
                    </table>
                    <?php
                    	$exec->link();
					?><br /></center>
                    <button>พิมพ์รายงาน</button>
                </form>
            </fieldset>
		<!-- InstanceEndEditable --></div>
    </div>
    <div id="footer"></div>
</body>
<!-- InstanceEnd --></html>
