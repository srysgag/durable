<?php
	require_once 'database.php';
    session_start();
	ob_start();
    $exec = database::getInstance();
	unset($_SESSION['drbno_id']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/prototype.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ระบบจัดการครุภัณฑ์ คณะบริหารธุรกิจ มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.min.css" />
    <script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <!-- InstanceBeginEditable name="head" -->
    <style type="text/css">
		.ui-menu .ui-menu-item a{ height:14px; font-family:tahoma; font-size:12px; }
	</style>
    <script>
		$(document).ready(function(){
			$('#check_all').click(function () {    
				 $('input:checkbox').prop('checked', this.checked);    
			});
            /*$('#btn_search').click(function(){
				if($('#drbno_number').val().length == 0){
				}else{
					$.ajax({
						type: 'POST',
						url: 'load_contr',
						data: { method: 'drbno', drbno_number: $('#drbno_number').val() },
						success: function(data){
							var obj = $.parseJSON(data);
							$.each(obj, function(i, v){
								var count = $('#tbldrbno>tbody tr').length;
								$('#tbldrbno>tbody').append('<tr><td>'+(count+1)+'</td><td>'+v['drbno_number']+'</td><td>'+v['drb_name']+'<input name="drbno_id[]" type="hidden" value="'+v['drbno_id']+'" readonly /></td><td>'+v['resp_name']+'</td><td>'+v['drb_receivedate']+'</td><td>'+v['drb_warrenty']+'</td><td>'+v['drb_lt']+'</td><td><button onclick="javascript:doRemoveItem(this);">ลบ</button></td></tr>');
							});
							$('#drbno_number').val('');
						}
					});
				}
			});*/
        });
		
		$(function(){
			$('.autocomplete').autocomplete({
				source: 'autocomplete.php?method=drbno',
				minLength: 1,
				focus: function(event, ui){
					$('.autocomplete').val(ui.item.drbno_number);
				},
				select: function(event, ui){
					$('#drbno_number').val(ui.item.drbno_number);
					/*$('#agent_tel').val(ui.item.agent_tel);
					$('#agent_id').val(ui.item.agent_id);*/
					return false;
				}
			})
			.data('ui-autocomplete')._renderItem = function(ul,item){
				return $('<li>')
					.append('<a>'+item.drbno_number+'</a>')
					.appendTo(ul);
			};
		});
		
		function doRemoveItem(obj) {
			$(obj).parent().parent().remove();
		}
	</script>
	<!-- InstanceEndEditable -->
</head>
<body>
	<div id="header"></div>
    <div id="menu">
    	<?php
			include 'login.php';
		?>
    </div>
    <div id="middle">
    	<div id="info"><!-- InstanceBeginEditable name="content" -->
        	<fieldset>
            	<legend>ค้นหาคุรภัณฑ์</legend>
                <form action="" method="get">
                <label class="lbl">หมายเลขครุภัณฑ์</label>
                <input id="drbno_number" class="autocomplete" name="search" type="text" />
                <button id="btn_search">ค้นหา</button>
                </form>
            </fieldset>
            <fieldset>
            	<legend>ผลการค้นหา</legend>
                <form action="drbrepair_ins.php" method="post">
                	<?php
						$qry = $exec->genpage("SELECT * FROM drb, drbno, area, building, floor, room, agent WHERE drb.drb_id=drbno.drb_id AND building.area_id=area.area_id AND floor.building_id=building.building_id AND room.floor_id=floor.floor_id AND drbno.room_id=room.room_id AND drb.agent_id=agent.agent_id AND drbno.drbno_status='1' AND drbno.drbno_number LIKE '%$_REQUEST[search]%' ORDER BY drbno.drbno_id");
                    	$start = $exec->getstart();
					?>
                	<table id="tbldrbno">
                    	<thead>
                        	<th>ลำดับที่</th>
                            <th>หมายเลขครุภัณฑ์</th>
                            <th>ชื่อครุภัณฑ์</th>
                            <th>วันที่รับครุภัณฑ์</th>
                			<th>ผู้ขาย/ผู้รับจ้าง/ผู้บริจาค</th>
                            <th>เลือก</th>
                        </thead>
                        <tbody>
                        	<?php
								while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
									$start++;
									echo "<tr><td>$start</td><td>$rs[drbno_number]</td><td>$rs[drb_name]</td><td>$rs[drb_receivedate]</td><td>$rs[agent_name]</td><td><input name=\"drbno_id[]\" type=\"checkbox\" value=\"$rs[drbno_id]\" /></td></tr>";
								}
							?>
                            <tr><td colspan="5">เลือกทั้งหมด</td><td><input id="check_all" type="checkbox" /></td></tr>
                        </tbody>
                    </table>
                    <?php
                    	$exec->link();
					?><br />
                    <center><button>ตกลง</button></center>
                </form>
            </fieldset>
		<!-- InstanceEndEditable --></div>
    </div>
    <div id="footer"></div>
</body>
<!-- InstanceEnd --></html>
