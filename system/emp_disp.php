<?php
	require_once 'database.php';
	require_once 'log.php';
    session_start();
	ob_start();
    $exec = database::getInstance()
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/prototype.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ระบบจัดการครุภัณฑ์ คณะบริหารธุรกิจ มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.min.css" />
    <script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <!-- InstanceBeginEditable name="head" -->
    <style type="text/css">
		.ui-menu .ui-menu-item a{ height:14px; font-family:tahoma; font-size:12px; }
	</style>
    <script>
		$(document).ready(function(){
            /*$('#btn_search').click(function(){
				if($('#drb_no').val().length == 0){
				}else{
					$.ajax({
						type: 'POST',
						url: 'load_contr',
						data: { method: 'drb', drb_no: $('#drb_no').val() },
						success: function(data){
							var obj = $.parseJSON(data);
							$.each(obj, function(i, v){
								var count = $('#tbldoc>tbody tr').length;
								$('#tbldoc>tbody').append('<tr><td>'+(count+1)+'</td><td>'+v['drb_no']+'</td><td>'+v['drb_detail']+'</td><td>'+v['drbtype_name']+'</td><td>'+v['drb_name']+'</td><td>'+v['drb_import']+'</td><td>'+v['drb_receivedate']+'</td><td><button name="btn_edit" value="'+v['drb_id']+'">แก้ไข</button></td></tr>');
							});
							$('#drb_no').val('');
						}
					});
				}
			});*/
        });
		
		/*$(function(){
			$('.autocomplete').autocomplete({
				source: 'autocomplete.php?method=emp',
				minLength: 1,
				focus: function(event, ui){
					$('.autocomplete').val(ui.item.name);
				},
				select: function(event, ui){
					$('#drb_no').val(ui.item.name);
					return false;
				}
			})
			.data('ui-autocomplete')._renderItem = function(ul,item){
				return $('<li>')
					.append('<a>'+item.name+'</a>')
					.appendTo(ul);
			};
		});
		
		function doRemoveItem(obj) {
			$(obj).parent().parent().remove();
		}*/
	</script>
	<!-- InstanceEndEditable -->
</head>
<body>
	<div id="header"></div>
    <div id="menu">
    	<?php
			include 'login.php';
		?>
    </div>
    <div id="middle">
    	<div id="info"><!-- InstanceBeginEditable name="content" -->
        	<fieldset>
            	<legend>ค้นหาผู้ใช้ระบบ</legend>
                <form action="" method="get">
                	<label class="lbl">เลือกประเภทผู้ใช้งาน</label>
                    <select name="perm_id">
                    	<option value="2">เจ้าหน้าที่ครุภัณฑ์</option>
                        <option value="3">ผู้ใช้งานทั่วไป</option>
                    </select>
                    <button id="btn_search">ค้นหา</button>
                </form>
            </fieldset>
        	<fieldset>
                <legend>ผลการค้นหา</legend>
                <form action="" method="post">
                	<?php
                    	#echo "SELECT * FROM perm, auth, employee WHERE perm.perm_id=auth.perm_id AND auth.auth_id=employee.auth_id AND (emp_fname LIKE '%$_REQUEST[search]%' OR emp_lname LIKE '%$_REQUEST[search]%') GROUP BY employee.emp_id";
						$qry = $exec->genpage("SELECT * FROM perm, auth, employee WHERE perm.perm_id=auth.perm_id AND auth.auth_id=employee.auth_id AND auth.perm_id='$_GET[perm_id]' GROUP BY employee.emp_id");
                    	$start = $exec->getstart();
					?>
                    <table id="tbldoc">
                    	<thead>
                            <th>ลำดับที่</th>
                            <th>ชื่อผู้ใช้งาน</th>
                            <th>รหัสผ่าน</th>
                            <th>ชื่อ - นามสกุล</th>
                            <th>เบอร์โทรศัพท์</th>
                            <th>แก้ไข</th>
                        </thead>
                        <tbody>
                        	<?php
								while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
									$start++;
									if($_GET['perm_id'] == '2')
										echo "<tr><td>$start</td><td>$rs[auth_user]</td><td>$rs[auth_pass]</td><td>$rs[emp_fname] $rs[emp_lname]</td><td>$rs[emp_tel]</td><td><button name=\"btn_disallow\" value=\"$rs[auth_id]\">ยกเลิกสิทธิการใช้งาน</button></td></tr>";
									elseif($_GET['perm_id'] == '3')
										echo "<tr><td>$start</td><td>$rs[auth_user]</td><td>$rs[auth_pass]</td><td>$rs[emp_fname] $rs[emp_lname]</td><td>$rs[emp_tel]</td><td><button name=\"btn_allow\" value=\"$rs[auth_id]\">ให้สิทธิการใช้งาน</button></td></tr>";
								}
							?>
                        </tbody>
                    </table>
                    <?php
                    	$exec->link();
					?><br />
                    <center>
                    <a href="index.php"><ย้อนกลับ></a>
                    </center>
                </form>
            </fieldset>
		<!-- InstanceEndEditable --></div>
    </div>
    <div id="footer"></div>
</body>
<!-- InstanceEnd --></html>
<?php
	if(!empty($_POST['btn_allow'])){
		$exec->execute("UPDATE auth SET perm_id='2' WHERE auth_id='$_POST[btn_allow]'");
		header("refresh:0;emp_disp.php");
		#echo "UPDATE auth SET perm_id='2' WHERE auth_id='$_POST[btn_allow]'";
	}elseif(!empty($_POST['btn_disallow'])){
		$exec->execute("UPDATE auth SET perm_id='3' WHERE auth_id='$_POST[btn_disallow]'");
		header("refresh:0;emp_disp.php");
	}
	
?>
