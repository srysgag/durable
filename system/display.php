<?php
	require_once 'database.php';
	require_once 'depreciation.php';
    session_start();
	ob_start();
    $exec = database::getInstance()
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/prototype.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ระบบจัดการครุภัณฑ์ คณะบริหารธุรกิจ มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.min.css" />
    <script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <!-- InstanceBeginEditable name="head" -->
    
     
    <style type="text/css">
		.ui-menu .ui-menu-item a{ height:14px; font-family:tahoma; font-size:12px; }
		p.search {text-align: right;}
	</style>
    <script type="text/javascript">
		$(document).ready(function(){
            /*$('#btn_search').click(function(){
				if($('#txt_search').val().length != 0){
					$.ajax({
						type: 'POST',
						url: 'load_contr',
						data: { 'method': 'search', 'type': $('#opt_search :selected').val(), 'term': $('#txt_search').val() },
						success: function(data){
							var count = 1;
							var obj = $.parseJSON(data);
							if(obj.length != 0){
								$('#tbldrb>thead').empty();
								$('#tbldrb>thead').append('<th>ลำดับ</th><th>ที่เอกสาร</th><th>หมายเลขครุภัณฑ์</th><th>ชื่อครุภัณฑ์</th><th>สุถานที่จัดเก็บ</th><th>ผู้รับผิดชอบ</th><th>สถานะครุภัณฑ์</th><th>ค่าเสื่อมราคาสะสม</th>');
								$('#tbldrb>tbody').empty();
								$.each(obj, function(i, v){
									if(v['drbno_status'] == '1')
										$('#tbldrb>tbody').append('<tr><td>'+count+'</td><td>'+v['drb_no']+'</td><td>'+v['drbno_number']+'</td><td>'+v['drb_name']+'</td><td>ห้อง '+v['room_name']+' ชั้น '+v['floor_name']+' อาคาร '+v['building_name']+' พื้นที่ '+v['area_name']+'</td><td>'+v['resp_name']+'</td><td>ปกติ</td><td>'+v['drb_depreciation']+'</td></tr>');
									else if(v['drbno_status'] == '2')
										$('#tbldrb>tbody').append('<tr><td>'+count+'</td><td>'+v['drb_no']+'</td><td>'+v['drbno_number']+'</td><td>'+v['drb_name']+'</td><td>'+v['room_name']+' '+v['floor_name']+' '+v['building_name']+' '+v['area_name']+'</td><td>'+v['resp_name']+'</td><td>ส่งซ่อม</td><td>'+v['drb_depreciation']+'</td></tr>');
									else if(v['drbno_status'] == '3')
										$('#tbldrb>tbody').append('<tr><td>'+count+'</td><td>'+v['drb_no']+'</td><td>'+v['drbno_number']+'</td><td>'+v['drb_name']+'</td><td>'+v['room_name']+' '+v['floor_name']+' '+v['building_name']+' '+v['area_name']+'</td><td>'+v['resp_name']+'</td><td>แทงจำหน่าย</td><td>'+v['drb_depreciation']+'</td></tr>');
								});
							}
						}
					});
				}
			});*/
			
			$(function(){				
				$('.autocomplete').autocomplete({
					source: function(request, response){
							$.ajax({
								url: 'autocomplete.php',
								dataType: "json",
								data: { method: 'search', term: request.term, type: $('#opt_search :selected').val() },
								success: function(data){
									console.log(data);
									response(data);
								}
							});
						},
					minLength: 1,
					focus: function(event, ui){
						$('.autocomplete').val(ui.item.name);
					},
					select: function(event, ui){
						$('#txt_search').val(ui.item.name);
						return false;
					}
				})
				.data('ui-autocomplete')._renderItem = function(ul,item){
					return $('<li>')
						.append('<a>'+item.name+'</a>')
						.appendTo(ul);
				};
			});
        });
	</script>
    
    
    
	<!-- InstanceEndEditable -->
</head>
<body>
	<div id="header"></div>
    <div id="menu">
    	<?php
			include 'login.php';
		?>
    </div>
    <div id="middle">
    	<div id="info"><!-- InstanceBeginEditable name="content" -->
        	
            <fieldset>
    	<legend>ค้นหาครุภัณฑ์</legend>
        	<p class="search">
            	<form action="" method="get">
                <label>เลือกประเภทการค้นหา</label>
                <select id="opt_search" name="opt_search">
                    <optgroup label="ข้อมูลครุภัณฑ์">
                        <option value="number">เลขครุภัณฑ์</option>
                        <option value="document">ที่เอกสาร</option>
                    </optgroup>
                    <optgroup label="สถานที่จัดเก็บ">
                        <option value="room">ห้อง</option>
                        <option value="building">ตึก</option>
                        <option value="area">พื้นที่</option>
                    </optgroup>
                    <optgroup label="ผู้รับผิดชอบ">
                        <option value="responsible">ชื่อผู้รับผิดชอบ</option>
                        <option value="branch">สาขา</option>
                    </optgroup>
                </select>
                <label>คำค้น</label><input id="txt_search" class="autocomplete" name="search" type="text" />
                <button id="btn_search">ค้นหา</button>
                </form>
            </p>
    </fieldset>
    <fieldset>
    	<legend>ผลการค้นหา</legend>
        <?php
			$term = trim(strip_tags($_REQUEST['search']));
			
			if(trim(strip_tags($_REQUEST['opt_search'])) == 'number'){
				$qry = $exec->genpage("SELECT * FROM drb, drbno, respons, room, floor, building, area WHERE drb.drb_id=drbno.drb_id AND respons.resp_id=drbno.resp_id AND room.room_id=drbno.room_id AND room.floor_id=floor.floor_id AND floor.building_id=building.building_id AND building.area_id=area.area_id AND drbno_number LIKE '%$term%'");
			}elseif(trim(strip_tags($_REQUEST['opt_search'])) == 'document'){
				$qry = $exec->genpage("SELECT * FROM drb, drbno, respons, room, floor, building, area WHERE drb.drb_id=drbno.drb_id AND respons.resp_id=drbno.resp_id AND room.room_id=drbno.room_id AND room.floor_id=floor.floor_id AND floor.building_id=building.building_id AND building.area_id=area.area_id AND drb_no LIKE '%$term%'");
			}elseif(trim(strip_tags($_REQUEST['opt_search'])) == 'room'){
				$qry = $exec->genpage("SELECT * FROM drb, drbno, respons, room, floor, building, area WHERE drb.drb_id=drbno.drb_id AND respons.resp_id=drbno.resp_id AND room.room_id=drbno.room_id AND room.floor_id=floor.floor_id AND floor.building_id=building.building_id AND building.area_id=area.area_id AND room_name LIKE '%$term%'");
			}elseif(trim(strip_tags($_REQUEST['opt_search'])) == 'building'){
				$qry = $exec->genpage("SELECT * FROM drb, drbno, respons, room, floor, building, area WHERE drb.drb_id=drbno.drb_id AND respons.resp_id=drbno.resp_id AND room.room_id=drbno.room_id AND room.floor_id=floor.floor_id AND floor.building_id=building.building_id AND building.area_id=area.area_id AND building_name LIKE '%$term%'");
			}elseif(trim(strip_tags($_REQUEST['opt_search'])) == 'area'){
				$qry = $exec->genpage("SELECT * FROM drb, drbno, respons, room, floor, building, area WHERE drb.drb_id=drbno.drb_id AND respons.resp_id=drbno.resp_id AND room.room_id=drbno.room_id AND room.floor_id=floor.floor_id AND floor.building_id=building.building_id AND building.area_id=area.area_id AND area_name LIKE '%$term%'");
			}elseif(trim(strip_tags($_REQUEST['opt_search'])) == 'responsible'){
				$qry = $exec->genpage("SELECT * FROM drb, drbno, respons, room, floor, building, area WHERE drb.drb_id=drbno.drb_id AND respons.resp_id=drbno.resp_id AND room.room_id=drbno.room_id AND room.floor_id=floor.floor_id AND floor.building_id=building.building_id AND building.area_id=area.area_id AND resp_name LIKE '%$term%'");
			}elseif(trim(strip_tags($_REQUEST['opt_search'])) == 'branch'){
				$qry = $exec->genpage("SELECT * FROM drb, drbno, respons, room, floor, building, area WHERE drb.drb_id=drbno.drb_id AND respons.resp_id=drbno.resp_id AND room.room_id=drbno.room_id AND room.floor_id=floor.floor_id AND floor.building_id=building.building_id AND building.area_id=area.area_id AND resp_branch LIKE '%$term%'");
			}
			$start = $exec->getstart();
		?>
      <center>  <table id="tbldrb">
        	<thead>
           		<th>ลำดับที่</th><th>หมายเลขครุภัณฑ์</th><th>ชื่อครุภัณฑ์</th><th>ยี่ห้อ</th><th>ห้อง</th><th>ชั้น</th><th>อาคาร</th><th>พื้นที่</th><th>สาขาที่รับผิดชอบ</th>
            </thead>
            <tbody>
                        	<?php
								while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
									$start++;
									echo "<tr><td>$start</td><td style=text-align:left >$rs[drbno_number]</td><td style=text-align:left >$rs[drb_name]</td><td style=text-align:left >$rs[drb_band]</td><td>$rs[room_name]</td><td>$rs[floor_name]</td><td>$rs[building_name]</td><td style=text-align:left >$rs[area_name]</td><td>$rs[resp_branch]</td></tr>";
								}
							?>
                        </tbody>
                    </table></center>
                 <center>   <?php
                    	$exec->link();
					?><br /> </center>
    </fieldset>
            
		<!-- InstanceEndEditable --></div>
    </div>
    <div id="footer"></div>
</body>
<!-- InstanceEnd --></html>
