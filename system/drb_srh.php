<?php
	require_once 'database.php';
    session_start();
	ob_start();
    $exec = database::getInstance()
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/prototype.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ระบบจัดการครุภัณฑ์ คณะบริหารธุรกิจ มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.min.css" />
    <script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <!-- InstanceBeginEditable name="head" -->
    <style type="text/css">
		.ui-menu .ui-menu-item a{ height:14px; font-family:tahoma; font-size:12px; }
	</style>
    <script>
		$(document).ready(function(){
            /*$('#btn_search').click(function(){
				if($('#drb_no').val().length == 0){
				}else{
					$.ajax({
						type: 'POST',
						url: 'load_contr',
						data: { method: 'drb', drb_no: $('#drb_no').val() },
						success: function(data){
							var obj = $.parseJSON(data);
							$.each(obj, function(i, v){
								var count = $('#tbldoc>tbody tr').length;
								$('#tbldoc>tbody').append('<tr><td>'+(count+1)+'</td><td>'+v['drb_no']+'</td><td>'+v['drb_detail']+'</td><td>'+v['drbtype_name']+'</td><td>'+v['drb_name']+'</td><td>'+v['drb_import']+'</td><td>'+v['drb_receivedate']+'</td><td><button name="btn_edit" value="'+v['drb_id']+'">แก้ไข</button></td></tr>');
							});
							$('#drb_no').val('');
						}
					});
				}
			});*/
        });
		
		$(function(){
			$('.autocomplete').autocomplete({
				source: 'autocomplete.php?method=drb',
				minLength: 1,
				focus: function(event, ui){
					$('.autocomplete').val(ui.item.drb_no);
				},
				select: function(event, ui){
					$('#drb_no').val(ui.item.drb_no);
					return false;
				}
			})
			.data('ui-autocomplete')._renderItem = function(ul,item){
				return $('<li>')
					.append('<a>'+item.drb_no+'</a>')
					.appendTo(ul);
			};
		});
		
		function doRemoveItem(obj) {
			$(obj).parent().parent().remove();
		}
	</script>
	<!-- InstanceEndEditable -->
</head>
<body>
	<div id="header"></div>
    <div id="menu">
    	<?php
			include 'login.php';
		?>
    </div>
    <div id="middle">
    	<div id="info"><!-- InstanceBeginEditable name="content" -->
        	<fieldset>
            	<legend>ค้นหาคุรภัณฑ์</legend>
                <form action="" method="get">
                	<label class="lbl">เลขที่เอกสาร</label>
                
                    <input id="drb_no" class="autocomplete" name="search" type="text" />
                    <button id="btn_search">ค้นหา</button>
                </form>
            </fieldset>
        	<fieldset>
                <legend>รายการครุภัณฑ์</legend>
                <form action="drb_edit.php" method="post">
                	<?php
						$qry = $exec->genpage("SELECT * FROM drb, drbno, drbtype WHERE drb.drb_id=drbno.drb_id AND drb.drbtype_id=drbtype.drbtype_id AND drb.drb_no LIKE '%$_REQUEST[search]%' GROUP BY drb.drb_no");
                    	$start = $exec->getstart();
					?>
                    <center>
                    <table id="tbldoc">
                    	<thead>
                            <th>ลำดับที่</th>
                            <th>เลขที่เอกสาร</th>
                            <th>รายละเอียด</th>
                            <th>ประเภทครุภัณฑ์</th>
                            <th>ชื่อครุภัณฑ์</th>
                            <th>ยี่ห้อ</th>
                            <th>จำนวน</th>
                            <th>วันที่รับ</th>
                            <th>แก้ไข</th>
                           
                        </thead>
                        <tbody>
                    </center>
                        	<?php
								while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
									$start++;
									echo "<tr><td>$start</td><td style=text-align:left >$rs[drb_no]</td><td style=text-align:left >$rs[drb_detail]</td><td style=text-align:left >$rs[drbtype_name]</td><td style=text-align:left >$rs[drb_name]</td><td style=text-align:left >$rs[drb_band]</td><td>$rs[drb_import]</td><td>$rs[drb_receivedate]</td><td><button name=\"btn_edit\" value=\"$rs[drb_id]\">แก้ไข</button></td></tr>";
								}
							?>
                        </tbody>
                    </table>
                    <?php
                    	$exec->link();
					?><br />
                    <center>
                    <a href="drb_ins.php" style="background-color:#E0FFFF">ย้อนกลับ</a>
                    </center>
                </form>
            </fieldset>
		<!-- InstanceEndEditable --></div>
    </div>
    <div id="footer"></div>
</body>
<!-- InstanceEnd --></html>
