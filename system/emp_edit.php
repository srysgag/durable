<?php
	require_once 'database.php';
    session_start();
	ob_start();
    $exec = database::getInstance()
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/prototype.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ระบบจัดการครุภัณฑ์ คณะบริหารธุรกิจ มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.min.css" />
    <script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <!-- InstanceBeginEditable name="head" -->
    <script type="text/javascript">
		function readFile(input){
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#myimg').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
	</script>
	<!-- InstanceEndEditable -->
</head>
<body>
	<div id="header"></div>
    <div id="menu">
    	<?php
			include 'login.php';
		?>
    </div>
    <div id="middle">
    	<div id="info"><!-- InstanceBeginEditable name="content" -->
        	<?php
            	if(!empty($_POST['btn_edit'])){
                    $qry = $exec->execute("SELECT * FROM perm, auth, employee WHERE perm.perm_id=auth.perm_id AND auth.auth_id=employee.auth_id AND employee.emp_id='$_POST[btn_edit]'");
                    $rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
                }
            ?>
            <form action="" method="post" enctype="multipart/form-data">
        		<fieldset>
                    <legend>ข้อมูลผู้ใช้ระบบ</legend>
                    <div class="box2">
                        <label class="lbl">ชื่อ</label ><input name="emp_fname" value="<?=$rs['emp_fname'];?>" /><br />
                        <label class="lbl">นามสกุล</label><input name="emp_lname" value="<?=$rs['emp_lname'];?>" /><br />
                        <label class="lbl">เบอร์โทรศัพท์</label><input name="emp_tel" value="<?=$rs['emp_tel'];?>" /><br />
                    	<input name="emp_id" type="hidden" value="<?=$rs['emp_id'];?>" />
                        <input name="auth_id" type="hidden" value="<?=$rs['auth_id'];?>" />
                    </div>
                    <div class="box3">
                        <img id="myimg" src="<?=$rs['emp_img'];?>" />
                        <input name="emp_img" type="file" onchange="readFile(this);" />
                    </div>
                     <button name="btn" value="save">บันทึกข้อมูล</button>
                </fieldset>
            </form>
		<!-- InstanceEndEditable --></div>
    </div>
    <div id="footer"></div>
</body>
<!-- InstanceEnd --></html>

<?php
	if(!empty($_POST['btn']) == 'save'){
    	if(empty($_POST['emp_fname'])){
			
		}elseif(empty($_POST['emp_lname'])){
			
		}elseif(empty($_POST['emp_tel'])){
			
		}else{
			if($_FILES['emp_img']['error'] == 0){
				if($_FILES['emp_img']['type'] == 'image/jpeg'){
					$tragetfolder = 'emp_img/'.$auth_id.'.jpg';
					move_uploaded_file($_FILES['emp_img']['tmp_name'], $tragetfolder);
					$exec->execute("UPDATE employee SET emp_img='$tragetfolder' WHERE emp_id='$_POST[emp_id]");
				}
			}
			
			$exec->execute("UPDATE employee SET emp_fname='$_POST[emp_fname]', emp_lname='$_POST[emp_lname]', emp_tel='$_POST[emp_tel]' WHERE emp_id='$_POST[emp_id]'");
			echo 'บันทึกข้อมูลพนักงานเรียบร้อยแล้ว';
			
		}
    }
?>
