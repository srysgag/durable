<?php
	require_once('database.php');
    session_start();
	ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/prototype.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ระบบจัดการครุภัณฑ์ คณะบริหารธุรกิจ มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.min.css" />
    <script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <!-- InstanceBeginEditable name="head" -->
   	<script>
		$(document).ready(function(){
            $('#btn_search').click(function(){
				if($('#drbno_number').val().length == 0){
				}else{
					$.ajax({
						type: 'POST',
						url: 'load_contr',
						data: { method: 'drbno', drbno_number: $('#drbno_number').val() },
						success: function(data){
							var obj = $.parseJSON(data);
							$.each(obj, function(i, v){
								var count = $('#tbldrbno>tbody tr').length;
								$('#tbldrbno>tbody').append('<tr><td>'+(count+1)+'</td><td>'+v['drbno_number']+'</td><td>'+v['drb_name']+'<input name="drbno_id[]" type="hidden" value="'+v['drb_id']+'" readonly /></td><td>'+v['room_name']+'</td><td>'+v['floor_name']+'</td><td>'+v['building_name']+'</td><td>'+v['area_name']+'</td><td>'+v['resp_name']+'</td><td><button onclick="javascript:doRemoveItem(this);">ลบ</button></td></tr>');
							});
							$('#drbno_number').val('');
						}
					});
				}
			});
        });
		
		function doRemoveItem(obj) {
			$(obj).parent().parent().remove();
		}
	</script>
	<!-- InstanceEndEditable -->
</head>
<body>
	<div id="header"></div>
    <div id="menu">
    	<?php
			include 'login.php';
		?>
    </div>
    <div id="middle">
    	<div id="info"><!-- InstanceBeginEditable name="content" -->
        	<fieldset>
            	<legend>ค้นหาคุรภัณฑ์</legend>
                <label class="lbl">หมายเลขครุภัณฑ์</label>
                <input id="drbno_number" type="text" />
                <button id="btn_search">ค้นหา</button>
            </fieldset>
            <fieldset>
            	<legend>ผลการค้นหา</legend>
                <form action="" method="post">
                	<table id="tbldrbno" cellpadding="7">
                    	<thead>
                        	<th>ลำดับ</th>
                            <th>หมายเลขครุภัณฑ์</th>
                            <th>ชื่อครุภัณฑ์</th>
                            <th>ห้อง</th>
                            <th>ชั้น</th>
                            <th>อาคาร</th>
                            <th>พื้นที่</th>
                            <th>ผู้รับผิดชอบ</th>
                            <th>ยกเลิก</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <button>แก้ไขข้อมูล</button>
               <button type="submit"><img src="C:\AppServ\www\pj\image\header.jpg" alt="Submit"></button>
                </form>
            </fieldset>
            
            <?php
				for($i=0; $i<10; $i++){
					for($n=0; $n<$i; $n++){
						echo '*';
					}
					echo '<br />';
				}
				
				echo time();
			?>
		<!-- InstanceEndEditable --></div>
    </div>
    <div id="footer"></div>
</body>
<!-- InstanceEnd --></html>
