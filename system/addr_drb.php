<?php
	require_once 'database.php';
	require_once 'log.php';
    session_start();
	ob_start();
    $exec = database::getInstance();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/prototype.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ระบบจัดการครุภัณฑ์ คณะบริหารธุรกิจ มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.min.css" />
    <script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <!-- InstanceBeginEditable name="head" -->
    <style type="text/css">
		.ui-tabs{ font-family:tahoma; font-size:11px; }
		.ui-datepicker{ width:220px; font-family:tahoma; font-size:11px; text-align:center; }
		.ui-menu .ui-menu-item a{ height:14px; font-family:tahoma; font-size:12px; }
	</style>
    <script>
		$(document).ready(function(){
			/*$('#building_id').hide();
			$('#floor_id').hide();
			$('#room_id').hide();*/
			/*
			$('#check_all').click(function () {    
				 $('input:checkbox').prop('checked', this.checked);    
			});*/
			
			$('#txtbuilding').hide();
			$('#txtfloor').hide();
			$('#txtroom').hide();
			$('#building_id').hide();
			$('#floor_id').hide();
			
			$('#choose').change(function(){
				if($('#choose option:selected').val() == 'building'){
					$('#txtbuilding').show();
					$('#txtfloor').show();
					$('#txtroom').show();
					$('#building_id').hide();
					$('#floor_id').hide();
				}else if($('#choose option:selected').val() == 'floor'){
					$('#txtbuilding').hide();
					$('#txtfloor').show();
					$('#txtroom').show();
					$('#building_id').show();
					$('#floor_id').hide();
				}else if($('#choose option:selected').val() == 'room'){
					$('#txtbuilding').hide();
					$('#txtfloor').hide();
					$('#txtroom').show();
					$('#building_id').show();
					$('#floor_id').show();
				}
				/*alert($('#choose option:selected').val());*/
			})
			
            loadarea();
			//loadroom();
			$('#area_id').change(function(){
				loadbuilding();
			});
			
			$('#building_id').change(function(){
				loadfloor();
			});
			
			$('#floor_id').change(function(){
				loadroom();
			});
        });
		
		/*$(function(){
			$('.autocomplete').autocomplete({
				source: 'autocomplete.php?method=resp',
				minLength: 1,
				focus: function(event, ui){
					$('.autocomplete').val(ui.item.resp_name);
				},
				select: function(event, ui){
					$('#resp_branch').val(ui.item.resp_branch);
					$('#resp_tel').val(ui.item.resp_tel);
					$('#resp_room').val(ui.item.resp_room);
					$('#resp_id').val(ui.item.resp_id);
					return false;
				}
			})
			.data('ui-autocomplete')._renderItem = function(ul,item){
				return $('<li>')
					.append('<a>'+item.resp_name+'</a>')
					.appendTo(ul);
			};
		});*/
		
		function loadarea(){
			$('#area_id').empty();
			$.ajax({
				type: 'POST',
				url: 'load_contr.php',
				data: { method: 'area' },
				success: function(data){
					var obj = $.parseJSON(data);
					$('#area_id').append('<option value="null">-- กรุณาเลือก --</option>');
					$.each(obj, function(i,v){
						$('#area_id').append('<option value="'+v['area_id']+'">'+v['area_name']+'</option>');
					});
				}
			});
		}
		
		function loadbuilding(){
			if($('#area_id').val() != 'null'){
				$('#building_id').empty();
				$.ajax({
					type: 'POST',
					url: 'load_contr.php',
					data: { method: 'building', area_id: $('#area_id').val() },
					success: function(data){
						$('#building_id').append('<option value="null">-- กรุณาเลือก --</option>');
						if(data != 'null'){
							var obj = $.parseJSON(data);
							$.each(obj, function(i,v){
								$('#building_id').append('<option value="'+v['building_id']+'">'+v['building_name']+'</option>');
							});
						}
					}
				});
			}else{
				$('#building_id').append('<option value="null">-- กรุณาเลือก --</option>');
			}
		}
		
		function loadfloor(){
			if($('#building_id').val() != 'null'){
				$('#floor_id').empty();
				$.ajax({
					type: 'POST',
					url: 'load_contr.php',
					data: { method: 'floor', building_id: $('#building_id').val() },
					success: function(data){
						$('#floor_id').append('<option value="null">-- กรุณาเลือก --</option>');
						if(data != 'null'){
							var obj = $.parseJSON(data);
							$.each(obj, function(i,v){
								$('#floor_id').append('<option value="'+v['floor_id']+'">'+v['floor_name']+'</option>');
							});
						}
					}
				});
			}else{
				$('#floor_id').append('<option value="null">-- กรุณาเลือก --</option>');
			}
		}
		
		function loadroom(){
			if($('#floor_id').val() != 'null'){
				$('#room_id').empty();
				console.log($('#floor_id :selected').val());
				$.ajax({
					type: 'POST',
					url: 'load_contr.php',
					data: { method: 'room', floor_id: $('#floor_id').val() },
					success: function(data){
						$('#room_id').append('<option value="null">-- กรุณาเลือก --</option>');
						if(data != 'null'){
							var obj = $.parseJSON(data);
							$.each(obj, function(i,v){
								$('#room_id').append('<option value="'+v['room_id']+'">'+v['room_name']+'</option>');
							});
						}
					}
				});
			}else{
				$('#room_id').append('<option value="null">-- กรุณาเลือก --</option>');
			}
		}
	</script>
<body id="popup">
	<fieldset>
    	<legend>เพิ่มข้อมูลสถานที่</legend>
        <form action="#" method="POST">
            <fieldset>
            	<select id="choose" name="choose">
                	<option value="null">-- กรุณาเลือก --</option>
                	<option value="building">อาคาร</option>
                    <option value="floor">ชั้น</option>
                    <option value="room">ห้อง</option>
                </select>
            	<!--<input id="choose" name="choose" type="radio" value="build" /><label>อาคาร</label><input id="choose" name="choose" type="radio" value="floor" /><label>ชั้น</label><input id="choose" name="choose" type="radio" value="room" /><label>ห้อง</label><br />-->
            </fieldset>
            <label class="lbl">พื่นที่</label><select id="area_id" name="area_id"></select><br />
            <label class="lbl">อาคาร</label><select id="building_id" name="building_id"></select><input id="txtbuilding" name="txtbuilding" /><br />
            <label class="lbl">ชั้น</label><select id="floor_id" name="floor_id"></select><input id="txtfloor" name="txtfloor" /><br />
            <label class="lbl">ห้อง</label><input id="txtroom" name="txtroom" /><br />
            <input type="submit" value="เพิ่มข้อมูล" />
        </form>
    </fieldset>
</body>
<?php
	if(!empty($_POST)){
		if($_POST['choose'] == "null"){
			echo "<script>alert('กรุณาเลือกประเภทที่ต้องการเพิ่มข้อมูล');</script>";
		}else{
			if($_POST['choose'] == "building"){
				/*echo "SELECT MAX(building_id) AS last_id FROM building";
				echo "SELECT MAX(floor_id) AS last_id FROM floor";
				echo "SELECT MAX(room_id) AS last_id FROM room";*/
				$qry = $exec->execute("SELECT MAX(building_id) AS last_id FROM building");
				$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
				if(!empty($rs['last_id'])){
					$gen = substr($rs['last_id'],1)+1;
					$building_id = sprintf('B%003.0f',$gen);
				}else{
					$building_id = 'B001';
				}
				#echo "INSERT building VALUES('$building_id','$_POST[txtbuilding]','$_POST[area_id]')";
				$exec->execute("INSERT building VALUES('$building_id','$_POST[txtbuilding]','$_POST[area_id]')");
				
				$qry = $exec->execute("SELECT MAX(floor_id) AS last_id FROM floor");
				$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
				if(!empty($rs['last_id'])){
					$gen = substr($rs['last_id'],1)+1;
					$floor_id = sprintf('F%003.0f',$gen);
				}else{
					$floor_id = 'F001';
				}
				//insert floor.
				$exec->execute("INSERT floor VALUES('$floor_id','$_POST[txtfloor]','$building_id')");
				
				$qry = $exec->execute("SELECT MAX(room_id) AS last_id FROM room");
				$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
				if(!empty($rs['last_id'])){
					$gen = substr($rs['last_id'],1)+1;
					$room_id = sprintf('R%003.0f',$gen);
				}else{
					$room_id = 'R001';
				}
				$exec->execute("INSERT room VALUES('$room_id','$_POST[txtroom]','$floor_id')");
				
				
			}elseif($_POST['choose'] == "floor"){
				$qry = $exec->execute("SELECT MAX(floor_id) AS last_id FROM floor");
				$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
				if(!empty($rs['last_id'])){
					$gen = substr($rs['last_id'],1)+1;
					$floor_id = sprintf('F%003.0f',$gen);
				}else{
					$floor_id = 'F001';
				}
				//insert floor.
				$exec->execute("INSERT floor VALUES('$floor_id','$_POST[txtfloor]','$building_id')");
				
				$qry = $exec->execute("SELECT MAX(room_id) AS last_id FROM room");
				$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
				if(!empty($rs['last_id'])){
					$gen = substr($rs['last_id'],1)+1;
					$room_id = sprintf('R%003.0f',$gen);
				}else{
					$room_id = 'R001';
				}
				$exec->execute("INSERT room VALUES('$room_id','$_POST[txtroom]','$_POST[building_id]')");
			}elseif($_POST['choose'] == "room"){
				$qry = $exec->execute("SELECT MAX(room_id) AS last_id FROM room");
				$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
				if(!empty($rs['last_id'])){
					$gen = substr($rs['last_id'],1)+1;
					$room_id = sprintf('R%003.0f',$gen);
				}else{
					$room_id = 'R001';
				}
				$exec->execute("INSERT room VALUES('$room_id','$_POST[txtroom]','$_POST[floor_id]')");
			}
		}
		
	}
?>
</html>
