<?php
	require_once('database.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="css/style.css" />
	<script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
</head>
<body id="popup">
	<fieldset>
    	<legend>เพิ่มหน่วยนับครุภัณฑ์</legend>
        <form action="#" method="POST">
            <label class="lbl">หน่วยนับ</label><input name="unit_name" type="text" value="<?php if(!empty($_POST['unit_name'])) echo $_POST['unit_name']; ?>" /><br />
            <p class="description"><?php if(empty($_POST['unit_name'])) echo 'กรุณากรอกหน่วยนับครุภัณฑ์'; ?></p><br />
            <input type="submit" value="เพิ่มข้อมูล" />
        </form>
    </fieldset>
</body>
<?php
	if(!empty($_POST)){
		if(empty($_POST['unit_name'])){
		}else{
			$exec = database::getInstance();
			$qry = $exec->execute('SELECT MAX(unit_id) AS max_id FROM unit');
			$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
			if(!empty($rs['max_id'])){
				$gen = substr($rs['max_id'],1)+1;
				$unit_id = sprintf('U%003.0f',$gen);
			}else{
				$unit_id = 'U001';
			}
			$exec->execute("INSERT INTO unit VALUES('$unit_id','$_POST[unit_name]')");
			echo 'เพิ่มข้อมูลประเภทครุภัณฑ์เรียบร้อยแล้ว';
			echo '<script>window.opener.loadunit();window.close();</script>';
		}
	}else{
		echo 'กรุณากรอกข้อมูลให้ครบถ้วน';
	}
?>
</html>
