<?php
	require_once 'database.php';
	require_once 'log.php';
    session_start();
	ob_start();
    $exec = database::getInstance();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/prototype.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ระบบจัดการครุภัณฑ์ คณะบริหารธุรกิจ มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.min.css" />
    <script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <!-- InstanceBeginEditable name="head" -->
    <style type="text/css">
		.ui-tabs{ font-family:tahoma; font-size:11px; }
		.ui-datepicker{ width:220px; font-family:tahoma; font-size:11px; text-align:center; }
		.ui-menu .ui-menu-item a{ height:14px; font-family:tahoma; font-size:12px; }
	</style>
    <script>
		$(document).ready(function(){
            loadarea();
			//loadroom();
			$('#area_id').change(function(){
				loadbuilding();
			});
			
			$('#building_id').change(function(){
				loadfloor();
			});
			
			$('#floor_id').change(function(){
				loadroom();
			});
        });
		
		$(function(){
			$('.autocomplete').autocomplete({
				source: 'autocomplete.php?method=resp',
				minLength: 1,
				focus: function(event, ui){
					$('.autocomplete').val(ui.item.resp_name);
				},
				select: function(event, ui){
					$('#resp_branch').val(ui.item.resp_branch);
					$('#resp_tel').val(ui.item.resp_tel);
					$('#resp_room').val(ui.item.resp_room);
					$('#resp_id').val(ui.item.resp_id);
					return false;
				}
			})
			.data('ui-autocomplete')._renderItem = function(ul,item){
				return $('<li>')
					.append('<a>'+item.resp_name+'</a>')
					.appendTo(ul);
			};
		});
		
		function loadarea(){
			$('#area_id').empty();
			$.ajax({
				type: 'POST',
				url: 'load_contr.php',
				data: { method: 'area' },
				success: function(data){
					var obj = $.parseJSON(data);
					$('#area_id').append('<option value="null">-- กรุณาเลือก --</option>');
					$.each(obj, function(i,v){
						$('#area_id').append('<option value="'+v['area_id']+'">'+v['area_name']+'</option>');
					});
				}
			});
		}
		
		function loadbuilding(){
			if($('#area_id').val() != 'null'){
				$('#building_id').empty();
				$.ajax({
					type: 'POST',
					url: 'load_contr.php',
					data: { method: 'building', area_id: $('#area_id').val() },
					success: function(data){
						$('#building_id').append('<option value="null">-- กรุณาเลือก --</option>');
						if(data != 'null'){
							var obj = $.parseJSON(data);
							$.each(obj, function(i,v){
								$('#building_id').append('<option value="'+v['building_id']+'">'+v['building_name']+'</option>');
							});
						}
					}
				});
			}else{
				$('#building_id').append('<option value="null">-- กรุณาเลือก --</option>');
			}
		}
		
		function loadfloor(){
			if($('#building_id').val() != 'null'){
				$('#floor_id').empty();
				$.ajax({
					type: 'POST',
					url: 'load_contr.php',
					data: { method: 'floor', building_id: $('#building_id').val() },
					success: function(data){
						$('#floor_id').append('<option value="null">-- กรุณาเลือก --</option>');
						if(data != 'null'){
							var obj = $.parseJSON(data);
							$.each(obj, function(i,v){
								$('#floor_id').append('<option value="'+v['floor_id']+'">'+v['floor_name']+'</option>');
							});
						}
					}
				});
			}else{
				$('#floor_id').append('<option value="null">-- กรุณาเลือก --</option>');
			}
		}
		
		function loadroom(){
			if($('#floor_id').val() != 'null'){
				$('#room_id').empty();
				$.ajax({
					type: 'POST',
					url: 'load_contr.php',
					data: { method: 'room', floor_id: $('#floor_id').val() },
					success: function(data){
						$('#room_id').append('<option value="null">-- กรุณาเลือก --</option>');
						if(data != 'null'){
							var obj = $.parseJSON(data);
							$.each(obj, function(i,v){
								$('#room_id').append('<option value="'+v['room_id']+'">'+v['room_name']+'</option>');
							});
						}
					}
				});
			}else{
				$('#room_id').append('<option value="null">-- กรุณาเลือก --</option>');
			}
		}
	</script>
	<!-- InstanceEndEditable -->
</head>
<body>
	<div id="header"></div>
    <div id="menu">
    	<?php
			include 'login.php';
		?>
    </div>
    <div id="middle">
    	<div id="info"><!-- InstanceBeginEditable name="content" -->
        	<form action="" method="post">
                <fieldset class="box1">
                    <legend>ข้อมูลสถานที่จัดเก็บ</legend>
                    <label class="lbl">พื้นที่</label><select id="area_id"></select><br />
                    <label class="lbl">อาคาร</label><select id="building_id"></select><br />
                    <label class="lbl">ชั้น</label><select id="floor_id"></select><br />
                    <label class="lbl">ห้อง</label><select id="room_id" name="room_id"></select><br />
                    <label class="lbl">ชื่อห้อง</label><input name="room_note" />
                </fieldset>
                <fieldset class="box1">
                    <legend>ข้อมูลผู้รับผิดชอบ</legend>
                    <label class="lbl">ชื่อ</label><input id="resp_name" class="autocomplete" name="resp_name" /><input type="submit" value="เพิ่ม" onclick="window.open('resp_ins.php','เพิ่มข้อมูลผู้รับผิดชอบ','width=512,height=160,toolbar=1,resizable=0');" /><br />
                    <label class="lbl">สาขา</label><input id="resp_branch" name="resp_branch" /><br />
                    <label class="lbl">เบอร์โทร</label><input id="resp_tel" name="resp_tel" /><br />
                    <label class="lbl">ห้องพัก</label><input id="resp_room" name="resp_room" /><br />
                    <input id="resp_id" name="resp_id" type="hidden" />
                </fieldset>
                <fieldset>
                    <legend>ข้อมูลครุภัณฑ์</legend>
                      <center> <table id="tbldrbno">
                            <thead>
                                <th>ลำดับที่</th>
                                <th>หมายเลขครุภัณฑ์</th>
                                <th>ชื่อครุภัณฑ์</th>
                                <th>สถานะปัจจุบัน</th>
                                <th>สภานที่เดิม</th>
                            </thead>
                       </center> <tbody>
                                <?php
                                    if(!empty($_POST)){
                                    	#print_r($_POST['drbno_id']);
                                        $i = 0;
                                        foreach($_POST['drbno_id'] as $v){
                                            #echo "SELECT * FROM drbno, drb, drbstatus WHERE drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND drbno.drb_id='$drb_id' ORDER BY drbno_id ASC";
                                            $qry = $exec->execute("SELECT * FROM drbno, drb, drbstatus, area,building,floor,room WHERE area.area_id=building.area_id AND building.building_id=floor.building_id AND floor.floor_id=room.floor_id AND room.room_id=drbno.room_id AND drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND drbno.drbno_id='$v'");
                                            if(mysqli_num_rows($qry) != 0){
                                                while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
                                                    $i++;
                                                    echo "<tr><td>$i</td><td>$rs[drbno_number]<input name=\"drbno_id[]\" type=\"hidden\" value=\"$rs[drbno_id]\" /></td><td>$rs[drb_name]</td><td>$rs[drbstatus_name]</td><td>$rs[room_name] $rs[floor_name] $rs[building_name] $rs[area_name]</td><tr>";
                                                }
                                            }
                                                #session_destroy();
                                            /*if($i == 0){
                                                session_destroy();
                                                header('location:drb_disp.php');
                                            }*/
                                                
                                            /*}else{
                                                session_destroy();
                                                header('location:drb_ins.php');
                                            }*/
                                        }
                                        echo $count;
                                    }else{
                                        session_destroy();
                                        header('location:drb_disp.php');
                                    }
                                    /*$i = 0;
                                    $exec = database::getInstance();
                                    $splitURL = explode('?', $_SERVER['REQUEST_URI']);
                                    #echo substr($splitURL[1], 7);
                                    $drb_id = substr($splitURL[1], 7);
                                    #echo "SELECT * FROM drbno, drb, drbstatus WHERE drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND drbno.drb_id='$drb_id' ORDER BY drbno_id ASC";
                                    $qry = $exec->execute("SELECT * FROM drbno, drb, drbstatus WHERE drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND drbno.drbno_status='0' AND drbno.drb_id='$drb_id' ORDER BY drbno_id ASC");
                                    if(mysqli_num_rows($qry) != 0){
                                        while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
                                            $i++;
                                            echo "<tr><td>$i</td><td>$rs[drbno_number]</td><td>$rs[drb_name]</td><td>$rs[drbstatus_name]</td><td><input name=\"drbno_id[]\" type=\"checkbox\" value=\"$rs[drbno_id]\" /></td><tr>";
                                        }
                                    }else{
                                        header('location:drb_ins.php');
                                    }*/
                                ?>
                            </tbody>
                        </table><br/>
                      <center>  <button name="btn" value="save">บันทึก</button><button name="btn" value="back">ย้อนกลับ</button></center>
                </fieldset>
                </form>
                
                <?php
					if($_POST['btn'] == 'save'){
						if(empty($_POST['resp_id'])){
							#echo 'empty control name resp_id.';
						}elseif($_POST['room_id'] == 'null'){
							#echo 'not choose room_id.';
						}/*elseif(count($_POST['drbno_id']) == 0){
							#echo 'not choose durable for storage.';
						}*/else{
							foreach($drbno_id as $v){
								#echo $v;
								echo "UPDATE drbno SET resp_id='$_POST[resp_id]', room_id='$_POST[room_id]', room_note='$_POST[room_note]' WHERE drbno_id='$v'<br />";
								$exec->execute("UPDATE drbno SET drbno_status='1', resp_id='$_POST[resp_id]', room_id='$_POST[room_id]', room_note='$_POST[room_note]' WHERE drbno_id='$v'");
								#$exec->execute("UPDATE drbno SET resp_id='$resp_id', room_id='$room_id' WHERE drbno_id='$v'");
							$log->write_log($_SESSION['auth']['name'].'->storage_edit:'.$v);
							}
							unset($_POST);
							echo "<script>alert('ย้ายครุภัณฑ์เรียบร้อยแล้ว');</script>";
                            header("refresh:0;drbstorage_disp.php");
						}
					}elseif($_POST['btn'] == 'back'){
						header('location:drbstorage_disp.php');
					}
				?>
		<!-- InstanceEndEditable --></div>
    </div>
    <div id="footer"></div>
</body>
<!-- InstanceEnd --></html>
