<?php
	require_once('database.php');
	
	if($_REQUEST['method'] == 'drbtype'){
		$exec = database::getInstance();
		$qry = $exec->execute('SELECT * FROM drbtype');
		while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
			$rs_set[] = $rs;
		}
		echo json_encode($rs_set);
	}elseif($_REQUEST['method'] == 'drbtype_deper'){
		#echo $_REQUEST['drbtype_id'];
		$exec = database::getInstance();
		$qry = $exec->execute("SELECT * FROM drbtype WHERE drbtype_id='$_REQUEST[drbtype_id]'");
		while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
			$rs_set[] = $rs;
		}
		echo json_encode($rs_set);
	}elseif($_REQUEST['method'] == 'unit'){
		$exec = database::getInstance();
		$qry = $exec->execute('SELECT * FROM unit');
		while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
			$rs_set[] = $rs;
		}
		echo json_encode($rs_set);
	}elseif($_REQUEST['method'] == 'drbstatus'){
		$exec = database::getInstance();
		$qry = $exec->execute('SELECT * FROM drbstatus');
		while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
			$rs_set[] = $rs;
		}
		echo json_encode($rs_set);
	}elseif($_REQUEST['method'] == 'area'){
		$exec = database::getInstance();
		$qry = $exec->execute('SELECT * FROM area');
		while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
			$rs_set[] = $rs;
		}
		echo json_encode($rs_set);
	}elseif($_REQUEST['method'] == 'building'){
		$exec = database::getInstance();
		$qry = $exec->execute("SELECT * FROM building WHERE area_id='$_REQUEST[area_id]'");
		while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
			$rs_set[] = $rs;
		}
		echo json_encode($rs_set);
	}elseif($_REQUEST['method'] == 'floor'){
		$exec = database::getInstance();
		$qry = $exec->execute("SELECT * FROM floor WHERE building_id='$_REQUEST[building_id]'");
		while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
			$rs_set[] = $rs;
		}
		echo json_encode($rs_set);
	}elseif($_REQUEST['method'] == 'room'){
		$exec = database::getInstance();
		$qry = $exec->execute("SELECT * FROM room WHERE floor_id='$_REQUEST[floor_id]'");
		while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
			$rs_set[] = $rs;
		}
		echo json_encode($rs_set);
	}elseif($_REQUEST['method'] == 'drbno'){
		$exec = database::getInstance();
		$qry = $exec->execute("SELECT * FROM drb, drbno, area, building, floor, room, respons WHERE drb.drb_id=drbno.drb_id AND building.area_id=area.area_id AND floor.building_id=building.building_id AND room.floor_id=floor.floor_id AND drbno.room_id=room.room_id AND drbno.resp_id=respons.resp_id AND drbno.drbno_number LIKE '%$_REQUEST[drbno_number]%'");
		while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
			$rs_set[] = $rs;
		}
		echo json_encode($rs_set);
	}elseif($_REQUEST['method'] == 'drb'){
		$exec = database::getInstance();
		$qry = $exec->execute("SELECT * FROM drb, drbno, drbtype WHERE drb.drb_id=drbno.drb_id AND drb.drbtype_id=drbtype.drbtype_id AND drb.drb_no LIKE '%$_REQUEST[drb_no]%' GROUP BY drb.drb_no");
		while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
			$rs_set[] = $rs;
		}
		echo json_encode($rs_set);
	}elseif($_REQUEST['method'] == 'drbdispose'){
		$exec = database::getInstance();
		$qry = $exec->execute("SELECT * FROM drb, drbno, drbtype, drbdispose WHERE drb.drb_id=drbno.drb_id AND drb.drbtype_id=drbtype.drbtype_id AND drbdispose.drbno_id=drbno.drbno_id AND drbno_status='3' AND drb.drb_no LIKE '%$_REQUEST[drb_no]%' GROUP BY drb.drb_no");
		while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
			$rs_set[] = $rs;
		}
		echo json_encode($rs_set);
	}elseif($_REQUEST['method'] == 'search'){
		$exec = database::getInstance();
		$data = array();
		$term = trim(strip_tags($_REQUEST['term']));
		$exec = database::getInstance();
		if(trim(strip_tags($_REQUEST['type'])) == 'number'){
			$qry = $exec->execute("SELECT * FROM drb, drbno, respons, room, floor, building, area WHERE drb.drb_id=drbno.drb_id AND respons.resp_id=drbno.resp_id AND room.room_id=drbno.room_id AND room.floor_id=floor.floor_id AND floor.building_id=building.building_id AND building.area_id=area.area_id AND drbno_number LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$rs_set[] = $rs;
			}
			/*$qry = $exec->execute("SELECT drbno_number FROM drbno WHERE drbno_number LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$temp = array('name'=>$rs['drbno_number']);
				array_push($data, $temp);
			}*/
		}elseif(trim(strip_tags($_REQUEST['type'])) == 'document'){
			$qry = $exec->execute("SELECT * FROM drb, drbno, respons, room, floor, building, area WHERE drb.drb_id=drbno.drb_id AND respons.resp_id=drbno.resp_id AND room.room_id=drbno.room_id AND room.floor_id=floor.floor_id AND floor.building_id=building.building_id AND building.area_id=area.area_id AND drb_no LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$rs_set[] = $rs;
			}
			/*$qry = $exec->execute("SELECT drb_no FROM drb WHERE drb_no LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$temp = array('name'=>$rs['drb_no']);
				array_push($data, $temp);
			}*/
		}elseif(trim(strip_tags($_REQUEST['type'])) == 'room'){
			$qry = $exec->execute("SELECT * FROM drb, drbno, respons, room, floor, building, area WHERE drb.drb_id=drbno.drb_id AND respons.resp_id=drbno.resp_id AND room.room_id=drbno.room_id AND room.floor_id=floor.floor_id AND floor.building_id=building.building_id AND building.area_id=area.area_id AND room_name LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$rs_set[] = $rs;
			}
			/*$qry = $exec->execute("SELECT room_name FROM room WHERE room_name LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$temp = array('name'=>$rs['room_name']);
				array_push($data, $temp);
			}*/
		}elseif(trim(strip_tags($_REQUEST['type'])) == 'building'){
			$qry = $exec->execute("SELECT * FROM drb, drbno, respons, room, floor, building, area WHERE drb.drb_id=drbno.drb_id AND respons.resp_id=drbno.resp_id AND room.room_id=drbno.room_id AND room.floor_id=floor.floor_id AND floor.building_id=building.building_id AND building.area_id=area.area_id AND building_name LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$rs_set[] = $rs;
			}
			/*$qry = $exec->execute("SELECT building_name FROM building WHERE building_name LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$temp = array('name'=>$rs['building_name']);
				array_push($data, $temp);
			}*/
		}elseif(trim(strip_tags($_REQUEST['type'])) == 'area'){
			$qry = $exec->execute("SELECT * FROM drb, drbno, respons, room, floor, building, area WHERE drb.drb_id=drbno.drb_id AND respons.resp_id=drbno.resp_id AND room.room_id=drbno.room_id AND room.floor_id=floor.floor_id AND floor.building_id=building.building_id AND building.area_id=area.area_id AND area_name LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$rs_set[] = $rs;
			}
			/*$qry = $exec->execute("SELECT area_name FROM area WHERE area_name LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$temp = array('name'=>$rs['area_name']);
				array_push($data, $temp);
			}*/
		}elseif(trim(strip_tags($_REQUEST['type'])) == 'responsible'){
			$qry = $exec->execute("SELECT * FROM drb, drbno, respons, room, floor, building, area WHERE drb.drb_id=drbno.drb_id AND respons.resp_id=drbno.resp_id AND room.room_id=drbno.room_id AND room.floor_id=floor.floor_id AND floor.building_id=building.building_id AND building.area_id=area.area_id AND resp_name LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$rs_set[] = $rs;
			}
			/*$qry = $exec->execute("SELECT resp_name FROM respons WHERE resp_name LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$temp = array('name'=>$rs['resp_name']);
				array_push($data, $temp);
			}*/
		}elseif(trim(strip_tags($_REQUEST['type'])) == 'branch'){
			$qry = $exec->execute("SELECT * FROM drb, drbno, respons, room, floor, building, area WHERE drb.drb_id=drbno.drb_id AND respons.resp_id=drbno.resp_id AND room.room_id=drbno.room_id AND room.floor_id=floor.floor_id AND floor.building_id=building.building_id AND building.area_id=area.area_id AND resp_branch LIKE '%$term%'");
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$rs_set[] = $rs;
			}
		}
		
		echo json_encode($rs_set);
	}
?>