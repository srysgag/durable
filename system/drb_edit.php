<?php
	require_once 'database.php';
	require_once 'log.php';
    session_start();
	ob_start();
    $exec = database::getInstance();
	
	$qry = $exec->execute("SELECT * FROM drb, agent, drbtype WHERE drb.agent_id=agent.agent_id AND drb.drbtype_id=drbtype.drbtype_id AND drb_id='$_POST[btn_edit]'");
	$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
	
	$drb_date = explode('-', $rs['drb_date']);
    $drb_date = $drb_date[2].'-'.$drb_date[1].'-'.$drb_date[0];
    $drb_receivedate = explode('-', $rs['drb_receivedate']);
    $drb_receivedate = $drb_receivedate[2].'-'.$drb_receivedate[1].'-'.$drb_receivedate[0];	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/prototype.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ระบบจัดการครุภัณฑ์ คณะบริหารธุรกิจ มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.min.css" />
    <script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <!-- InstanceBeginEditable name="head" -->
    <style type="text/css">
		.ui-tabs{ font-family:tahoma; font-size:11px; }
		.ui-datepicker{ width:220px; font-family:tahoma; font-size:11px; text-align:center; }
		.ui-menu .ui-menu-item a{ height:14px; font-family:tahoma; font-size:12px; }
	</style> 
    <script>
		$(document).ready(function() {
			loaddrbtype();
			loadunit();
			$('#tbldrbno').hide();
			
			$('.formatint').on('keypress',function(){
				if (event.keyCode < 48 || event.keyCode > 57) {
					event.preventDefault();
				}
			});
			
			$('.formatfloat').on('keypress',function(){
				if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode != 46 || $(this).val().indexOf('.') != -1)) {
					event.preventDefault();
				}
			});
			
			$('.formatstring').on('keypress',function(){
				console.log(event.keyCode);
				if ((event.keyCode < 97 || event.keyCode > 122) && ((event.keyCode < 3585 || event.keyCode > 3660))) {
					event.preventDefault();
				}
			});
			
			$('#drbtype_id').on('change', function(){
				$.ajax({
					type: 'POST',
					url: 'load_contr.php',
					data: { method: 'drbtype_deper', drbtype_id: $('#drbtype_id').val() },
					success: function(data){
						var obj = $.parseJSON(data);
						$.each(obj, function(i,v){
							$('#drbtype_deper').val(v['drbtype_deper']);
						});
					}
				});
			});
			
			$('#drb_import, #drb_price').change(function(){
				if($('#drb_price').val().length !=0 && $('#drb_import').val().length != 0){
					$('#drb_total').val($('#drb_price').val()*$('#drb_import').val());
				}
			});
		});
		
		$(function() {
			$('.autocomplete').autocomplete({
				source: 'autocomplete.php?method=agent',
				minLength: 1,
				focus: function(event, ui){
					$('.autocomplete').val(ui.item.agent_name);
				},
				select: function(event, ui){
					$('#agent_addr').val(ui.item.agent_addr);
					$('#agent_tel').val(ui.item.agent_tel);
					$('#agent_id').val(ui.item.agent_id);
					return false;
				}
			})
			.data('ui-autocomplete')._renderItem = function(ul,item){
				return $('<li>')
					.append('<a>'+item.agent_name+'</a>')
					.appendTo(ul);
			};
			
			$('#nowdate').datepicker({dateFormat: 'dd-mm-yy'}).datepicker('setDate',new Date());
			
			$('.datepicker').datepicker({
				dateFormat: 'dd-mm-yy',
				dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
				monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
				changeMonth: true,  
				changeYear: true,
				beforeShow:function(){    
					if($(this).val() != ''){  
						var arrayDate=$(this).val().split('-');       
						arrayDate[2]=parseInt(arrayDate[2]);  
						$(this).val(arrayDate[0]+'-'+arrayDate[1]+'-'+arrayDate[2]);  
					}  
					setTimeout(function(){
						$.each($('.ui-datepicker-year option'),function(j,k){  
							var textYear=parseInt($('.ui-datepicker-year option').eq(j).val());  
							$('.ui-datepicker-year option').eq(j).text(textYear);  
						});               
					},50);  
				},  
				onChangeMonthYear: function(){
					setTimeout(function(){
						$.each($('.ui-datepicker-year option'),function(i,v){  
							var textYear=parseInt($('.ui-datepicker-year option').eq(i).val());  
							$('.ui-datepicker-year option').eq(i).text(textYear);  
						});               
					},50);        
				},  
				onClose:function(){
					if($(this).val() != '' && $(this).val() == dateBefore){           
						var arrayDate=dateBefore.split('-');  
						arrayDate[2]=parseInt(arrayDate[2]);  
						$(this).val(arrayDate[0]+'-'+arrayDate[1]+'-'+arrayDate[2]);      
					}         
				},  
				onSelect: function(dateText, inst){
					dateBefore=$(this).val();  
					var arrayDate=dateText.split('-');  
					arrayDate[2]=parseInt(arrayDate[2]);  
					$(this).val(arrayDate[0]+'-'+arrayDate[1]+'-'+arrayDate[2]);  
				}
			});
		});
		
		function loaddrbtype(){
			$('#drbtype_id').empty();
			$.ajax({
				type: 'POST',
				url: 'load_contr.php',
				data: { method: 'drbtype' },
				success: function(data){
					var obj = $.parseJSON(data);
					$('#drbtype_id').append('<option value="null">-- กรุณาเลือก --</option>');
					$.each(obj, function(i,v){
						if(v['drbtype_id'] == "<?=$rs['drbtype_id'];?>"){
							$('#drbtype_id').append('<option value="'+v['drbtype_id']+'" selected>'+v['drbtype_name']+'</option>');
						}else{
							$('#drbtype_id').append('<option value="'+v['drbtype_id']+'">'+v['drbtype_name']+'</option>');
						}
					});
				}
			});
		}
		
		function loadunit(){
			$('#unit_id').empty();
			$.ajax({
				type: 'POST',
				url: 'load_contr.php',
				data: { method: 'unit' },
				success: function(data){
					var obj = $.parseJSON(data);
					$('#unit_id').append('<option value="null">-- กรุณาเลือก --</option>');
					$.each(obj, function(i,v){
						if(v['unit_id'] == "<?=$rs['unit_id'];?>"){
							$('#unit_id').append('<option value="'+v['unit_id']+'" selected>'+v['unit_name']+'</option>');
						}else{
							$('#unit_id').append('<option value="'+v['unit_id']+'">'+v['unit_name']+'</option>');
						}
					});
				}
			});
		}
		
		function loaddrbstatus(){
			$('.drbstatus_id').empty();
			$.ajax({
				type: 'POST',
				url: 'load_contr.php',
				data: { method: 'drbstatus' },
				success: function(data){
					var obj = $.parseJSON(data);
					$('.drbstatus_id').append('<option value="null">-- กรุณาเลือก --</option>');
					$.each(obj, function(i,v){
						if(v['drbstatus_id'] == '1')
							$('.drbstatus_id').append('<option value="'+v['drbstatus_id']+'" selected>'+v['drbstatus_name']+'</option>');
						else
							$('.drbstatus_id').append('<option value="'+v['drbstatus_id']+'">'+v['drbstatus_name']+'</option>');
					});
				}
			});
		}
		
		function readFile(input){
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#myimg').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
	</script>
	<!-- InstanceEndEditable -->
</head>
<body>
	<div id="header"></div>
    <div id="menu">
    	<?php
			include 'login.php';
		?>
    </div>
    <div id="middle">
    	<div id="info"><!-- InstanceBeginEditable name="content" -->
        	<form action="" method="post" enctype="multipart/form-data">
                <fieldset class="box1">
                    <legend>ข้อมูลรายการครุภัณฑ์</legend>
                    <label class="lbl">วันที่บันทึก</label><input name="drb_date" type="text" class="datepicker" value="<?=$drb_date;?>" /><br />
                    <label class="lbl">เลขที่เอกสาร</label><input name="drb_no" type="text" value="<?=$rs['drb_no'];?>" /><br />
                    <label class="lbl">รายละเอียด</label><textarea name="drb_detail"><?=$rs['drb_detail'];?></textarea><br />
                    <label class="lbl">ประเภทเงิน</label><select id="typeof" name="typeof_id">
                            <option value="null">-- กรุณาเลือก --</option>
                            <?php
                                $qry = $exec->execute('SELECT * FROM typeof');
                                while ($temprs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
                                    if ($temprs['typeof_id'] == $rs['typeof_id'])
                                        echo "<option value=$temprs[typeof_id] selected=selected>$temprs[typeof_name]</option>";
                                    else
                                        echo "<option value=$temprs[typeof_id]>$temprs[typeof_name]</option>";
                                }
                            ?>
                        </select>
                        ปี พ.ศ. <input class="formatint" name="typeof_detail" type="text" maxlength="4" value="<?=$rs['typeof_detail'];?>" size="4" /><br />
                        <label class="lbl">วิธีการได้มา</label><select id="acquirement" name="acq_id">
                            <option value="null">-- กรุณาเลือก --</option>
                            <?php
                                $qry = $exec->execute('SELECT * FROM acquirement');
                                while ($temprs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
                                    if ($temprs['acq_id'] == $rs['acq_id'])
                                        echo "<option value=$temprs[acq_id] selected=selected>$temprs[acq_name]</option>";
                                    else
                                        echo "<option value=$temprs[acq_id]>$temprs[acq_name]</option>";
                                }
                            ?>
                        </select>
                        <input id="drb_id" name="drb_id" type="hidden" value="<?=$_POST['btn_edit'];?>" readonly="readonly" />
                </fieldset>
                <fieldset class="box1">
                    <legend>ข้อมูลผู้ขาย / ผู้รับจ้าง / ผู้บริจาค</legend>
                    <label class="lbl">ชื่อ</label><input id="agent_name" class="autocomplete" name="agent_name" type="text" value="<?=$rs['agent_name'];?>" /><input type="submit" value="เพิ่ม" onclick="window.open('agent_ins.php','เพิ่มผู้ขาย/ผู้รับจ้า/ผู้บริจาค','width=512,height=160,toolbar=1,resizable=0');" /><br />
                    <label class="lbl">ที่อยู่</label><textarea id="agent_addr" name="agent_addr" readonly="readonly"><?=$rs['agent_addr'];?></textarea><br />
                    <label class="lbl">เบอร์โทร</label><input id="agent_tel" name="agent_tel" type="text" value="<?=$rs['agent_tel'];?>" readonly="readonly" /><br />
                    <label class="lbl">ชื่อผู้ติดต่อ</label><input class="formatstring" name="agent_contact" type="text" value="<?=$rs['agent_contact'];?>" /><br />
                    <label class="lbl">เบอร์โทรผู้ติดต่อ</label><input class="formatint" name="agent_telcontact" type="text" value="<?=$rs['agent_telcontac'];?>" maxlength="10" /><br />
                    <input id="agent_id" name="agent_id" type="hidden" value="<?=$rs['agent_id'];?>" readonly="readonly" />
                </fieldset>
                <!--<fieldset>
                    <legend>รายการครุภัณฑ์</legend>
                    <iframe width="987" src="drb_disp.php"></iframe>
                </fieldset>-->
                <fieldset>
                    <legend>ข้อมูลครุภัณฑ์</legend>
                    <div class="box2">
                        <br />
                        <label class="lbl">ประเภทครุภัณฑ์</label ><select id="drbtype_id" name="drbtype_id"></select>
                        <input type="submit" value="เพิ่ม" onclick="window.open('drbtype_ins.php','เพิ่มประเภทครุภัณฑ์','width=400,height=128,toolbar=1,resizable=0');" /><br />
                        <label class="lbl">ชื่อครุภัณฑ์</label><input name="drb_name" value="<?=$rs['drb_name'];?>" type="text" /><br />
                        <label class="lbl">ยี่ห้อ</label><input name="drb_band" value="<?=$rs['drb_band'];?>" type="text" /><br />
                        <label class="lbl">รุ่น/แบบ</label><input name="drb_model" type="text" value="<?=$rs['drb_model'];?>"  /><br />
                        <label class="lbl">วันที่ได้รับครุภัณฑ์</label><input name="drb_receivedate" class="datepicker" type="text" value="<?=$drb_receivedate;?>" /><br />
                        <label class="lbl">หน่วยนับ</label><select id="unit_id" name="unit_id"></select>
                        <input type="submit" value="เพิ่ม" onclick="window.open('unit_ins.php','เพิ่มหน่วยนับครุภัณฑ์','width=400,height=128,toolbar=1,resizable=0');" /><br />
                        <label class="lbl">ราคา/หน่วย</label><input id="drb_price" class="formatfloat" name="drb_price" type="text" value="<?=$rs['drb_price'];?>" size="5" readonly="readonly" style="background-color:#CCC;" /><label class="tail">บาท</label><br />
                        <label class="lbl">อัตราค่าเสื่อม/ปี</label><input id="drbtype_deper" class="formatfloat" name="drb_deper" type="text" value="<?=$rs['drbtype_deper'];?>" size="5" /><label class="tail">%</label><br />
                        <label class="lbl">ราคารวมทั้งหมด</label><input id="drb_total" class="formatfloat" name="drb_total" type="text" value="<?=$rs['drb_total'];?>" size="5"  readonly="readonly" style="background-color:#CCC;"/><label class="tail">บาท</label><br />
                        <label class="lbl">อายุการใช้งาน</label><input class="formatint" name="drb_lt" type="text" value="<?=$rs['drb_lt'];?>" maxlength="2" size="5" /><label class="tail">ปี</label><br />
                        <label class="lbl">ระยะเวลารับประกัน</label><input class="formatint" name="drb_warrenty" type="text" value="<?=$rs['drb_warrenty'];?>" maxlength="2" size="5" /><label>ปี</label><br />
                    </div>
                    <div class="box3">
                        <img id="myimg" src="<?=$rs['drb_path'];?>" />
                        <input name="drb_path" type="file" onchange="readFile(this);" />
                    </div>
                </fieldset>
                <center><button id="btn" name="btn" value="save">บันทึกข้อมูล</button></center>
            </form>
            <?php
                if($_POST['btn'] == 'save'){
                    if(!empty($_POST)){
                        if(empty($_POST['drb_date'])){
                            echo '<script>alert(\'กรุณาเลือกวันที่บันทึก\');</script>';
							return false;
                        }elseif(empty($_POST['drb_no'])){
							echo '<script>alert(\'กรุณากรอกเลขที่เอกสาร\');</script>';
							return false;
                            //echo 'drb_no';
                        }elseif(empty($_POST['drb_detail'])){
							echo '<script>alert(\'กรุณากรอกรายละเอียดเอกสาร\');</script>';
							return false;
                        //	echo 'drb_detail';
                        }elseif(empty($_POST['agent_id'])){
							echo '<script>alert(\'กรุณากรอกชื่อผู้ขาย / ผู้รับจ้าง / ผู้ติดต่อ\');</script>';
							return false;
                        //	echo 'agent_id';
                        }elseif(empty($_POST['agent_contact'])){
							echo '<script>alert(\'กรุณากรอกชื่อผู้ติดต่อ\');</script>';
							return false;
                        //	echo 'agent_contact';
                        }elseif(empty($_POST['agent_telcontact'])){
							echo '<script>alert(\'กรุณากรอกเบอร์โทรศัพท์ผู้ติดต่อ\');</script>';
							return false;
                        //	echo 'agent_telcontact';
                        }elseif($_POST['typeof_id'] == 'null'){
							echo '<script>alert(\'กรุณาเลือกประเภทเงิน\');</script>';
							return false;
                        //	echo 'typeof_id';
                        }elseif(empty($_POST['typeof_detail'])){
							echo '<script>alert(\'กรุณากรอกปี พ.ศ.\');</script>';
							return false;
                        //	echo 'acq_id';
                        }elseif($_POST['acq_id'] == 'null'){
							echo '<script>alert(\'กรุณาเลือกวิธีการได้มา\');</script>';
							return false;
                        //	echo 'acq_id';
                        }elseif($_POST['drbtype_id'] == 'null'){
							echo '<script>alert(\'กรุณาเลือกประเภทครุภัณฑ์\');</script>';
							return false;
                        //	echo 'drbtype_id';
                        }elseif(empty($_POST['drb_name'])){
							echo '<script>alert(\'กรุณากรอกชื่อครุภัณฑ์\');</script>';
							return false;
                            //echo 'drb_name';
                        }elseif(empty($_POST['drb_band'])){
							echo '<script>alert(\'กรุณากรอกยี่ห้อครุภัณฑ์\');</script>';
							return false;
                            //echo 'drb_band';
                        }elseif(empty($_POST['drb_receivedate'])){
							echo '<script>alert(\'กรุณาเลือกวันที่ได้รับครุภัณฑ์\');</script>';
							return false;
                            //echo 'drb_receivedate';
                        }elseif($_POST['unit_id'] == 'null'){
							echo '<script>alert(\'กรุณาเลือกหน่วยนับ\');</script>';
							return false;
                            //echo 'unit_id';
                        }elseif(empty($_POST['drb_price'])){
							echo '<script>alert(\'กรุณากรอกราคา/หน่วย\');</script>';
							return false;
                            //echo 'drb_price';
                        }elseif(empty($_POST['drb_lt'])){
							echo '<script>alert(\'กรุณากรอกอายุการใช้งาน\');</script>';
							return false;
                            //echo 'drb_lt';
                        }/*elseif(empty($_FILES['drb_path'])){
                            echo 'drb_path';
                        }*/else{                            
                            $drb_date = explode('-', $_POST['drb_date']);
                            $drb_date = $drb_date[2].'-'.$drb_date[1].'-'.$drb_date[0];
                            $drb_receivedate = explode('-', $_POST['drb_receivedate']);
                            $drb_receivedate = $drb_receivedate[2].'-'.$drb_receivedate[1].'-'.$drb_receivedate[0];			
                            $targetPath = 'drb_img/';
							if(!empty($_FILES['drb_path']['name'])){
								$tempFile = $_FILES['drb_path']['tmp_name'];
								$drb_path = $targetPath . date('Ymd-His') . '-' . $_FILES['drb_path']['name'];
								
								$fileTypes = array('jpg','jpeg');
								$fileParts = pathinfo($_FILES['drb_path']['name']);
									
								if (in_array($fileParts['extension'],$fileTypes)) {
									move_uploaded_file($tempFile,$drb_path);
								}
								
								$exec->execute("UPDATE drb SET drb_date='$drb_date', drb_no='$_POST[drb_no]', drb_detail='$_POST[drb_detail]', agent_id='$_POST[agent_id]', agent_contact='$_POST[agent_contact]', agent_telcontac='$_POST[agent_telcontact]', typeof_id='$_POST[typeof_id]', typeof_detail='$_POST[typeof_detail]',acq_id='$_POST[acq_id]', drbtype_id='$_POST[drbtype_id]', drb_name='$_POST[drb_name]', drb_band='$_POST[drb_band]', drb_model='$_POST[drb_model]', drb_receivedate='$drb_receivedate', unit_id='$_POST[unit_id]', drb_price='$_POST[drb_price]', drb_lt='$_POST[drb_lt]', drb_warrenty='$_POST[drb_warrenty]', drb_path='$drb_path' WHERE drb_id='$_POST[drb_id]'");
							}else{
								$exec->execute("UPDATE drb SET drb_date='$drb_date', drb_no='$_POST[drb_no]', drb_detail='$_POST[drb_detail]', agent_id='$_POST[agent_id]', agent_contact='$_POST[agent_contact]', agent_telcontac='$_POST[agent_telcontact]', typeof_id='$_POST[typeof_id]', typeof_detail='$_POST[typeof_detail]',acq_id='$_POST[acq_id]', drbtype_id='$_POST[drbtype_id]', drb_name='$_POST[drb_name]', drb_band='$_POST[drb_band]', drb_model='$_POST[drb_model]', drb_receivedate='$drb_receivedate', unit_id='$_POST[unit_id]', drb_price='$_POST[drb_price]', drb_lt='$_POST[drb_lt]', drb_warrenty='$_POST[drb_warrenty]' WHERE drb_id='$_POST[drb_id]'");
							}
							
                            #echo "UPDATE drb SET drb_date='$drb_date', drb_no='$_POST[drb_no]', drb_detail='$_POST[drb_detail]', agent_id='$_POST[agent_id]', agent_contact='$_POST[agent_contact]', agent_telcontac='$_POST[agent_telcontact]', typeof_id='$_POST[typeof_id]', typeof_detail='$_POST[typeof_detail]',acq_id='$_POST[acq_id]', drbtype_id='$_POST[drbtype_id]', drb_name='$_POST[drb_name]', drb_band='$_POST[drb_band]', drb_model='$_POST[drb_model]', drb_receivedate='$drb_receivedate', unit_id='$_POST[unit_id]', drb_price='$_POST[drb_price]', drb_lt='$_POST[drb_lt]', drb_warrenty='$_POST[drb_warrenty]', drb_path='$drb_path' WHERE drb_id='$_POST[drb_id]'<br />";
                            
                           $log->write_log($_SESSION['auth']['name'].'->durable_edit:'.$_POST['drb_id']);
                            echo "<script>alert('แก้ไขข้อมูลครุภัณฑ์เรียบร้อยแล้ว');</script>";
                            header("refresh:0;drb_srh.php");
                        }
                    }else{
                        header("location:index.php");
                    }
                }
            ?>
		<!-- InstanceEndEditable --></div>
    </div>
    <div id="footer"></div>
</body>
<!-- InstanceEnd --></html>
