<?php
	require_once 'database.php';
	require_once 'log.php';
    session_start();
	ob_start();
    $exec = database::getInstance();
	
	if (!empty($_POST['cancel'])){
		/*$qry = $exec->execute("SELECT * FROM drbrepair WHERE drbno_id='$_POST[cancel]'");
		$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);*/
		$exec->execute("UPDATE drbno SET drbno_status='1' WHERE drbno_id='$_POST[cancel]'");
		$exec->execute("DELETE FROM drbdispose WHERE drbno_id='$_POST[cancel]'");
		header("location:drbdispose_srh.php");
		
	}
    
    $qry = $exec->execute("SELECT * FROM drb, drbno, drbdispose, agent, drbtype WHERE drb.agent_id=agent.agent_id AND drb.drbtype_id=drbtype.drbtype_id AND drb.drb_id=drbno.drb_id AND drbno.drbno_id=drbdispose.drbno_id AND drbno.drbno_id='$_POST[drbno_id]'");
    #echo "SELECT * FROM drb, drbno, drbdispose, agent, drbtype WHERE drb.agent_id=agent.agent_id AND drb.drbtype_id=drbtype.drbtype_id AND drb.drb_id=drbno.drb_id AND drbno.drbno_id=drbdispose.drbno_id AND drbno_id='$_POST[drbno_id]'";
	$rs = mysqli_fetch_array($qry, MYSQLI_ASSOC);
	
	$dispose_date = explode('-', $rs['dispose_date']);
    $dispose_date = $dispose_date[2].'-'.$dispose_date[1].'-'.$dispose_date[0];
    $drb_receivedate = explode('-', $rs['drb_receivedate']);
    $drb_receivedate = $drb_receivedate[2].'-'.$drb_receivedate[1].'-'.$drb_receivedate[0];	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/prototype.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ระบบจัดการครุภัณฑ์ คณะบริหารธุรกิจ มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.min.css" />
    <script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <!-- InstanceBeginEditable name="head" -->
    <style type="text/css">
		.ui-tabs{ font-family:tahoma; font-size:11px; }
		.ui-datepicker{ width:220px; font-family:tahoma; font-size:11px; text-align:center; }
		.ui-menu .ui-menu-item a{ height:14px; font-family:tahoma; font-size:12px; }
	</style> 
    <script>
		$(document).ready(function() {
			loaddrbtype();
			loadunit();
			$('#tbldrbno').hide();
			
			$('.formatint').on('keypress',function(){
				if (event.keyCode < 48 || event.keyCode > 57) {
					event.preventDefault();
				}
			});
			
			$('.formatfloat').on('keypress',function(){
				if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode != 46 || $(this).val().indexOf('.') != -1)) {
					event.preventDefault();
				}
			});
			
			$('.formatstring').on('keypress',function(){
				console.log(event.keyCode);
				if ((event.keyCode < 97 || event.keyCode > 122) && ((event.keyCode < 3585 || event.keyCode > 3660))) {
					event.preventDefault();
				}
			});
			
			$('#drbtype_id').on('change', function(){
				$.ajax({
					type: 'POST',
					url: 'load_contr.php',
					data: { method: 'drbtype_deper', drbtype_id: $('#drbtype_id').val() },
					success: function(data){
						var obj = $.parseJSON(data);
						$.each(obj, function(i,v){
							$('#drbtype_deper').val(v['drbtype_deper']);
						});
					}
				});
			});
			
			$('#drb_import, #drb_price').change(function(){
				if($('#drb_price').val().length !=0 && $('#drb_import').val().length != 0){
					$('#drb_total').val($('#drb_price').val()*$('#drb_import').val());
				}
			});
		});
		
		$(function() {			
			$('#nowdate').datepicker({dateFormat: 'dd-mm-yy'}).datepicker('setDate',new Date());
			
			$('.datepicker').datepicker({
				dateFormat: 'dd-mm-yy',
				dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
				monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
				changeMonth: true,  
				changeYear: true,
				beforeShow:function(){    
					if($(this).val() != ''){  
						var arrayDate=$(this).val().split('-');       
						arrayDate[2]=parseInt(arrayDate[2]);  
						$(this).val(arrayDate[0]+'-'+arrayDate[1]+'-'+arrayDate[2]);  
					}  
					setTimeout(function(){
						$.each($('.ui-datepicker-year option'),function(j,k){  
							var textYear=parseInt($('.ui-datepicker-year option').eq(j).val());  
							$('.ui-datepicker-year option').eq(j).text(textYear);  
						});               
					},50);  
				},  
				onChangeMonthYear: function(){
					setTimeout(function(){
						$.each($('.ui-datepicker-year option'),function(i,v){  
							var textYear=parseInt($('.ui-datepicker-year option').eq(i).val());  
							$('.ui-datepicker-year option').eq(i).text(textYear);  
						});               
					},50);        
				},  
				onClose:function(){
					if($(this).val() != '' && $(this).val() == dateBefore){           
						var arrayDate=dateBefore.split('-');  
						arrayDate[2]=parseInt(arrayDate[2]);  
						$(this).val(arrayDate[0]+'-'+arrayDate[1]+'-'+arrayDate[2]);      
					}         
				},  
				onSelect: function(dateText, inst){
					dateBefore=$(this).val();  
					var arrayDate=dateText.split('-');  
					arrayDate[2]=parseInt(arrayDate[2]);  
					$(this).val(arrayDate[0]+'-'+arrayDate[1]+'-'+arrayDate[2]);  
				}
			});
		});
		
		function loaddrbtype(){
			$('#drbtype_id').empty();
			$.ajax({
				type: 'POST',
				url: 'load_contr.php',
				data: { method: 'drbtype' },
				success: function(data){
					var obj = $.parseJSON(data);
					$('#drbtype_id').append('<option value="null">-- กรุณาเลือก --</option>');
					$.each(obj, function(i,v){
						if(v['drbtype_id'] == "<?=$rs['drbtype_id'];?>"){
							$('#drbtype_id').append('<option value="'+v['drbtype_id']+'" selected>'+v['drbtype_name']+'</option>');
						}else{
							$('#drbtype_id').append('<option value="'+v['drbtype_id']+'">'+v['drbtype_name']+'</option>');
						}
					});
				}
			});
		}
		
		function loadunit(){
			$('#unit_id').empty();
			$.ajax({
				type: 'POST',
				url: 'load_contr.php',
				data: { method: 'unit' },
				success: function(data){
					var obj = $.parseJSON(data);
					$('#unit_id').append('<option value="null">-- กรุณาเลือก --</option>');
					$.each(obj, function(i,v){
						if(v['unit_id'] == "<?=$rs['unit_id'];?>"){
							$('#unit_id').append('<option value="'+v['unit_id']+'" selected>'+v['unit_name']+'</option>');
						}else{
							$('#unit_id').append('<option value="'+v['unit_id']+'">'+v['unit_name']+'</option>');
						}
					});
				}
			});
		}
		
		function loaddrbstatus(){
			$('.drbstatus_id').empty();
			$.ajax({
				type: 'POST',
				url: 'load_contr.php',
				data: { method: 'drbstatus' },
				success: function(data){
					var obj = $.parseJSON(data);
					$('.drbstatus_id').append('<option value="null">-- กรุณาเลือก --</option>');
					$.each(obj, function(i,v){
						if(v['drbstatus_id'] == '1')
							$('.drbstatus_id').append('<option value="'+v['drbstatus_id']+'" selected>'+v['drbstatus_name']+'</option>');
						else
							$('.drbstatus_id').append('<option value="'+v['drbstatus_id']+'">'+v['drbstatus_name']+'</option>');
					});
				}
			});
		}
		
		function readFile(input){
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#myimg').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
	</script>
	<!-- InstanceEndEditable -->
</head>
<body>
	<div id="header"></div>
    <div id="menu">
    	<?php
			include 'login.php';
		?>
    </div>
    <div id="middle">
    	<div id="info"><!-- InstanceBeginEditable name="content" -->
        	<form action="" method="post">
                <fieldset>
                    <legend>ข้อมูลการแทงจำหน่ายครุภัณฑ์</legend>
                    <label class="lbl">วันที่แทงจำหน่าย</label><input class="datepicker" name="dispose_date" type="text" value="<?=$dispose_date;?>" /><br />
                    <label class="lbl">สาเหตุ</label><select name="dispose_cause">
                    	<option>ชำรุด</option>
                        <option>บริจาค</option>
                    </select><br />
                    <label class="lbl">หมายเหตุ</label><textarea name="dispose_note"><?=$rs['dispose_note'];?></textarea>
                </fieldset>
                <fieldset>
                    <legend>ข้อมูลครุภัณฑ์</legend>
                    	<div class="box2">
                            <label class="lbl">เลขที่เอกสาร</label><input name="drb_no" type="text" value="<?=$rs['drb_no'];?>" readonly="readonly" /><br />
                            <label class="lbl">รายละเอียด</label><textarea name="drb_detail" readonly="readonly"><?=$rs['drb_detail'];?></textarea><br />
                            <label class="lbl">ประเภทครุภัณฑ์</label ><select id="drbtype_id" name="drbtype_id" disabled="disabled"></select><br />
                            <label class="lbl">ชื่อครุภัณฑ์</label><input name="drb_name" value="<?=$rs['drb_name'];?>" type="text" readonly="readonly" /><br />
                            <label class="lbl">ยี่ห้อ</label><input name="drb_band" value="<?=$rs['drb_band'];?>" type="text" readonly="readonly" /><br />
                            <label class="lbl">รุ่น/แบบ</label><input name="drb_model" type="text" value="<?=$rs['drb_model'];?>" readonly="readonly"  /><br />
                            <label class="lbl">วันที่ได้รับครุภัณฑ์</label><input name="drb_receivedate" class="datepicker" type="text" value="<?=$drb_receivedate;?>" readonly="readonly" /><br />
                        	<input id="drbno_id" name="drbno_id" type="hidden" value="<?=$_POST['drbno_id'];?>" readonly="readonly" />
                        </div>
                        <div class="box3">
                            <img id="myimg" src="<?=$rs['drb_path'];?>" />
                        </div>
                        <!--<table id="tbldrbno">
                            <thead>
                                <th>ลำดับ</th>
                                <th>หมายเลขครุภัณฑ์</th>
                                <th>ชื่อครุภัณฑ์</th>
                                <th>สถานะปัจจุบัน</th>
                                <th>ผู้รับผิดชอบ</th>
                                <th>สถานะประกัน</th>
                            </thead>
                            <tbody>
                                <?php
                                    /*if(empty($_SESSION['drbno_id']))
                                        $_SESSION['drbno_id'] = $_POST['drbno_id'];
											
                                    $i = 0;
                                    foreach($_SESSION['drbno_id'] as $v){
                                        $qry = $exec->execute("SELECT * FROM drbno, drb, drbstatus, respons WHERE drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND respons.resp_id=drbno.resp_id AND drbno.drbno_status='1' AND drbno.drbno_id='$v' ORDER BY drbno_id ASC");
                                        if(mysqli_num_rows($qry) != 0){
                                            while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
                                                $i++;
                                                $culdate = date('Y-m-d', strtotime("+$rs[drb_warrenty] year", strtotime($rs['drb_receivedate'])));
                                                $nowdate = date('Y-m-d');
                                                if($culdate > $nowdate)
                                                	echo "<tr><td>$i</td><td>$rs[drbno_number]</td><td>$rs[drb_name]</td><td>$rs[drbstatus_name]</td><td>$rs[resp_name]</td><td>อยู่ในระยะรับประกัน</td><input name=\"drbno_id[]\" type=\"hidden\" value=\"$rs[drbno_id]\" /><tr>";
                                            	else
                                                	echo "<tr><td>$i</td><td>$rs[drbno_number]</td><td>$rs[drb_name]</td><td>$rs[drbstatus_name]</td><td>$rs[resp_name]</td><td>หมดประกัน</td><input name=\"drbno_id[]\" type=\"hidden\" value=\"$rs[drbno_id]\" /><tr>";
                                            }
                                        }
                                    }
									if($i == 0){
                                        unset($_SESSION['drbno_id']);
                                    	header('location:drbdispose_disp.php');
                                    }*/
                                ?>
                            </tbody>
                        </table>-->
                       <center> <button name="btn" value="save">บันทึก</button><button name="btn" value="back">ย้อนกลับ</button></center>
                </fieldset>
            </form>
            <?php
				if($_POST['btn'] == 'save'){
					$dispose_date = explode('-', $_POST['dispose_date']);
                    $dispose_date = $dispose_date[2].'-'.$dispose_date[1].'-'.$dispose_date[0];
					$exec->execute("UPDATE drbdispose SET dispose_date='$dispose_date', dispose_cause='$_POST[dispose_cause]', dispose_note='$_POST[dispose_note]' WHERE drbno_id='$_POST[drbno_id]'");
					
					$log->write_log($_SESSION['auth']['name'].'->dulable_dispose_edit:'.$_POST['drbno_id']);
					
					echo "<script>alert('แก้ไขแทงจำหน่ายครุภัณฑ์เรียบร้อยแล้ว');</script>";
                    header("refresh:0;drbdispose_srh.php");
				}elseif($_POST['btn'] == 'back'){
                    header("location:drbdispose_srh.php");
                }
			?>
		<!-- InstanceEndEditable --></div>
    </div>
    <div id="footer"></div>
</body>
<!-- InstanceEnd --></html>
