<?php
	require_once 'pagination.php';
	
	class database extends pagination{
		public static $local_db, $conn;
		public $host, $user, $pass, $db, $cs;
		
		public function __construct(){
			parent::__construct();
			
			$this->host = '127.0.0.1';
			$this->user = 'root';
			$this->pass = 'root';
			$this->db = 'pj_db';
			$this->cs = 'UTF8';
			
			$this->$conn = new mysqli($this->host, $this->user, $this->pass, $this->db);
/*
			mysql_connect($this->host, $this->user, $this->pass);
			mysql_select_db($this->db);*/
			mysqli_query($this->$conn,"SET NAMES '$this->cs'");
		}
		
		public static function getInstance(){
			if(!self::$local_db){
				self::$local_db = new database();
			}
			return self::$local_db;
		}
		
		public function execute($strSQL){
			return mysqli_query($this->$conn, $strSQL);
		}
	}
	
	/*$obj = database::getInstance();
    $obj->genpage("SELECT * FROM drb, drbno WHERE drb.drb_id=drbno.drb_id"); 
	
    $obj->link(); */
?>