-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- โฮสต์: localhost
-- เวลาในการสร้าง: 
-- รุ่นของเซิร์ฟเวอร์: 5.0.51
-- รุ่นของ PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- ฐานข้อมูล: `pj_db`
-- 
CREATE DATABASE `pj_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `pj_db`;

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `acquirement`
-- 

CREATE TABLE `acquirement` (
  `acq_id` varchar(1) collate utf8_unicode_ci NOT NULL,
  `acq_name` varchar(100) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`acq_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `acquirement`
-- 

INSERT INTO `acquirement` VALUES ('1', 'ตกลงราคา');
INSERT INTO `acquirement` VALUES ('2', 'สอบถามราคา');
INSERT INTO `acquirement` VALUES ('3', 'ประกวดราคา');
INSERT INTO `acquirement` VALUES ('4', 'วิธีพิเศษ');
INSERT INTO `acquirement` VALUES ('5', 'วิธีรับบริจาค');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `agent`
-- 

CREATE TABLE `agent` (
  `agent_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  `agent_name` varchar(100) collate utf8_unicode_ci NOT NULL,
  `agent_addr` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `agent_tel` varchar(10) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`agent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `agent`
-- 

INSERT INTO `agent` VALUES ('A001', 'บริษัท มีเดียเน็ทเวิร์ค จำกัด', 'สวนหลวง กรุงเทพฯ', '027424674');
INSERT INTO `agent` VALUES ('A002', 'บริษัท Advice Computer จำกัด', 'ราชเทวี กรุงเทพฯ', '029943585');
INSERT INTO `agent` VALUES ('A003', 'บริษัท ABC จำกัด', 'สาทร ซอย 10', '083-451-54');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `area`
-- 

CREATE TABLE `area` (
  `area_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  `area_name` varchar(50) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`area_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `area`
-- 

INSERT INTO `area` VALUES ('A001', 'เทคนิคกรุงเทพ');
INSERT INTO `area` VALUES ('A002', 'บพิตรพิมุข มหาเมฆ');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `auth`
-- 

CREATE TABLE `auth` (
  `auth_id` varchar(11) collate utf8_unicode_ci NOT NULL,
  `auth_user` varchar(20) collate utf8_unicode_ci NOT NULL,
  `auth_pass` varchar(20) collate utf8_unicode_ci NOT NULL,
  `perm_id` varchar(1) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`auth_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `auth`
-- 

INSERT INTO `auth` VALUES ('06172014-01', 'admin', 'admin', '1');
INSERT INTO `auth` VALUES ('07162014-06', 'user', 'user', '2');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `building`
-- 

CREATE TABLE `building` (
  `building_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  `building_name` varchar(50) collate utf8_unicode_ci NOT NULL,
  `area_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`building_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `building`
-- 

INSERT INTO `building` VALUES ('B001', 'อาคาร 50 ปี', 'A001');
INSERT INTO `building` VALUES ('B002', 'อาคาร 4B', 'A002');
INSERT INTO `building` VALUES ('B003', 'อาคาร 7B', 'A002');
INSERT INTO `building` VALUES ('B004', 'อาคาร 9B', 'A002');
INSERT INTO `building` VALUES ('B005', '3', 'A002');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `drb`
-- 

CREATE TABLE `drb` (
  `drb_id` varchar(5) collate utf8_unicode_ci NOT NULL,
  `drb_date` date NOT NULL,
  `drb_no` varchar(100) collate utf8_unicode_ci NOT NULL,
  `drb_detail` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `agent_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  `agent_contact` varchar(100) collate utf8_unicode_ci NOT NULL,
  `agent_telcontac` varchar(10) collate utf8_unicode_ci NOT NULL,
  `typeof_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  `typeof_detail` varchar(100) collate utf8_unicode_ci NOT NULL,
  `acq_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  `drbtype_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  `drb_name` varchar(100) collate utf8_unicode_ci NOT NULL,
  `drb_band` varchar(100) collate utf8_unicode_ci NOT NULL,
  `drb_model` varchar(100) collate utf8_unicode_ci NOT NULL,
  `drb_receivedate` date NOT NULL,
  `unit_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  `drb_price` float NOT NULL,
  `drb_total` float NOT NULL,
  `drb_depreciation` float NOT NULL,
  `drb_lt` int(11) NOT NULL,
  `drb_warrenty` int(11) NOT NULL,
  `drb_import` int(11) NOT NULL,
  `drb_path` varchar(1000) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`drb_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `drb`
-- 

INSERT INTO `drb` VALUES ('D0001', '2014-07-15', 'เล่มที่1/1-10', 'ครุภัณฑ์ห้องปฎิบัติการคอมพิวเตอร์', 'A002', 'คุณเจ', '0843434343', '1', '2557', '1', 'D006', 'ชุดคอมพิวเตอร์', 'DELL', 'Optiplex 380', '2014-07-16', 'U005', 35000, 175000, 17500, 12, 1, 5, 'drb_img/20140716-002754-veritonm670g__97543_zoom.jpg');
INSERT INTO `drb` VALUES ('D0002', '2014-07-15', 'เล่มที่1/15-20', 'ครุภัณฑ์ห้องปฎิบัติการคอมพิวเตอร์', 'A001', 'คุณตอง', '0890324234', '1', '2557', '1', 'D001', 'ชุดโต๊ะคอมพิวเตอร์', '', '', '2014-07-15', 'U005', 5000, 25000, 500, 12, 1, 5, 'drb_img/20140716-003059-spd_20091106102531_b.jpg');
INSERT INTO `drb` VALUES ('D0003', '2014-07-15', 'เล่มที่1/1-50', 'ครุภัณฑ์ห้องปฎิบัติการคอมพิวเตอร์', 'A003', 'คุณเหลือง', '0867674645', '1', '2557', '1', 'D001', 'เครื่องปรับอากาศ', 'Star air 36689BTU', 'AR561-3', '2014-07-16', 'U004', 43000, 215000, 4300, 10, 1, 5, 'drb_img/20140716-003640-Lighthouse.jpg');
INSERT INTO `drb` VALUES ('D0004', '2014-07-15', 'เล่มที่2/1-2', 'ครุภัณฑ์เครื่องฉายภาพข้ามศีรษะ \r\n', 'A003', 'คุณฟ้า', '2342342342', '1', '2557', '1', 'D004', 'โปรเจคเตอร์', 'ACER', 'ACER TJT-341', '2014-07-14', 'U004', 5500, 16500, 1100, 12, 1, 3, 'drb_img/20140716-004601-Lighthouse.jpg');
INSERT INTO `drb` VALUES ('D0005', '2014-07-16', 'เล่มที่9/1-5', 'เครื่องขยายเสียง', 'A003', 'คุณจ้า', '0834552234', '1', '2557', '1', 'D005', 'เครื่องขยายเสียง', 'sound', 'sound1-a2', '2014-07-16', 'U004', 5500, 33000, 1100, 5, 1, 6, 'drb_img/20140716-150017-10519349_0.jpg');
INSERT INTO `drb` VALUES ('D0006', '2014-07-16', 'เล่มที่5/1-5', 'ครุภัณฑ์คอมพิวเตอร์', 'A002', 'คุณเขียว', '0834567811', '1', '2557', '1', 'D006', 'ชุดคอมพิวเตอร์', 'Acer', 'ac41-1', '2014-07-16', 'U005', 36000, 108000, 18000, 12, 1, 3, 'drb_img/20140716-165246-veritonm670g__97543_zoom.jpg');
INSERT INTO `drb` VALUES ('D0007', '2014-07-16', 'test', 'test', 'A003', 'test', '3423423423', '1', '2556', '1', 'D001', 'ตู้เอกสาร', 'test', 'test', '2012-07-01', 'U003', 10000, 10000, 1000, 5, 1, 1, 'drb_img/20140716-172348-Desert.jpg');
INSERT INTO `drb` VALUES ('D0008', '2014-07-17', 'เล่มที่1/2-6', 'ชุดคอมพิวเตอร์', 'A003', 'ศรัญญา', '0834301876', '1', '2557', '1', 'D006', 'คอมพิวเตอร์', 'acer', 'aspire4736', '2014-07-17', 'U004', 15000, 30000, 7500, 2, 2, 2, 'drb_img/20140717-120846-Koala.jpg');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `drbdispose`
-- 

CREATE TABLE `drbdispose` (
  `dispose_id` varchar(10) collate utf8_unicode_ci NOT NULL,
  `dispose_date` date NOT NULL,
  `dispose_cause` varchar(100) collate utf8_unicode_ci NOT NULL,
  `dispose_note` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `drbno_id` varchar(10) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`dispose_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `drbdispose`
-- 

INSERT INTO `drbdispose` VALUES ('D000000001', '2014-08-22', 'ชำรุด', '', 'D000000003');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `drbno`
-- 

CREATE TABLE `drbno` (
  `drbno_id` varchar(10) collate utf8_unicode_ci NOT NULL,
  `drbno_number` varchar(100) collate utf8_unicode_ci NOT NULL,
  `drbno_serial` varchar(100) collate utf8_unicode_ci NOT NULL,
  `drbstatus_id` varchar(1) collate utf8_unicode_ci NOT NULL,
  `drbno_note` varchar(100) collate utf8_unicode_ci NOT NULL,
  `drbno_status` tinyint(1) NOT NULL,
  `drb_id` varchar(5) collate utf8_unicode_ci NOT NULL,
  `resp_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  `room_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  `room_note` varchar(100) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`drbno_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `drbno`
-- 

INSERT INTO `drbno` VALUES ('D000000019', '2001-001-0001-01/201', '', '1', '', 1, 'D0005', 'R004', 'R064', '');
INSERT INTO `drbno` VALUES ('D000000025', '5000-006-0110-01/101', '', '1', '', 1, 'D0006', 'R005', 'R008', '');
INSERT INTO `drbno` VALUES ('D000000015', '4120-01-001-01/005', '', '1', '', 1, 'D0003', 'R001', 'R070', '');
INSERT INTO `drbno` VALUES ('D000000016', '2012-0002-002-01/101', '', '1', '', 1, 'D0004', 'R004', 'R057', '');
INSERT INTO `drbno` VALUES ('D000000018', '2012-0002-002-01/103', '', '1', '', 1, 'D0004', 'R002', 'R127', '');
INSERT INTO `drbno` VALUES ('D000000017', '2012-0002-002-01/102', '', '1', '', 1, 'D0004', 'R002', 'R127', '');
INSERT INTO `drbno` VALUES ('D000000003', '7440-001-0001-01/03', '100003', '1', '', 3, 'D0001', 'R001', 'R069', 'คอม1');
INSERT INTO `drbno` VALUES ('D000000004', '7440-001-0001-01/04', '100004', '1', '', 1, 'D0001', 'R001', 'R069', 'คอม1');
INSERT INTO `drbno` VALUES ('D000000005', '7440-001-0001-01/05', '100005', '1', '', 1, 'D0001', 'R001', 'R069', 'คอม1');
INSERT INTO `drbno` VALUES ('D000000006', '6001-06-006-01/01', '', '1', '', 1, 'D0002', 'R001', 'R069', 'คอม1');
INSERT INTO `drbno` VALUES ('D000000007', '6001-06-006-01/02', '', '1', '', 1, 'D0002', 'R001', 'R069', 'คอม1');
INSERT INTO `drbno` VALUES ('D000000008', '6001-06-006-01/03', '', '1', '', 1, 'D0002', 'R001', 'R069', 'คอม1');
INSERT INTO `drbno` VALUES ('D000000009', '6001-06-006-01/04', '', '1', '', 1, 'D0002', 'R001', 'R069', 'คอม1');
INSERT INTO `drbno` VALUES ('D000000010', '6001-06-006-01/05', '', '1', '', 1, 'D0002', 'R001', 'R069', 'คอม1');
INSERT INTO `drbno` VALUES ('D000000011', '4120-01-001-01/001', '', '1', '', 1, 'D0003', 'R007', 'R094', '');
INSERT INTO `drbno` VALUES ('D000000012', '4120-01-001-01/002', '', '1', '', 1, 'D0003', 'R002', 'R074', '');
INSERT INTO `drbno` VALUES ('D000000014', '4120-01-001-01/004', '', '1', '', 1, 'D0003', 'R001', 'R070', '');
INSERT INTO `drbno` VALUES ('D000000013', '4120-01-001-01/003', '', '1', '', 1, 'D0003', 'R002', 'R074', '');
INSERT INTO `drbno` VALUES ('D000000001', '7440-001-0001-01/01', '100001', '1', '', 2, 'D0001', 'R001', 'R069', 'คอม1');
INSERT INTO `drbno` VALUES ('D000000002', '7440-001-0001-01/02', '100002', '1', '', 2, 'D0001', 'R001', 'R069', 'คอม1');
INSERT INTO `drbno` VALUES ('D000000020', '2001-001-0001-01/203', '', '1', '', 1, 'D0005', 'R004', 'R064', '');
INSERT INTO `drbno` VALUES ('D000000021', '2001-001-0001-01/204', '', '1', '', 1, 'D0005', 'R004', 'R064', '');
INSERT INTO `drbno` VALUES ('D000000022', '2001-001-0001-01/205', '', '1', '', 1, 'D0005', 'R004', 'R064', '');
INSERT INTO `drbno` VALUES ('D000000023', '2001-001-0001-01/206', '', '1', '', 1, 'D0005', 'R004', 'R064', '');
INSERT INTO `drbno` VALUES ('D000000024', '2001-001-0001-01/207', '', '1', '', 1, 'D0005', 'R004', 'R064', '');
INSERT INTO `drbno` VALUES ('D000000026', '5000-006-0110-01/102', '', '1', '', 1, 'D0006', 'R003', 'R075', '');
INSERT INTO `drbno` VALUES ('D000000027', '5000-006-0110-01/103', '', '1', '', 1, 'D0006', 'R003', 'R075', '');
INSERT INTO `drbno` VALUES ('D000000028', '1001-001-0001-01', '', '1', '', 1, 'D0007', 'R001', 'R068', '');
INSERT INTO `drbno` VALUES ('D000000029', '7110-006-0001-01-461', '', '1', '', 0, 'D0008', '', '', '');
INSERT INTO `drbno` VALUES ('D000000030', '7110-006-0001-01-462', '', '1', '', 0, 'D0008', '', '', '');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `drbrepair`
-- 

CREATE TABLE `drbrepair` (
  `repair_id` varchar(10) collate utf8_unicode_ci NOT NULL,
  `repair_company` varchar(100) collate utf8_unicode_ci NOT NULL,
  `repair_addr` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `repair_contact` varchar(100) collate utf8_unicode_ci NOT NULL,
  `repair_contacttel` varchar(10) collate utf8_unicode_ci NOT NULL,
  `repair_name` varchar(100) collate utf8_unicode_ci NOT NULL,
  `repair_tel` varchar(10) collate utf8_unicode_ci NOT NULL,
  `repair_symptoms` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `repair_datesend` date NOT NULL,
  `repair_datereceive` date NOT NULL,
  `repair_price` float NOT NULL,
  `drbno_id` varchar(10) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`repair_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `drbrepair`
-- 

INSERT INTO `drbrepair` VALUES ('R000000013', 'บริษัท Advice Computer จำกัด', 'ราชเทวี กรุงเทพฯ', 'คุณเจ', '0843434343', 'ศรัญญา', '0834301876', 'ค้าง', '2014-08-22', '0000-00-00', 0, 'D000000002');
INSERT INTO `drbrepair` VALUES ('R000000012', 'บริษัท Advice Computer จำกัด', 'ราชเทวี กรุงเทพฯ', 'คุณเจ', '0843434343', 'ศรัญญา', '0834301876', 'ค้าง', '2014-08-22', '0000-00-00', 0, 'D000000001');
INSERT INTO `drbrepair` VALUES ('R000000010', 'บริษัท Advice Computer จำกัด', 'ราชเทวี กรุงเทพฯ', 'คุณเจ', '0843434343', 'test', '1231231', 'test', '2014-07-18', '2014-08-22', 0, 'D000000001');
INSERT INTO `drbrepair` VALUES ('R000000011', 'บริษัท ABC จำกัด', 'สาทร ซอย 10', 'test', '3423423423', 'test', '4524234', 'test', '2014-07-18', '2014-07-29', 0, 'D000000028');
INSERT INTO `drbrepair` VALUES ('R000000009', 'รับซ่อม', 'บางนา', 'คุณเจ', '0834302811', 'พี่อ๊อฟ', '083430187', 'พัง', '2014-07-17', '2014-07-16', 0, 'D000000005');
INSERT INTO `drbrepair` VALUES ('R000000008', 'รับซ่อม', 'บางนา', 'คุณเจ', '0834302811', 'พี่อ๊อฟ', '083430187', 'พัง', '2014-07-17', '2014-07-16', 100, 'D000000004');
INSERT INTO `drbrepair` VALUES ('R000000006', 'dsd', 'fsdfs', 'dfsdf', '34234', 'sdfsdf', '23423', 'dsfsdf', '2014-07-16', '2014-07-16', 50, 'D000000003');
INSERT INTO `drbrepair` VALUES ('R000000007', 'หกด', 'หกดหก', 'ดหกด', '234234', 'sdfsdf', '23432', 'sdfsdf', '2014-07-17', '2014-07-16', 100, 'D000000011');
INSERT INTO `drbrepair` VALUES ('R000000005', 'test', 'test', 'test', '123123', 'test', '32423423', 'ewrwer', '2014-07-16', '2014-07-16', 50, 'D000000003');
INSERT INTO `drbrepair` VALUES ('R000000004', 'test', 'test', 'test', '123123', 'test', '32423423', 'ewrwer', '2014-07-16', '2014-07-16', 1000, 'D000000001');
INSERT INTO `drbrepair` VALUES ('R000000003', 'erfsf', 'sdfsdf', 'esfdf', '3242342', 'dfsdf', '34234', 'dsfsdfs', '2014-07-17', '2014-07-16', 222, 'D000000001');
INSERT INTO `drbrepair` VALUES ('R000000002', 'sdfsdf', 'sdfsdf', 'sdfsdf', '324324', 'dsfsdf', '32424', 'dsdsfsdf', '2014-07-17', '2014-08-22', 0, 'D000000002');
INSERT INTO `drbrepair` VALUES ('R000000001', 'sdfsdf', 'sdfsdf', 'sdfsdf', '324324', 'dsfsdf', '32424', 'dsdsfsdf', '2014-07-17', '2014-07-16', 111, 'D000000001');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `drbstatus`
-- 

CREATE TABLE `drbstatus` (
  `drbstatus_id` varchar(1) collate utf8_unicode_ci NOT NULL,
  `drbstatus_name` varchar(100) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`drbstatus_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `drbstatus`
-- 

INSERT INTO `drbstatus` VALUES ('1', 'ปกติ');
INSERT INTO `drbstatus` VALUES ('2', 'ส่งซ่อม');
INSERT INTO `drbstatus` VALUES ('3', 'แทงจำหน่าย');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `drbtype`
-- 

CREATE TABLE `drbtype` (
  `drbtype_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  `drbtype_name` varchar(100) collate utf8_unicode_ci NOT NULL,
  `drbtype_deper` float NOT NULL,
  PRIMARY KEY  (`drbtype_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `drbtype`
-- 

INSERT INTO `drbtype` VALUES ('D004', 'โฆษณาและเผยแพร่', 20);
INSERT INTO `drbtype` VALUES ('D003', 'การเกษตร', 20);
INSERT INTO `drbtype` VALUES ('D002', 'ยานพาหนะและขนส่ง', 20);
INSERT INTO `drbtype` VALUES ('D001', 'สำนักงาน', 10);
INSERT INTO `drbtype` VALUES ('D005', 'ไฟฟ้าและวิทยุ', 20);
INSERT INTO `drbtype` VALUES ('D006', 'คอมพิวเตอร์', 50);
INSERT INTO `drbtype` VALUES ('D007', 'การศึกษา', 12);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `employee`
-- 

CREATE TABLE `employee` (
  `emp_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  `emp_fname` varchar(50) collate utf8_unicode_ci NOT NULL,
  `emp_lname` varchar(50) collate utf8_unicode_ci NOT NULL,
  `emp_tel` varchar(11) collate utf8_unicode_ci NOT NULL,
  `emp_img` varchar(1000) collate utf8_unicode_ci NOT NULL,
  `auth_id` varchar(11) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`emp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `employee`
-- 

INSERT INTO `employee` VALUES ('E001', 'สมชาย', 'สวนเพ็ชร', '', '', '06172014-01');
INSERT INTO `employee` VALUES ('E002', 'สมจิต', 'ศรีอนันต์', '0823423443', 'emp_img/07162014-06.jpg', '07162014-06');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `floor`
-- 

CREATE TABLE `floor` (
  `floor_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  `floor_name` varchar(50) collate utf8_unicode_ci NOT NULL,
  `building_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`floor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `floor`
-- 

INSERT INTO `floor` VALUES ('F009', '9', 'B001');
INSERT INTO `floor` VALUES ('F008', '8', 'B001');
INSERT INTO `floor` VALUES ('F007', '7', 'B001');
INSERT INTO `floor` VALUES ('F006', '6', 'B001');
INSERT INTO `floor` VALUES ('F005', '5', 'B001');
INSERT INTO `floor` VALUES ('F004', '4', 'B001');
INSERT INTO `floor` VALUES ('F003', '3', 'B001');
INSERT INTO `floor` VALUES ('F002', '2', 'B001');
INSERT INTO `floor` VALUES ('F001', '1', 'B001');
INSERT INTO `floor` VALUES ('F021', '6', 'B004');
INSERT INTO `floor` VALUES ('F020', '5', 'B004');
INSERT INTO `floor` VALUES ('F019', '4', 'B004');
INSERT INTO `floor` VALUES ('F018', '3', 'B004');
INSERT INTO `floor` VALUES ('F017', '2', 'B004');
INSERT INTO `floor` VALUES ('F016', '1', 'B004');
INSERT INTO `floor` VALUES ('F015', '1', 'B003');
INSERT INTO `floor` VALUES ('F014', '5', 'B002');
INSERT INTO `floor` VALUES ('F013', '4', 'B002');
INSERT INTO `floor` VALUES ('F012', '3', 'B002');
INSERT INTO `floor` VALUES ('F011', '2', 'B002');
INSERT INTO `floor` VALUES ('F010', '1', 'B002');
INSERT INTO `floor` VALUES ('F022', '1', 'B005');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `perm`
-- 

CREATE TABLE `perm` (
  `perm_id` varchar(1) collate utf8_unicode_ci NOT NULL,
  `perm_name` varchar(100) collate utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `perm`
-- 

INSERT INTO `perm` VALUES ('1', 'ผู้ดูแลระบบ');
INSERT INTO `perm` VALUES ('2', 'เจ้าหน้าที่ครุภัณฑ์');
INSERT INTO `perm` VALUES ('3', 'ผู้ใช้งานทั่วไป');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `respons`
-- 

CREATE TABLE `respons` (
  `resp_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  `resp_name` varchar(100) collate utf8_unicode_ci NOT NULL,
  `resp_branch` varchar(100) collate utf8_unicode_ci NOT NULL,
  `resp_tel` varchar(100) collate utf8_unicode_ci NOT NULL,
  `resp_room` varchar(10) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`resp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `respons`
-- 

INSERT INTO `respons` VALUES ('R001', 'อาจารย์สุธินี', 'ระบบสารสนเทศ', '0895461237', '431');
INSERT INTO `respons` VALUES ('R002', 'อาจารย์ตติยา', 'ระบบสารสนเทศ', '0898765432', '441');
INSERT INTO `respons` VALUES ('R003', 'อาจารย์วิทยา', 'ระบบสารสนเทศ', '0812345678', '441');
INSERT INTO `respons` VALUES ('R004', 'อาจารย์สมรัก', 'ระบบสารสนเทศ', '084578961', '433');
INSERT INTO `respons` VALUES ('R005', 'อาจารย์การตลาด', 'การตลาด', '0834515845', '9001');
INSERT INTO `respons` VALUES ('R006', 'อาจารย์ สมเจตน์ สวนเพ็ชร', 'ไม่มี', '0845596541', 'ไม่มี');
INSERT INTO `respons` VALUES ('R007', 'อาจารย์วรรณิสา', 'การจัดการ', '0891234567', 'ึ731');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `room`
-- 

CREATE TABLE `room` (
  `room_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  `room_name` varchar(50) collate utf8_unicode_ci NOT NULL,
  `floor_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`room_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `room`
-- 

INSERT INTO `room` VALUES ('R006', '303', 'F003');
INSERT INTO `room` VALUES ('R005', '302', 'F003');
INSERT INTO `room` VALUES ('R004', '301', 'F003');
INSERT INTO `room` VALUES ('R003', '206', 'F002');
INSERT INTO `room` VALUES ('R002', '201', 'F002');
INSERT INTO `room` VALUES ('R001', '101', 'F001');
INSERT INTO `room` VALUES ('R007', '304', 'F003');
INSERT INTO `room` VALUES ('R008', '305', 'F003');
INSERT INTO `room` VALUES ('R009', '306', 'F003');
INSERT INTO `room` VALUES ('R010', '307', 'F003');
INSERT INTO `room` VALUES ('R011', '308', 'F003');
INSERT INTO `room` VALUES ('R012', '309', 'F003');
INSERT INTO `room` VALUES ('R013', '401', 'F004');
INSERT INTO `room` VALUES ('R014', '402', 'F004');
INSERT INTO `room` VALUES ('R024', '503', 'F005');
INSERT INTO `room` VALUES ('R023', '502', 'F005');
INSERT INTO `room` VALUES ('R017', '405', 'F004');
INSERT INTO `room` VALUES ('R018', '406', 'F004');
INSERT INTO `room` VALUES ('R022', '501', 'F005');
INSERT INTO `room` VALUES ('R020', '408', 'F004');
INSERT INTO `room` VALUES ('R021', '409', 'F004');
INSERT INTO `room` VALUES ('R025', '504', 'F005');
INSERT INTO `room` VALUES ('R026', '505', 'F005');
INSERT INTO `room` VALUES ('R027', '601', 'F006');
INSERT INTO `room` VALUES ('R028', '602', 'F006');
INSERT INTO `room` VALUES ('R029', '603', 'F006');
INSERT INTO `room` VALUES ('R030', '604', 'F006');
INSERT INTO `room` VALUES ('R031', '605', 'F006');
INSERT INTO `room` VALUES ('R032', '606', 'F006');
INSERT INTO `room` VALUES ('R033', '607', 'F006');
INSERT INTO `room` VALUES ('R034', '608', 'F006');
INSERT INTO `room` VALUES ('R035', '609', 'F006');
INSERT INTO `room` VALUES ('R036', '701', 'F007');
INSERT INTO `room` VALUES ('R037', '702', 'F007');
INSERT INTO `room` VALUES ('R038', '703', 'F007');
INSERT INTO `room` VALUES ('R039', '704', 'F007');
INSERT INTO `room` VALUES ('R040', '705', 'F007');
INSERT INTO `room` VALUES ('R041', '706', 'F007');
INSERT INTO `room` VALUES ('R042', '707', 'F007');
INSERT INTO `room` VALUES ('R043', '708', 'F007');
INSERT INTO `room` VALUES ('R044', '709', 'F007');
INSERT INTO `room` VALUES ('R045', '801', 'F008');
INSERT INTO `room` VALUES ('R046', '802', 'F008');
INSERT INTO `room` VALUES ('R047', '803', 'F008');
INSERT INTO `room` VALUES ('R048', '804', 'F008');
INSERT INTO `room` VALUES ('R049', '805', 'F008');
INSERT INTO `room` VALUES ('R050', '806', 'F008');
INSERT INTO `room` VALUES ('R051', '807', 'F008');
INSERT INTO `room` VALUES ('R052', '808', 'F008');
INSERT INTO `room` VALUES ('R053', '809', 'F008');
INSERT INTO `room` VALUES ('R054', '901', 'F009');
INSERT INTO `room` VALUES ('R055', '902', 'F009');
INSERT INTO `room` VALUES ('R056', '903', 'F009');
INSERT INTO `room` VALUES ('R057', '904', 'F009');
INSERT INTO `room` VALUES ('R058', '905', 'F009');
INSERT INTO `room` VALUES ('R059', '906', 'F009');
INSERT INTO `room` VALUES ('R060', '907', 'F009');
INSERT INTO `room` VALUES ('R061', '908', 'F009');
INSERT INTO `room` VALUES ('R062', '909', 'F009');
INSERT INTO `room` VALUES ('R079', '452B', 'F014');
INSERT INTO `room` VALUES ('R063', '421B', 'F011');
INSERT INTO `room` VALUES ('R064', '422B', 'F011');
INSERT INTO `room` VALUES ('R065', '423B', 'F011');
INSERT INTO `room` VALUES ('R066', '424B', 'F011');
INSERT INTO `room` VALUES ('R067', '425B', 'F011');
INSERT INTO `room` VALUES ('R078', '451B', 'F014');
INSERT INTO `room` VALUES ('R077', '445B', 'F013');
INSERT INTO `room` VALUES ('R076', '444B', 'F013');
INSERT INTO `room` VALUES ('R075', '443B', 'F013');
INSERT INTO `room` VALUES ('R074', '442B', 'F013');
INSERT INTO `room` VALUES ('R073', '441B', 'F013');
INSERT INTO `room` VALUES ('R072', '435B', 'F012');
INSERT INTO `room` VALUES ('R071', '434B', 'F012');
INSERT INTO `room` VALUES ('R070', '433B', 'F012');
INSERT INTO `room` VALUES ('R069', '432B', 'F012');
INSERT INTO `room` VALUES ('R068', '431B', 'F012');
INSERT INTO `room` VALUES ('R080', '453B', 'F014');
INSERT INTO `room` VALUES ('R081', '454B', 'F014');
INSERT INTO `room` VALUES ('R082', '455B', 'F014');
INSERT INTO `room` VALUES ('R093', '9211B', 'F017');
INSERT INTO `room` VALUES ('R092', '9210B', 'F017');
INSERT INTO `room` VALUES ('R091', '9209B', 'F017');
INSERT INTO `room` VALUES ('R090', '9208B', 'F017');
INSERT INTO `room` VALUES ('R089', '9207B', 'F017');
INSERT INTO `room` VALUES ('R088', '9206B', 'F017');
INSERT INTO `room` VALUES ('R087', '9205B', 'F017');
INSERT INTO `room` VALUES ('R086', '9204B', 'F017');
INSERT INTO `room` VALUES ('R085', '9203B', 'F017');
INSERT INTO `room` VALUES ('R084', '9202B', 'F017');
INSERT INTO `room` VALUES ('R083', '9201B', 'F017');
INSERT INTO `room` VALUES ('R094', '9301B', 'F018');
INSERT INTO `room` VALUES ('R095', '9302B', 'F018');
INSERT INTO `room` VALUES ('R096', '9303B', 'F018');
INSERT INTO `room` VALUES ('R097', '9304B', 'F018');
INSERT INTO `room` VALUES ('R098', '9305B', 'F018');
INSERT INTO `room` VALUES ('R099', '9306B', 'F018');
INSERT INTO `room` VALUES ('R100', '9307B', 'F018');
INSERT INTO `room` VALUES ('R101', '9308B', 'F018');
INSERT INTO `room` VALUES ('R102', '9309B', 'F018');
INSERT INTO `room` VALUES ('R103', '9310B', 'F018');
INSERT INTO `room` VALUES ('R104', '9311B', 'F018');
INSERT INTO `room` VALUES ('R105', '9401B', 'F019');
INSERT INTO `room` VALUES ('R106', '9402B', 'F019');
INSERT INTO `room` VALUES ('R107', '9403B', 'F019');
INSERT INTO `room` VALUES ('R108', '9404B', 'F019');
INSERT INTO `room` VALUES ('R109', '9405B', 'F019');
INSERT INTO `room` VALUES ('R110', '9406B', 'F019');
INSERT INTO `room` VALUES ('R111', '9407B', 'F019');
INSERT INTO `room` VALUES ('R112', '9408B', 'F019');
INSERT INTO `room` VALUES ('R113', '9409B', 'F019');
INSERT INTO `room` VALUES ('R114', '9410B', 'F019');
INSERT INTO `room` VALUES ('R115', '9411B', 'F019');
INSERT INTO `room` VALUES ('R116', '9501B', 'F020');
INSERT INTO `room` VALUES ('R117', '9502B', 'F020');
INSERT INTO `room` VALUES ('R118', '9503B', 'F020');
INSERT INTO `room` VALUES ('R119', '9504B', 'F020');
INSERT INTO `room` VALUES ('R120', '9505B', 'F020');
INSERT INTO `room` VALUES ('R121', '9506B', 'F020');
INSERT INTO `room` VALUES ('R122', '9507B', 'F020');
INSERT INTO `room` VALUES ('R123', '9508B', 'F020');
INSERT INTO `room` VALUES ('R124', '9509B', 'F020');
INSERT INTO `room` VALUES ('R125', '9510B', 'F020');
INSERT INTO `room` VALUES ('R126', '9511B', 'F020');
INSERT INTO `room` VALUES ('R127', '9602B', 'F021');
INSERT INTO `room` VALUES ('R128', '311', 'F022');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `typeof`
-- 

CREATE TABLE `typeof` (
  `typeof_id` varchar(1) collate utf8_unicode_ci NOT NULL,
  `typeof_name` varchar(100) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`typeof_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `typeof`
-- 

INSERT INTO `typeof` VALUES ('1', 'เงินงบประมาณ');
INSERT INTO `typeof` VALUES ('2', 'เงินนอกงบประมาณ');
INSERT INTO `typeof` VALUES ('3', 'เงินบริจาค / เงินช่วยเหลือ');
INSERT INTO `typeof` VALUES ('4', 'อื่นๆ');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `unit`
-- 

CREATE TABLE `unit` (
  `unit_id` varchar(4) collate utf8_unicode_ci NOT NULL,
  `unit_name` varchar(100) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`unit_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- dump ตาราง `unit`
-- 

INSERT INTO `unit` VALUES ('U005', 'ชุด');
INSERT INTO `unit` VALUES ('U004', 'เครื่อง');
INSERT INTO `unit` VALUES ('U003', 'ตัว');
INSERT INTO `unit` VALUES ('U002', 'อัน');
INSERT INTO `unit` VALUES ('U001', 'ชิ้น');
