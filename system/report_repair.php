<?php
	define('FPDF_FONTPATH','font/');
	header('Content-type: application/pdf');
	header('Content-Type: text/html; charset=UTF-8');
	
	require_once 'mc_table.php';
	require_once 'database.php';
	
	$exec = database::getInstance();
	$pdf = new PDF_MC_Table();
	$month = array('มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม');
	$getdate = date('Y-m-d');
	$today = explode('-', date('Y-m-d'));
	$i = 0;
	$c = 1;
	$p = 0;
	$start = explode('-', $_GET['start']);
	$end = explode('-', $_GET['end']);
	$qry = $exec->execute("SELECT * FROM drbno, drb, drbstatus, respons, drbrepair WHERE drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND respons.resp_id=drbno.resp_id AND drbno.drbno_id=drbrepair.drbno_id AND drbrepair.repair_datesend BETWEEN '$start[2]-$start[1]-$start[0]' AND '$end[2]-$end[1]-$end[0]' AND drb.drbtype_id='$_GET[drbtype_id]' and drbno.drbno_status NOT IN('3') ORDER BY drbno.drbno_id ASC");
	
	
	$pdf->AddPage();
	$pdf->AddFont('angsana','','angsa.php');
	$pdf->AddFont('angsana','B','angsab.php');
	$pdf->AddFont('angsana','I','angsai.php');
	$pdf->AddFont('angsana','BI','angsaz.php');
	$pdf->SetFont('angsana','B',16);
	$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'หน้า '.($p+1) ), 0, 'R');
	$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'ณ วันที่ '.intval($today[2]).' เดือน '.$month[$today[1] - 1].' พ.ศ. '.($today[0]+543) ), 0, 'R');
    $pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ' ), 0, 'C');
	$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'รายงานส่งซ่อมครุภัณฑ์' ), 0, 'C');
	$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'ตั้งแต่วันที่ '.intval($start[0]).' '.$month[$start[1]-1].' '.($start[2]+543).' ถึงวันที่ '.intval($end[0]).' '.$month[$end[1]-1].' '.($end[2]+543)), 0, 'C');
	#$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'ประเภทคุรภัณฑ์ '.), 0, 'C');
	$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'คณะบริหารธุรกิจ'), 0, 'R');
	$pdf->SetWidths(array(13,40,50,22,25,20,20));
	#$pdf->HeaderRow(array(iconv('UTF-8', 'cp874', 'ลำดับที่'), iconv('UTF-8', 'cp874', 'หมายเลขครุภัณฑ์'), iconv('UTF-8', 'cp874', 'รายการ'), iconv('UTF-8', 'cp874', 'วันที่ส่งซ่อม'), iconv('UTF-8', 'cp874', 'ผู้รับผิดชอบ'), iconv('UTF-8', 'cp874', 'บริษัทที่รับซ่อม'), iconv('UTF-8', 'cp874', 'อาการ')));
	while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
		
		if($c == 1){
			
			$pdf->HeaderRow(array(iconv('UTF-8', 'cp874', 'ลำดับที่'), iconv('UTF-8', 'cp874', 'หมายเลขครุภัณฑ์'), iconv('UTF-8', 'cp874', 'รายการ'), iconv('UTF-8', 'cp874', 'วันที่ส่งซ่อม'), iconv('UTF-8', 'cp874', 'ผู้รับผิดชอบ'), iconv('UTF-8', 'cp874', 'บริษัทที่รับซ่อม'), iconv('UTF-8', 'cp874', 'อาการ')));
		
		}elseif($c / 20 == 1){
			$p++;
			$pdf->AddPage();
			$pdf->MultiCell(0, 7, iconv('UTF-8', 'cp874', 'หน้า '.($p+1) ), 0, 'R');
			$pdf->HeaderRow(array(iconv('UTF-8', 'cp874', 'ลำดับที่'), iconv('UTF-8', 'cp874', 'หมายเลขครุภัณฑ์'), iconv('UTF-8', 'cp874', 'รายการ'), iconv('UTF-8', 'cp874', 'วันที่ส่งซ่อม'), iconv('UTF-8', 'cp874', 'ผู้รับผิดชอบ'), iconv('UTF-8', 'cp874', 'บริษัทที่รับซ่อม'), iconv('UTF-8', 'cp874', 'อาการ')));
			
		}
		$i++;
		$c++;
		$pdf->Row(array($i, iconv('UTF-8', 'cp874', $rs['drbno_number']), iconv('UTF-8', 'cp874', $rs['drb_name'].' '.$rs['drb_model']), iconv('UTF-8', 'cp874', $rs['repair_datesend']), iconv('UTF-8', 'cp874', $rs['resp_name']), iconv('UTF-8', 'cp874', $rs['repair_contact']), iconv('UTF-8', 'cp874', $rs['repair_symptoms'])));
	}
	$pdf->Output();
?>