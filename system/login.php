<?php
	if(!empty($_SESSION['auth'])){
		if($_SESSION['auth']['perm'] == 'ผู้ดูแลระบบ'){
			echo '<ul class="nav">
			<li><a class="menu" href="index.php">หน้าหลัก</a></li>
				<li><a class="menu" href="#แสดงครุภัณฑ์ทั้งหมด">จัดการครุภัณฑ์</a>
					<ul>
						<li><a class="submenu" href="drb_ins.php">จัดเก็บครุภัณฑ์</a></li>         
						<li><a class="submenu" href="drb_srh.php">แก้ไขครุภัณฑ์</a></li>
						<li><a class="submenu" href="drbstorage_disp.php">แก้ไขสถานที่จัดเก็บครุภัณฑ์</a></li>
						<li><a class="submenu" href="drb_disp.php">ครุภัณฑ์ยังไม่ได้จัดเก็บ</a></li>
						<li><a class="submenu" href="display.php">ค้นหาครุภัณฑ์</a></li>
					</ul>
				</li>
				<li><a class="menu" href="#แสดงครุภัณฑ์แทงจำหน่ายทั้งหมด">ครุภัณฑ์แทงจำหน่าย</a>
					<ul>
						<li><a class="submenu" href="drbdispose_disp.php">แทงจำหน่ายครุภัณฑ์</a></li>
						<li><a class="submenu" href="drbdispose_srh.php">ค้นหาคุรุภัณฑ์แทงจำหน่าย</a></li>           
					</ul>
				</li>
				<li><a class="menu" href="#แสดงครุภัณฑ์ส่งซ่อมทั้งหมด">ครุภัณฑ์ส่งซ่อม</a>
					<ul>
						<li><a class="submenu" href="drbrepair_disp.php">ส่งซ่อมครุภัณฑ์</a></li>
						<li><a class="submenu" href="drbrepair_srh.php">รับคืนครุภัณฑ์</a></li> 
					</ul>
				</li>
				<li><a class="menu" href="#">จัดการผู้ใช้งานระบบ</a>
					<ul>
						<li><a class="submenu" href="emp_disp.php">กำหนดสิทธิ์ผู้ใช้งาน</a></li>
						<li><a class="submenu" href="emp_ins.php">เพิ่มผู้ใช้งานระบบ</a></li> 
						<li><a class="submenu" href="emp_edit.php">แก้ไขข้อมูลผู้ใช้งานระบบ</a></li> 
					</ul>
				</li>
					
				<li><a class="menu" href="#">รายงาน</a>
						<ul>
						<li><a class="submenu" href="drb_report.php">รายงานครุภัณฑ์</a></li>
						<li><a class="submenu" href="drb_fiscalyear.php">รายงานครุภัณฑ์ปีงบประมาณ</a></li>
						<li><a class="submenu" href="drbrepair_report.php">รายงานครุภัณฑ์<br>ส่งซ่อม</a></li>
						<li><a class="submenu" href="drbdispose_report.php">รายงานครุภัณฑ์<br>แทงจำหน่าย</a></li>
						<li><a class="submenu" href="report_depreciation.php">รายงานค่าเสื่อมครุภัณฑ์ประจำปี</a></li>
					</ul>
				</li>
			</ul>';
		}elseif($_SESSION['auth']['perm'] == 'เจ้าหน้าที่ครุภัณฑ์'){
			echo '<ul class="nav">
			<li><a class="menu" href="index.php">หน้าหลัก</a></li>
				<li><a class="menu" href="#แสดงครุภัณฑ์ทั้งหมด">จัดการครุภัณฑ์</a>
					<ul>
						<li><a class="submenu" href="drb_ins.php">จัดเก็บครุภัณฑ์</a></li>         
						<li><a class="submenu" href="drb_srh.php">แก้ไขครุภัณฑ์</a></li>
						<li><a class="submenu" href="drbstorage_disp.php">แก้ไขสถานที่จัดเก็บครุภัณฑ์</a></li>
						<li><a class="submenu" href="display.php">ค้นหาครุภัณฑ์</a></li>
					</ul>
				</li>
				<li><a class="menu" href="#แสดงครุภัณฑ์แทงจำหน่ายทั้งหมด">ครุภัณฑ์แทงจำหน่าย</a>
					<ul>
						<li><a class="submenu" href="drbdispose_disp.php">แทงจำหน่ายครุภัณฑ์</a></li>
						<li><a class="submenu" href="drbdispose_srh.php">ค้นหาคุรุภัณฑ์แทงจำหน่าย</a></li>           
					</ul>
				</li>
				<li><a class="menu" href="#แสดงครุภัณฑ์ส่งซ่อมทั้งหมด">ครุภัณฑ์ส่งซ่อม</a>
					<ul>
						<li><a class="submenu" href="drbrepair_disp.php">ส่งซ่อมครุภัณฑ์</a></li>
						<li><a class="submenu" href="drbrepair_srh.php">รับคืนครุภัณฑ์</a></li> 
					</ul>
				</li>  
				<li><a class="menu" href="#">รายงาน</a>
						<ul>
						<li><a class="submenu" href="drb_report.php">รายงานครุภัณฑ์</a></li>
							<li><a class="submenu" href="drb_fiscalyear.php">รายงานครุภัณฑ์ปีงบประมาณ</a></li>
						<li><a class="submenu" href="drbrepair_report.php">รายงานครุภัณฑ์<br>ส่งซ่อม</a></li>
						<li><a class="submenu" href="drbdispose_report.php">รายงานครุภัณฑ์<br>แทงจำหน่าย</a></li>
						<li><a class="submenu" href="report_depreciation.php">รายงานค่าเสื่อมครุภัณฑ์ประจำปี</a></li>
					</ul>
				</li>
			</ul>';
		}elseif($_SESSION['auth']['perm'] == 'ผู้ใช้งานทั่วไป'){
			echo '<ul class="nav">
			<li><a class="menu" href="index.php">หน้าหลัก</a></li>
			</ul>';
		}
    	echo '<form id="auth" action="" method="post">'.$_SESSION['auth']['name'].':'.$_SESSION['auth']['perm'].'<button name="btn_auth" value="logout">logout</button></form>';
    }else{
    	echo '<form id="auth" action="" method="post">';
        echo '<input name="user" type="text" size="18" maxlength="20" placeholder="ชื่อผู้ใช้งาน" />';
        echo '<input name="pass" type="password" size="18" maxlength="20" placeholder="รหัสผ่าน" />';
        echo '<button name="btn_auth" value="login">login</button></form>';
        #header('refresh:0; index.php');
        #exit();
    }
    
    if(!empty($_POST)){
    	if($_POST['btn_auth'] == 'login'){
			//echo "SELECT * FROM perm, auth, employee WHERE perm.perm_id=auth.perm_id AND auth.auth_id=employee.auth_id AND auth.auth_user='$_POST[user]' AND auth.auth_pass='$_POST[pass]'";
			$qry = $exec->execute("SELECT * FROM perm, auth, employee WHERE perm.perm_id=auth.perm_id AND auth.auth_id=employee.auth_id AND auth.auth_user='$_POST[user]' AND auth.auth_pass='$_POST[pass]'");
			#$qry = exec->execute("SELECT * FROM perm, auth, employee WHERE perm.perm_id=auth.perm_id AND auth.auth_id=employee.auth_id AND auth.auth_user='$_POST[user]' AND auth.auth_pass='$_POST[pass]'");
			#print_r($qry);
			$_SESSION['user'] = "";
			while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
				$_SESSION['auth']['name'] = $rs['emp_fname'];
                $_SESSION['auth']['perm'] = $rs['perm_name'];
				$log->write_log($_SESSION['auth']['name'].'->user_login');
				echo "<script>alert('เข้าสู่ระบบ');</script>";
			}
			#header('refresh:0; index.php');
			
			header('refresh:0;index.php');
        }elseif($_POST['btn_auth'] == 'logout'){
			$log->write_log($_SESSION['auth']['name'].'->user_logout');
        	session_destroy();
          header('refresh:0;index.php');
			echo "<script>alert('ออกจากระบบ');</script>";
        }
    }
?>