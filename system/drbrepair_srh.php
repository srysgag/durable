<?php
	require_once 'database.php';
	require_once 'log.php';
    session_start();
	ob_start();
    $exec = database::getInstance()
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/prototype.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ระบบจัดการครุภัณฑ์ คณะบริหารธุรกิจ มหาวิทยาลัยเทคโนโลยีราชมงคลกรุงเทพ</title>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.4.min.css" />
    <script src="js/jquery-1.10.2.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <!-- InstanceBeginEditable name="head" -->
    <style type="text/css">
		.ui-tabs{ font-family:tahoma; font-size:11px; }
		.ui-datepicker{ width:220px; font-family:tahoma; font-size:11px; text-align:center; }
		.ui-menu .ui-menu-item a{ height:14px; font-family:tahoma; font-size:12px; }
	</style> 
    <script type="text/javascript">
		$(document).ready(function() {
			$('.formatfloat').on('keypress',function(){
				if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode != 46 || $(this).val().indexOf('.') != -1)) {
					event.preventDefault();
				}
				
				$('#check_all').click(function () {    
					 $('input:checkbox').prop('checked', this.checked);    
				});
			});
			
			$('.datepicker').datepicker({
				dateFormat: 'dd-mm-yy',
				dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
				monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],  
				changeMonth: true,  
				changeYear: true,
				beforeShow:function(){    
					if($(this).val() != ''){  
						var arrayDate=$(this).val().split('-');       
						arrayDate[2]=parseInt(arrayDate[2]);  
						$(this).val(arrayDate[0]+'-'+arrayDate[1]+'-'+arrayDate[2]);  
					}  
					setTimeout(function(){
						$.each($('.ui-datepicker-year option'),function(j,k){  
							var textYear=parseInt($('.ui-datepicker-year option').eq(j).val());  
							$('.ui-datepicker-year option').eq(j).text(textYear);  
						});               
					},50);  
				},  
				onChangeMonthYear: function(){
					setTimeout(function(){
						$.each($('.ui-datepicker-year option'),function(i,v){  
							var textYear=parseInt($('.ui-datepicker-year option').eq(i).val());  
							$('.ui-datepicker-year option').eq(i).text(textYear);  
						});               
					},50);        
				},  
				onClose:function(){
					if($(this).val() != '' && $(this).val() == dateBefore){           
						var arrayDate=dateBefore.split('-');  
						arrayDate[2]=parseInt(arrayDate[2]);  
						$(this).val(arrayDate[0]+'-'+arrayDate[1]+'-'+arrayDate[2]);      
					}         
				},  
				onSelect: function(dateText, inst){
					dateBefore=$(this).val();  
					var arrayDate=dateText.split('-');  
					arrayDate[2]=parseInt(arrayDate[2]);  
					$(this).val(arrayDate[0]+'-'+arrayDate[1]+'-'+arrayDate[2]);  
				}
			});
		});
    </script>
	<!-- InstanceEndEditable -->
</head>
<body>
	<div id="header"></div>
    <div id="menu">
    	<?php
			include 'login.php';
		?>
    </div>
    <div id="middle">
    	<div id="info"><!-- InstanceBeginEditable name="content" -->
        	<fieldset>
            	<legend>ข้อมูลการค้นหา</legend>
                <form action="" method="post">
                    <label>เลือกประเภทการค้นหา</label>
                    <select name="search">
                        <option value="send">ครุภัณฑ์ที่ส่งซ่อม</option>
                        <option value="receive">ครุภัณฑ์ที่รับคืน</option>
                    </select>
                    <label>วันที่ต้องการค้นหา</label><input name="start" type="text" class="datepicker" value="<?php if(!empty($_POST['start'])) echo $_POST['start']; ?>" />
                    <label>ถึง</label><input name="end" type="text" class="datepicker" value="<?php if(!empty($_POST['end'])) echo $_POST['end']; ?>" />
                    <button name="btn" value="search">ค้นหา</button>
                </form>
            </fieldset>
            <fieldset>
            	<legend>ผลการค้นหา</legend>
                <form action="" method="post">
                    <?php
						if($_POST['btn'] == 'search'){
							$i = 1;
							
							if($_POST['search'] == 'send'){
								$start = explode('-', $_POST['start']);
								$end = explode('-', $_POST['end']);
								$qry = $exec->execute("SELECT * FROM drbno, drb, drbstatus, respons, drbrepair WHERE drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND respons.resp_id=drbno.resp_id AND drbno.drbno_id=drbrepair.drbno_id AND (drbrepair.repair_datesend BETWEEN '$start[2]-$start[1]-$start[0]' AND '$end[2]-$end[1]-$end[0]') AND  drbrepair.repair_datereceive='0000-00-00' AND drbno.drbno_status='2' ORDER BY drbno.drbno_id ASC");
								#echo "SELECT * FROM drbno, drb, drbstatus, respons, drbrepair WHERE drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND respons.resp_id=drbno.resp_id AND drbno.drbno_id=drbrepair.drbno_id AND (drbrepair.repair_datesend BETWEEN '$start[2]-$start[1]-$start[0]' AND '$end[2]-$end[1]-$end[0]') AND  drbrepair.repair_datereceive='0000-00-00' AND drbno.drbno_status='2' ORDER BY drbno.drbno_id ASC";
								if(mysqli_num_rows($qry) != 0){
									echo '<table id="tbldrbno"><thead><th>ลำดับที่</th><th>หมายเลขครุภัณฑ์</th><th>ชื่อครุภัณฑ์</th><th>ผู้รับผิดชอบ</th><th>วันที่ส่งซ่อม</th><th>อาการที่เสีย</th><th>ค่าใช้จ่าย</th><th>รับคืน</th></thead><tbody>';
									while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
										echo "<tr><td>$i</td><td>$rs[drbno_number]</td><td>$rs[drb_name]</td><td>$rs[resp_name]</td><td>$rs[repair_datesend]</td><td>$rs[repair_symptoms]</td><td><input id=\"repair_price\" class=\"formatfloat\" name=\"$rs[drbno_id]\" type=\"text\" /><input id=\"repair_price\" class=\"formatfloat\" name=\"repair$rs[drbno_id]\" type=\"hidden\" value=\"$rs[repair_id]\" /></td><td><input id=\"drbno_id\" name=\"drbno_id[]\" type=\"checkbox\" value=\"$rs[drbno_id]\" /></td></tr>";
										$i++;
									}
									echo '</tbody></table><center><br/><button id="btn" value="repair">ตกลง</button></center>';
								}else{
									echo '<center>ไม่พบข้อมูล</center>';
								}
								
							}elseif($_POST['search'] == 'receive'){
								$start = explode('-', $_POST['start']);
								$end = explode('-', $_POST['end']);
								$qry = $exec->execute("SELECT * FROM drbno, drb, drbstatus, respons, drbrepair WHERE drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND respons.resp_id=drbno.resp_id AND drbno.drbno_id=drbrepair.drbno_id AND (drbrepair.repair_datereceive BETWEEN '$start[2]-$start[1]-$start[0]' AND '$end[2]-$end[1]-$end[0]') AND drbno.drbno_status='1' GROUP BY drbno.drbno_id ASC");
								#echo "SELECT * FROM drbno, drb, drbstatus, respons, drbrepair WHERE drbstatus.drbstatus_id=drbno.drbstatus_id AND drbno.drb_id=drb.drb_id AND respons.resp_id=drbno.resp_id AND drbno.drbno_id=drbrepair.drbno_id AND (drbrepair.repair_datereceive BETWEEN '$start[2]-$start[1]-$start[0]' AND '$end[2]-$end[1]-$end[0]') AND drbno.drbno_status='1' ORDER BY drbno.drbno_id ASC";
								#print_r($qry);
								if(!empty($qry)){
									if(mysqli_num_rows($qry) != 0){
										echo '<table id="tbldrbno"><thead><th>ลำดับที่</th><th>หมายเลขครุภัณฑ์</th><th>ชื่อครุภัณฑ์</th><th>ผู้รับผิดชอบ</th><th>วันที่ส่งซ่อม</th><th>วันที่รับคืน</th><th>จำนวนครั้งที่ส่งซ่อม</th><th>ค่าใช้จ่าย</th></thead><tbody>';
										while($rs = mysqli_fetch_array($qry, MYSQLI_ASSOC)){
											$count = mysqli_fetch_array($exec->execute("SELECT COUNT(*) AS rows, SUM(repair_price) AS total FROM drbrepair WHERE drbno_id='$rs[drbno_id]'"));
											$sum = $exec->execute("SELECT SUM(repair_price) FROM drbrepair WHERE drbno_id='$rs[drbno_id]'");
											echo "<tr><td>$i</td><td>$rs[drbno_number]</td><td>$rs[drb_name]</td><td>$rs[resp_name]</td><td>$rs[repair_datesend]</td><td>$rs[repair_datereceive]</td><td>$count[rows]</td><td>".number_format($count['total'], 2, '.', ',')."</td></tr>";
											$i++;
										}
										echo '</tbody></table>';
									}else{
										echo '<center>ไม่พบข้อมูล</center>';
									}
								}else{
										echo '<center>ไม่พบข้อมูล</center>';
									}
							}
						}
                       ?>
                </form>
            </fieldset>
            <?php
				if(!empty($_POST['drbno_id'])){
					$drbno_id = $_POST['drbno_id'];
					$today = date('Y-m-d');
					for($i=0; $i<count($drbno_id); $i++){
						$repair_price = $_POST[$drbno_id[$i]];
						$repair_id = $_POST['repair'.$drbno_id[$i]];
						#echo $repair_id;
						$exec->execute("UPDATE drbrepair SET repair_datereceive='$today', repair_price='$repair_price' WHERE repair_id='$repair_id'");
						#echo "UPDATE drbrepair SET repair_datereceive='$today', repair_price='$repair_price' WHERE repair_id='$repair_id[$i]'";
						$exec->execute("UPDATE drbno SET drbno_status='1' WHERE drbno_id='$drbno_id[$i]'");
						
						$log->write_log($_SESSION['auth']['name'].'->dulable_receive:'.$drbno_id[$i]);
						
						
					}
					echo "<script>alert('รับคืนครุภัณฑ์เรียบร้อยแล้ว');</script>";
                   	    header("refresh:0;drbrepair_srh.php");
				}
			?>
		<!-- InstanceEndEditable --></div>
    </div>
    <div id="footer"></div>
</body>
<!-- InstanceEnd --></html>
